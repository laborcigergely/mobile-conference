/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 5);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
class Screen{

	init($screen, screenManager){
		this.$screen = $screen;
		this.screenManager = screenManager;

		this.$header = document.createElement('header');
		this.$header.innerHTML = '<span class="left"></span>' + '<span class="title"></span>' +'<span class="right"></span>'
		this.title = this.$screen.dataset.title;
		this.$screen.appendChild(this.$header);



		this.$content = document.createElement('article');
		this.$screen.appendChild(this.$content);

		if(this.getTemplate('screen')) this.$content.innerHTML = this.getTemplate('screen');

		this.initialize();

		['click'].forEach(eventName=> {

			this.$header.querySelector('span.left').addEventListener(eventName, event=>this.leftButtonClick(event));
			this.$header.querySelector('span.right').addEventListener(eventName, event=>this.rightButtonClick(event));

			this.$screen.addEventListener(eventName, event => {
				let $source = event.target;
				do {
					if ($source.dataset && typeof $source.dataset.clickName !== 'undefined') {
						this.click($source, $source.dataset.clickName, event);
						event.preventDefault();
						event.stopPropagation();
						break;
					} else {
						$source = $source.parentElement;
					}
				} while ($source !== this.$screen.parentElement);
			})
		});
	}

	initialize(){}
	setup(options){}

	activate(options = false){
		if(options !== false) { this.setup(options); }
		this.$content.scrollTop = 0;
		this.screenManager.activateScreen(this);
		return this;
	}

	set title(value){
		this.$header.querySelector('.title').innerHTML = value;
	}

	get title(){
		return this.$header.querySelector('.title').innerHTML;
	}

	set leftButtonIcon(icon){
		this.$header.querySelector('.left').innerHTML = '<i class="'+icon+'"></i>';
	}

	set rightButtonIcon(icon){
		this.$header.querySelector('.right').innerHTML = '<i class="'+icon+'"></i>';
	}

	leftButtonClick(event){}
	rightButtonClick(event){}

	getTemplate(name){
		let $template = this.$screen.querySelector('template[name='+name+']');
		if($template) return $template.innerHTML;
		return null;
	}

	click($source, name, event){
		console.log($source, name, event);
	}


}
/* harmony export (immutable) */ __webpack_exports__["a"] = Screen;


/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
let data = [
	{
		"type": "section",
		"from": "2018-06-21T10:30",
		"to": "2018-06-21T12:30",
		"title": "Hibrid és multimodális képalkotás",
		"room": "1",
		"id": "1.1"
	},
	{
		"type": "section",
		"from": "2018-06-21T11:00",
		"to": "2018-06-21T12:35",
		"title": "Intervenciós radiológia",
		"room": "2",
		"id": "1.2"
	},
	{
		"type": "section",
		"from": "2018-06-21T10:30",
		"to": "2018-06-21T12:35",
		"title": "Ifjúsági Bizottság Workshop",
		"room": "3",
		"id": "1.3"
	},
	{
		"type": "section",
		"from": "2018-06-21T14:00",
		"to": "2018-06-21T16:00",
		"title": "Boehringer-Ingelheim szimpózium és mellkasi radiológia",
		"room": "1",
		"id": "1.4"
	},
	{
		"type": "section",
		"from": "2018-06-21T14:00",
		"to": "2018-06-21T16:00 ",
		"title": "Radiológiai IT",
		"room": "2",
		"id": "1.5"
	},
	{
		"type": "section",
		"from": "2018-06-21T14:00",
		"to": "2018-06-21T15:50",
		"title": "Pelvis",
		"room": "3",
		"id": "1.6"
	},
	{
		"type": "section",
		"from": "2018-06-22T08:20",
		"to": "2018-06-22T16:30",
		"title": "Mesterséges intelligencia és radiológia",
		"room": "1",
		"id": "2.1"
	},
	{
		"type": "section",
		"from": "2018-06-22T08:30",
		"to": "2018-06-22T10:20",
		"title": "Gócos májelváltozások korszerű diagnosztikája és kezelése",
		"room": "2",
		"id": "2.2"
	},
	{
		"type": "section",
		"from": "2018-06-22T08:30",
		"to": "2018-06-22T10:20",
		"title": "Musculoskeletalis radiológia",
		"room": "3",
		"id": "2.3"
	},
	{
		"type": "section",
		"from": "2018-06-22T08:30",
		"to": "2018-06-22T10:20",
		"title": "Vasculáris ultrahang",
		"room": "4",
		"id": "2.4"
	},
	{
		"type": "section",
		"from": "2018-06-22T10:50",
		"to": "2018-06-22T12:30",
		"title": "Plenáris - Radiológia határok nélkül",
		"room": "1",
		"id": "2.5"
	},
	{
		"type": "section",
		"from": "2018-06-22T10:50",
		"to": "2018-06-22T12:30",
		"title": "Plenáris",
		"room": "2",
		"id": "2.6"
	},
	{
		"type": "section",
		"from": "2018-06-22T10:50",
		"to": "2018-06-22T12:30",
		"title": "Plenáris",
		"room": "3",
		"id": "2.7"
	},
	{
		"type": "section",
		"from": "2018-06-22T13:45",
		"to": "2018-06-22T16:25",
		"title": "Neuroradiológia",
		"room": "1",
		"id": "2.8"
	},
	{
		"type": "section",
		"from": "2018-06-22T14:00",
		"to": "2018-06-22T16:30",
		"title": "Mammográfia",
		"room": "2",
		"id": "2.9"
	},
	{
		"type": "section",
		"from": "2018-06-22T08:20",
		"to": "2018-06-22T16:30",
		"title": "Pancreas, gasztrointesztinális rendszer",
		"room": "3",
		"id": "2.10"
	},
	{
		"type": "section",
		"from": "2018-06-23T08:20",
		"to": "2018-06-23T10:06",
		"title": "Stroke",
		"room": "1",
		"id": "3.1"
	},
	{
		"type": "section",
		"from": "2018-06-23T08:30",
		"to": "2018-06-23T10:20",
		"title": "Onkológia",
		"room": "2",
		"id": "3.2"
	},
	{
		"type": "section",
		"from": "2018-06-23T08:30",
		"to": "2018-06-23T10:30",
		"title": "Workshop / Vetítés",
		"room": "3",
		"id": "3.3"
	},
	{
		"type": "section",
		"from": "2018-06-23T10:50",
		"to": "2018-06-23T13:00",
		"title": "Management",
		"room": "1",
		"id": "3.4"
	},
	{
		"type": "section",
		"from": "2018-06-23T10:50",
		"to": "2018-06-23T12:35",
		"title": "Kardiológiai képalkotás",
		"room": "2",
		"id": "3.5"
	},
	{
		"type": "section",
		"from": "2018-06-23T10:50",
		"to": "2018-06-23T13:00",
		"title": "Gyermekradiológia",
		"room": "3",
		"id": "3.6"
	},
	{
		"type": "section",
		"from": "2018-06-23T10:50",
		"to": "2018-06-23T12:00",
		"title": "Kvíz",
		"room": "4",
		"id": "3.7"
	},
	{
		"type": "event",
		"from": "2018-06-21T12:45",
		"to": "2018-06-21T14:00",
		"title": "Ebédszünet"
	},
	{
		"type": "event",
		"from": "2018-06-21T16:15",
		"to": "2018-06-21T18:00",
		"title": "Megnyitó"
	},
	{
		"type": "event",
		"from": "2018-06-21T20:00",
		"to": "2018-06-21T23:00",
		"title": "Koncert és állófogadás"
	},
	{
		"type": "event",
		"from": "2018-06-22T10:30",
		"to": "2018-06-22T10:50",
		"title": "Szünet"
	},
	{
		"type": "event",
		"from": "2018-06-22T12:30",
		"to": "2018-06-22T13:45",
		"title": "Ebédszünet"
	},
	{
		"type": "event",
		"from": "2018-06-22T17:00",
		"to": "2018-06-22T19:00",
		"title": "Közgyűlés"
	},
	{
		"type": "event",
		"from": "2018-06-22T20:00",
		"to": "2018-06-22T24:00",
		"title": "Gálavacsora"
	},
	{
		"type": "event",
		"from": "2018-06-23T10:30",
		"to": "2018-06-23T10:50",
		"title": "Szünet"
	}
];

data.forEach(v => {
	v.from = new Date(v.from);
	v.to = new Date(v.to);
});


data.sort(
	(a,b)=>{
		if(a.from < b.from) return -1;
		if(a.from > b.from) return 1;
		return 0
	}
);

/* harmony default export */ __webpack_exports__["a"] = (data);



/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
class DataReader {
	constructor(data) {
		this.data = data;
		/*
		this.data = data.sort((a, b) => {
			if (a[this.sort] < b[this.sort]) return -1;
			if (a[this.sort] > b[this.sort]) return 1;
			return 0;
		});*/
	}

	find(options) {

		let source = [];
		this.data.forEach(value => {source.push(value)});
		let matches = [];

		for (let field in options) {
			let pattern = options[field];
			let re = new RegExp(pattern);
			matches = [];
			source.forEach(element => {
				if (re.test(element[field])) {
					matches.push(element);
				}
			});
			source = matches;
		}

		return matches;
	}

	get(field, value){
		let found = null;
		this.data.forEach(row=>{
			if(row[field] == value) found = row;
		});
		return found;
	}

	getAll(field, value){
		let found = [];
		this.data.forEach(row=>{
			if(row[field] == value) found.push(row);
		});
		return found;
	}

	findStartsWith(field, pattern){
		let search = {};
		search[field] = DataReader.escapeRegExp(pattern + '.')+'(.*)';
		return this.find(search);
	}

	static escapeRegExp(str) {
		return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
	}
}
/* harmony export (immutable) */ __webpack_exports__["a"] = DataReader;


/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (class {

	constructor() {
		this.months = 'január,február,március,április,május,június,július,augusztus,szeptember,október,november,december'.split(',');
		this.days = 'vasárnap,hétfő,kedd,szerda,csütörtök,péntek,szombat'.split(',');
	}

	getFullDate(date) {
		let day = date.getDate();
		let monthIndex = date.getMonth();
		let year = date.getFullYear();
		return year + '. ' + this.months[monthIndex] + ' ' + day + '., '+this.days[date.getDay()];
	}

	getTime(date){
		return date.getHours().toString().padStart(2,'0')+':'+date.getMinutes().toString().padStart(2,'0');
	}
});

/***/ }),
/* 4 */
/***/ (function(module, exports) {

/* WEBPACK VAR INJECTION */(function(__webpack_amd_options__) {/* globals __webpack_amd_options__ */
module.exports = __webpack_amd_options__;

/* WEBPACK VAR INJECTION */}.call(exports, {}))

/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__modules_screen_manager__ = __webpack_require__(7);





(function () {
	'use strict';

	/**
	 * @preserve FastClick: polyfill to remove click delays on browsers with touch UIs.
	 *
	 * @codingstandard ftlabs-jsv2
	 * @copyright The Financial Times Limited [All Rights Reserved]
	 * @license MIT License (see LICENSE.txt)
	 */

	/*jslint browser:true, node:true*/
	/*global define, Event, Node*/


	/**
	 * Instantiate fast-clicking listeners on the specified layer.
	 *
	 * @constructor
	 * @param {Element} layer The layer to listen on
	 * @param {Object} [options={}] The options to override the defaults
	 */
	function FastClick(layer, options) {
		var oldOnClick;

		options = options || {};

		/**
		 * Whether a click is currently being tracked.
		 *
		 * @type boolean
		 */
		this.trackingClick = false;


		/**
		 * Timestamp for when click tracking started.
		 *
		 * @type number
		 */
		this.trackingClickStart = 0;


		/**
		 * The element being tracked for a click.
		 *
		 * @type EventTarget
		 */
		this.targetElement = null;


		/**
		 * X-coordinate of touch start event.
		 *
		 * @type number
		 */
		this.touchStartX = 0;


		/**
		 * Y-coordinate of touch start event.
		 *
		 * @type number
		 */
		this.touchStartY = 0;


		/**
		 * ID of the last touch, retrieved from Touch.identifier.
		 *
		 * @type number
		 */
		this.lastTouchIdentifier = 0;


		/**
		 * Touchmove boundary, beyond which a click will be cancelled.
		 *
		 * @type number
		 */
		this.touchBoundary = options.touchBoundary || 10;


		/**
		 * The FastClick layer.
		 *
		 * @type Element
		 */
		this.layer = layer;

		/**
		 * The minimum time between tap(touchstart and touchend) events
		 *
		 * @type number
		 */
		this.tapDelay = options.tapDelay || 200;

		/**
		 * The maximum time for a tap
		 *
		 * @type number
		 */
		this.tapTimeout = options.tapTimeout || 700;

		if (FastClick.notNeeded(layer)) {
			return;
		}

		// Some old versions of Android don't have Function.prototype.bind
		function bind(method, context) {
			return function() { return method.apply(context, arguments); };
		}


		var methods = ['onMouse', 'onClick', 'onTouchStart', 'onTouchMove', 'onTouchEnd', 'onTouchCancel'];
		var context = this;
		for (var i = 0, l = methods.length; i < l; i++) {
			context[methods[i]] = bind(context[methods[i]], context);
		}

		// Set up event handlers as required
		if (deviceIsAndroid) {
			layer.addEventListener('mouseover', this.onMouse, true);
			layer.addEventListener('mousedown', this.onMouse, true);
			layer.addEventListener('mouseup', this.onMouse, true);
		}

		layer.addEventListener('click', this.onClick, true);
		layer.addEventListener('touchstart', this.onTouchStart, false);
		layer.addEventListener('touchmove', this.onTouchMove, false);
		layer.addEventListener('touchend', this.onTouchEnd, false);
		layer.addEventListener('touchcancel', this.onTouchCancel, false);

		// Hack is required for browsers that don't support Event#stopImmediatePropagation (e.g. Android 2)
		// which is how FastClick normally stops click events bubbling to callbacks registered on the FastClick
		// layer when they are cancelled.
		if (!Event.prototype.stopImmediatePropagation) {
			layer.removeEventListener = function(type, callback, capture) {
				var rmv = Node.prototype.removeEventListener;
				if (type === 'click') {
					rmv.call(layer, type, callback.hijacked || callback, capture);
				} else {
					rmv.call(layer, type, callback, capture);
				}
			};

			layer.addEventListener = function(type, callback, capture) {
				var adv = Node.prototype.addEventListener;
				if (type === 'click') {
					adv.call(layer, type, callback.hijacked || (callback.hijacked = function(event) {
						if (!event.propagationStopped) {
							callback(event);
						}
					}), capture);
				} else {
					adv.call(layer, type, callback, capture);
				}
			};
		}

		// If a handler is already declared in the element's onclick attribute, it will be fired before
		// FastClick's onClick handler. Fix this by pulling out the user-defined handler function and
		// adding it as listener.
		if (typeof layer.onclick === 'function') {

			// Android browser on at least 3.2 requires a new reference to the function in layer.onclick
			// - the old one won't work if passed to addEventListener directly.
			oldOnClick = layer.onclick;
			layer.addEventListener('click', function(event) {
				oldOnClick(event);
			}, false);
			layer.onclick = null;
		}
	}

	/**
	 * Windows Phone 8.1 fakes user agent string to look like Android and iPhone.
	 *
	 * @type boolean
	 */
	var deviceIsWindowsPhone = navigator.userAgent.indexOf("Windows Phone") >= 0;

	/**
	 * Android requires exceptions.
	 *
	 * @type boolean
	 */
	var deviceIsAndroid = navigator.userAgent.indexOf('Android') > 0 && !deviceIsWindowsPhone;


	/**
	 * iOS requires exceptions.
	 *
	 * @type boolean
	 */
	var deviceIsIOS = /iP(ad|hone|od)/.test(navigator.userAgent) && !deviceIsWindowsPhone;


	/**
	 * iOS 4 requires an exception for select elements.
	 *
	 * @type boolean
	 */
	var deviceIsIOS4 = deviceIsIOS && (/OS 4_\d(_\d)?/).test(navigator.userAgent);


	/**
	 * iOS 6.0-7.* requires the target element to be manually derived
	 *
	 * @type boolean
	 */
	var deviceIsIOSWithBadTarget = deviceIsIOS && (/OS [6-7]_\d/).test(navigator.userAgent);

	/**
	 * BlackBerry requires exceptions.
	 *
	 * @type boolean
	 */
	var deviceIsBlackBerry10 = navigator.userAgent.indexOf('BB10') > 0;

	/**
	 * Determine whether a given element requires a native click.
	 *
	 * @param {EventTarget|Element} target Target DOM element
	 * @returns {boolean} Returns true if the element needs a native click
	 */
	FastClick.prototype.needsClick = function(target) {
		switch (target.nodeName.toLowerCase()) {

			// Don't send a synthetic click to disabled inputs (issue #62)
			case 'button':
			case 'select':
			case 'textarea':
				if (target.disabled) {
					return true;
				}

				break;
			case 'input':

				// File inputs need real clicks on iOS 6 due to a browser bug (issue #68)
				if ((deviceIsIOS && target.type === 'file') || target.disabled) {
					return true;
				}

				break;
			case 'label':
			case 'iframe': // iOS8 homescreen apps can prevent events bubbling into frames
			case 'video':
				return true;
		}

		return (/\bneedsclick\b/).test(target.className);
	};


	/**
	 * Determine whether a given element requires a call to focus to simulate click into element.
	 *
	 * @param {EventTarget|Element} target Target DOM element
	 * @returns {boolean} Returns true if the element requires a call to focus to simulate native click.
	 */
	FastClick.prototype.needsFocus = function(target) {
		switch (target.nodeName.toLowerCase()) {
			case 'textarea':
				return true;
			case 'select':
				return !deviceIsAndroid;
			case 'input':
				switch (target.type) {
					case 'button':
					case 'checkbox':
					case 'file':
					case 'image':
					case 'radio':
					case 'submit':
						return false;
				}

				// No point in attempting to focus disabled inputs
				return !target.disabled && !target.readOnly;
			default:
				return (/\bneedsfocus\b/).test(target.className);
		}
	};


	/**
	 * Send a click event to the specified element.
	 *
	 * @param {EventTarget|Element} targetElement
	 * @param {Event} event
	 */
	FastClick.prototype.sendClick = function(targetElement, event) {
		var clickEvent, touch;

		// On some Android devices activeElement needs to be blurred otherwise the synthetic click will have no effect (#24)
		if (document.activeElement && document.activeElement !== targetElement) {
			document.activeElement.blur();
		}

		touch = event.changedTouches[0];

		// Synthesise a click event, with an extra attribute so it can be tracked
		clickEvent = document.createEvent('MouseEvents');
		clickEvent.initMouseEvent(this.determineEventType(targetElement), true, true, window, 1, touch.screenX, touch.screenY, touch.clientX, touch.clientY, false, false, false, false, 0, null);
		clickEvent.forwardedTouchEvent = true;
		targetElement.dispatchEvent(clickEvent);
	};

	FastClick.prototype.determineEventType = function(targetElement) {

		//Issue #159: Android Chrome Select Box does not open with a synthetic click event
		if (deviceIsAndroid && targetElement.tagName.toLowerCase() === 'select') {
			return 'mousedown';
		}

		return 'click';
	};


	/**
	 * @param {EventTarget|Element} targetElement
	 */
	FastClick.prototype.focus = function(targetElement) {
		var length;

		// Issue #160: on iOS 7, some input elements (e.g. date datetime month) throw a vague TypeError on setSelectionRange. These elements don't have an integer value for the selectionStart and selectionEnd properties, but unfortunately that can't be used for detection because accessing the properties also throws a TypeError. Just check the type instead. Filed as Apple bug #15122724.
		if (deviceIsIOS && targetElement.setSelectionRange && targetElement.type.indexOf('date') !== 0 && targetElement.type !== 'time' && targetElement.type !== 'month' && targetElement.type !== 'email') {
			length = targetElement.value.length;
			targetElement.setSelectionRange(length, length);
		} else {
			targetElement.focus();
		}
	};


	/**
	 * Check whether the given target element is a child of a scrollable layer and if so, set a flag on it.
	 *
	 * @param {EventTarget|Element} targetElement
	 */
	FastClick.prototype.updateScrollParent = function(targetElement) {
		var scrollParent, parentElement;

		scrollParent = targetElement.fastClickScrollParent;

		// Attempt to discover whether the target element is contained within a scrollable layer. Re-check if the
		// target element was moved to another parent.
		if (!scrollParent || !scrollParent.contains(targetElement)) {
			parentElement = targetElement;
			do {
				if (parentElement.scrollHeight > parentElement.offsetHeight) {
					scrollParent = parentElement;
					targetElement.fastClickScrollParent = parentElement;
					break;
				}

				parentElement = parentElement.parentElement;
			} while (parentElement);
		}

		// Always update the scroll top tracker if possible.
		if (scrollParent) {
			scrollParent.fastClickLastScrollTop = scrollParent.scrollTop;
		}
	};


	/**
	 * @param {EventTarget} targetElement
	 * @returns {Element|EventTarget}
	 */
	FastClick.prototype.getTargetElementFromEventTarget = function(eventTarget) {

		// On some older browsers (notably Safari on iOS 4.1 - see issue #56) the event target may be a text node.
		if (eventTarget.nodeType === Node.TEXT_NODE) {
			return eventTarget.parentNode;
		}

		return eventTarget;
	};


	/**
	 * On touch start, record the position and scroll offset.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onTouchStart = function(event) {
		var targetElement, touch, selection;

		// Ignore multiple touches, otherwise pinch-to-zoom is prevented if both fingers are on the FastClick element (issue #111).
		if (event.targetTouches.length > 1) {
			return true;
		}

		targetElement = this.getTargetElementFromEventTarget(event.target);
		touch = event.targetTouches[0];

		if (deviceIsIOS) {

			// Only trusted events will deselect text on iOS (issue #49)
			selection = window.getSelection();
			if (selection.rangeCount && !selection.isCollapsed) {
				return true;
			}

			if (!deviceIsIOS4) {

				// Weird things happen on iOS when an alert or confirm dialog is opened from a click event callback (issue #23):
				// when the user next taps anywhere else on the page, new touchstart and touchend events are dispatched
				// with the same identifier as the touch event that previously triggered the click that triggered the alert.
				// Sadly, there is an issue on iOS 4 that causes some normal touch events to have the same identifier as an
				// immediately preceeding touch event (issue #52), so this fix is unavailable on that platform.
				// Issue 120: touch.identifier is 0 when Chrome dev tools 'Emulate touch events' is set with an iOS device UA string,
				// which causes all touch events to be ignored. As this block only applies to iOS, and iOS identifiers are always long,
				// random integers, it's safe to to continue if the identifier is 0 here.
				if (touch.identifier && touch.identifier === this.lastTouchIdentifier) {
					event.preventDefault();
					return false;
				}

				this.lastTouchIdentifier = touch.identifier;

				// If the target element is a child of a scrollable layer (using -webkit-overflow-scrolling: touch) and:
				// 1) the user does a fling scroll on the scrollable layer
				// 2) the user stops the fling scroll with another tap
				// then the event.target of the last 'touchend' event will be the element that was under the user's finger
				// when the fling scroll was started, causing FastClick to send a click event to that layer - unless a check
				// is made to ensure that a parent layer was not scrolled before sending a synthetic click (issue #42).
				this.updateScrollParent(targetElement);
			}
		}

		this.trackingClick = true;
		this.trackingClickStart = event.timeStamp;
		this.targetElement = targetElement;

		this.touchStartX = touch.pageX;
		this.touchStartY = touch.pageY;

		// Prevent phantom clicks on fast double-tap (issue #36)
		if ((event.timeStamp - this.lastClickTime) < this.tapDelay) {
			event.preventDefault();
		}

		return true;
	};


	/**
	 * Based on a touchmove event object, check whether the touch has moved past a boundary since it started.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.touchHasMoved = function(event) {
		var touch = event.changedTouches[0], boundary = this.touchBoundary;

		if (Math.abs(touch.pageX - this.touchStartX) > boundary || Math.abs(touch.pageY - this.touchStartY) > boundary) {
			return true;
		}

		return false;
	};


	/**
	 * Update the last position.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onTouchMove = function(event) {
		if (!this.trackingClick) {
			return true;
		}

		// If the touch has moved, cancel the click tracking
		if (this.targetElement !== this.getTargetElementFromEventTarget(event.target) || this.touchHasMoved(event)) {
			this.trackingClick = false;
			this.targetElement = null;
		}

		return true;
	};


	/**
	 * Attempt to find the labelled control for the given label element.
	 *
	 * @param {EventTarget|HTMLLabelElement} labelElement
	 * @returns {Element|null}
	 */
	FastClick.prototype.findControl = function(labelElement) {

		// Fast path for newer browsers supporting the HTML5 control attribute
		if (labelElement.control !== undefined) {
			return labelElement.control;
		}

		// All browsers under test that support touch events also support the HTML5 htmlFor attribute
		if (labelElement.htmlFor) {
			return document.getElementById(labelElement.htmlFor);
		}

		// If no for attribute exists, attempt to retrieve the first labellable descendant element
		// the list of which is defined here: http://www.w3.org/TR/html5/forms.html#category-label
		return labelElement.querySelector('button, input:not([type=hidden]), keygen, meter, output, progress, select, textarea');
	};


	/**
	 * On touch end, determine whether to send a click event at once.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onTouchEnd = function(event) {
		var forElement, trackingClickStart, targetTagName, scrollParent, touch, targetElement = this.targetElement;

		if (!this.trackingClick) {
			return true;
		}

		// Prevent phantom clicks on fast double-tap (issue #36)
		if ((event.timeStamp - this.lastClickTime) < this.tapDelay) {
			this.cancelNextClick = true;
			return true;
		}

		if ((event.timeStamp - this.trackingClickStart) > this.tapTimeout) {
			return true;
		}

		// Reset to prevent wrong click cancel on input (issue #156).
		this.cancelNextClick = false;

		this.lastClickTime = event.timeStamp;

		trackingClickStart = this.trackingClickStart;
		this.trackingClick = false;
		this.trackingClickStart = 0;

		// On some iOS devices, the targetElement supplied with the event is invalid if the layer
		// is performing a transition or scroll, and has to be re-detected manually. Note that
		// for this to function correctly, it must be called *after* the event target is checked!
		// See issue #57; also filed as rdar://13048589 .
		if (deviceIsIOSWithBadTarget) {
			touch = event.changedTouches[0];

			// In certain cases arguments of elementFromPoint can be negative, so prevent setting targetElement to null
			targetElement = document.elementFromPoint(touch.pageX - window.pageXOffset, touch.pageY - window.pageYOffset) || targetElement;
			targetElement.fastClickScrollParent = this.targetElement.fastClickScrollParent;
		}

		targetTagName = targetElement.tagName.toLowerCase();
		if (targetTagName === 'label') {
			forElement = this.findControl(targetElement);
			if (forElement) {
				this.focus(targetElement);
				if (deviceIsAndroid) {
					return false;
				}

				targetElement = forElement;
			}
		} else if (this.needsFocus(targetElement)) {

			// Case 1: If the touch started a while ago (best guess is 100ms based on tests for issue #36) then focus will be triggered anyway. Return early and unset the target element reference so that the subsequent click will be allowed through.
			// Case 2: Without this exception for input elements tapped when the document is contained in an iframe, then any inputted text won't be visible even though the value attribute is updated as the user types (issue #37).
			if ((event.timeStamp - trackingClickStart) > 100 || (deviceIsIOS && window.top !== window && targetTagName === 'input')) {
				this.targetElement = null;
				return false;
			}

			this.focus(targetElement);
			this.sendClick(targetElement, event);

			// Select elements need the event to go through on iOS 4, otherwise the selector menu won't open.
			// Also this breaks opening selects when VoiceOver is active on iOS6, iOS7 (and possibly others)
			if (!deviceIsIOS || targetTagName !== 'select') {
				this.targetElement = null;
				event.preventDefault();
			}

			return false;
		}

		if (deviceIsIOS && !deviceIsIOS4) {

			// Don't send a synthetic click event if the target element is contained within a parent layer that was scrolled
			// and this tap is being used to stop the scrolling (usually initiated by a fling - issue #42).
			scrollParent = targetElement.fastClickScrollParent;
			if (scrollParent && scrollParent.fastClickLastScrollTop !== scrollParent.scrollTop) {
				return true;
			}
		}

		// Prevent the actual click from going though - unless the target node is marked as requiring
		// real clicks or if it is in the whitelist in which case only non-programmatic clicks are permitted.
		if (!this.needsClick(targetElement)) {
			event.preventDefault();
			this.sendClick(targetElement, event);
		}

		return false;
	};


	/**
	 * On touch cancel, stop tracking the click.
	 *
	 * @returns {void}
	 */
	FastClick.prototype.onTouchCancel = function() {
		this.trackingClick = false;
		this.targetElement = null;
	};


	/**
	 * Determine mouse events which should be permitted.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onMouse = function(event) {

		// If a target element was never set (because a touch event was never fired) allow the event
		if (!this.targetElement) {
			return true;
		}

		if (event.forwardedTouchEvent) {
			return true;
		}

		// Programmatically generated events targeting a specific element should be permitted
		if (!event.cancelable) {
			return true;
		}

		// Derive and check the target element to see whether the mouse event needs to be permitted;
		// unless explicitly enabled, prevent non-touch click events from triggering actions,
		// to prevent ghost/doubleclicks.
		if (!this.needsClick(this.targetElement) || this.cancelNextClick) {

			// Prevent any user-added listeners declared on FastClick element from being fired.
			if (event.stopImmediatePropagation) {
				event.stopImmediatePropagation();
			} else {

				// Part of the hack for browsers that don't support Event#stopImmediatePropagation (e.g. Android 2)
				event.propagationStopped = true;
			}

			// Cancel the event
			event.stopPropagation();
			event.preventDefault();

			return false;
		}

		// If the mouse event is permitted, return true for the action to go through.
		return true;
	};


	/**
	 * On actual clicks, determine whether this is a touch-generated click, a click action occurring
	 * naturally after a delay after a touch (which needs to be cancelled to avoid duplication), or
	 * an actual click which should be permitted.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onClick = function(event) {
		var permitted;

		// It's possible for another FastClick-like library delivered with third-party code to fire a click event before FastClick does (issue #44). In that case, set the click-tracking flag back to false and return early. This will cause onTouchEnd to return early.
		if (this.trackingClick) {
			this.targetElement = null;
			this.trackingClick = false;
			return true;
		}

		// Very odd behaviour on iOS (issue #18): if a submit element is present inside a form and the user hits enter in the iOS simulator or clicks the Go button on the pop-up OS keyboard the a kind of 'fake' click event will be triggered with the submit-type input element as the target.
		if (event.target.type === 'submit' && event.detail === 0) {
			return true;
		}

		permitted = this.onMouse(event);

		// Only unset targetElement if the click is not permitted. This will ensure that the check for !targetElement in onMouse fails and the browser's click doesn't go through.
		if (!permitted) {
			this.targetElement = null;
		}

		// If clicks are permitted, return true for the action to go through.
		return permitted;
	};


	/**
	 * Remove all FastClick's event listeners.
	 *
	 * @returns {void}
	 */
	FastClick.prototype.destroy = function() {
		var layer = this.layer;

		if (deviceIsAndroid) {
			layer.removeEventListener('mouseover', this.onMouse, true);
			layer.removeEventListener('mousedown', this.onMouse, true);
			layer.removeEventListener('mouseup', this.onMouse, true);
		}

		layer.removeEventListener('click', this.onClick, true);
		layer.removeEventListener('touchstart', this.onTouchStart, false);
		layer.removeEventListener('touchmove', this.onTouchMove, false);
		layer.removeEventListener('touchend', this.onTouchEnd, false);
		layer.removeEventListener('touchcancel', this.onTouchCancel, false);
	};


	/**
	 * Check whether FastClick is needed.
	 *
	 * @param {Element} layer The layer to listen on
	 */
	FastClick.notNeeded = function(layer) {
		var metaViewport;
		var chromeVersion;
		var blackberryVersion;
		var firefoxVersion;

		// Devices that don't support touch don't need FastClick
		if (typeof window.ontouchstart === 'undefined') {
			return true;
		}

		// Chrome version - zero for other browsers
		chromeVersion = +(/Chrome\/([0-9]+)/.exec(navigator.userAgent) || [,0])[1];

		if (chromeVersion) {

			if (deviceIsAndroid) {
				metaViewport = document.querySelector('meta[name=viewport]');

				if (metaViewport) {
					// Chrome on Android with user-scalable="no" doesn't need FastClick (issue #89)
					if (metaViewport.content.indexOf('user-scalable=no') !== -1) {
						return true;
					}
					// Chrome 32 and above with width=device-width or less don't need FastClick
					if (chromeVersion > 31 && document.documentElement.scrollWidth <= window.outerWidth) {
						return true;
					}
				}

				// Chrome desktop doesn't need FastClick (issue #15)
			} else {
				return true;
			}
		}

		if (deviceIsBlackBerry10) {
			blackberryVersion = navigator.userAgent.match(/Version\/([0-9]*)\.([0-9]*)/);

			// BlackBerry 10.3+ does not require Fastclick library.
			// https://github.com/ftlabs/fastclick/issues/251
			if (blackberryVersion[1] >= 10 && blackberryVersion[2] >= 3) {
				metaViewport = document.querySelector('meta[name=viewport]');

				if (metaViewport) {
					// user-scalable=no eliminates click delay.
					if (metaViewport.content.indexOf('user-scalable=no') !== -1) {
						return true;
					}
					// width=device-width (or less than device-width) eliminates click delay.
					if (document.documentElement.scrollWidth <= window.outerWidth) {
						return true;
					}
				}
			}
		}

		// IE10 with -ms-touch-action: none or manipulation, which disables double-tap-to-zoom (issue #97)
		if (layer.style.msTouchAction === 'none' || layer.style.touchAction === 'manipulation') {
			return true;
		}

		// Firefox version - zero for other browsers
		firefoxVersion = +(/Firefox\/([0-9]+)/.exec(navigator.userAgent) || [,0])[1];

		if (firefoxVersion >= 27) {
			// Firefox 27+ does not have tap delay if the content is not zoomable - https://bugzilla.mozilla.org/show_bug.cgi?id=922896

			metaViewport = document.querySelector('meta[name=viewport]');
			if (metaViewport && (metaViewport.content.indexOf('user-scalable=no') !== -1 || document.documentElement.scrollWidth <= window.outerWidth)) {
				return true;
			}
		}

		// IE11: prefixed -ms-touch-action is no longer supported and it's recomended to use non-prefixed version
		// http://msdn.microsoft.com/en-us/library/windows/apps/Hh767313.aspx
		if (layer.style.touchAction === 'none' || layer.style.touchAction === 'manipulation') {
			return true;
		}

		return false;
	};


	/**
	 * Factory method for creating a FastClick object
	 *
	 * @param {Element} layer The layer to listen on
	 * @param {Object} [options={}] The options to override the defaults
	 */
	FastClick.attach = function(layer, options) {
		return new FastClick(layer, options);
	};


	if (typeof define === 'function' && typeof __webpack_require__(4) === 'object' && __webpack_require__(4)) {

		// AMD. Register as an anonymous module.
		define(function() {
			return FastClick;
		});
	} else if (typeof module !== 'undefined' && module.exports) {
		module.exports = FastClick.attach;
		module.exports.FastClick = FastClick;
	} else {
		window.FastClick = FastClick;
	}
}());


(function(){
	let availableModules = {
		ScreenManager: __WEBPACK_IMPORTED_MODULE_0__modules_screen_manager__["a" /* default */]
	};

	let modules = document.currentScript.getAttribute('modules').split(' ');
	window.addEventListener('load',()=>{
		FastClick.attach(document.body);
		for(let module of modules){
			availableModules[module].init();
		}
	});
})();
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(6)(module)))

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = function(originalModule) {
	if(!originalModule.webpackPolyfill) {
		var module = Object.create(originalModule);
		// module.parent = undefined by default
		if(!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function() {
				return module.i;
			}
		});
		Object.defineProperty(module, "exports", {
			enumerable: true,
		});
		module.webpackPolyfill = 1;
	}
	return module;
};


/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__screens_index__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__screens_users__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__screens_schedule__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__screens_section__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__screens_abstracts__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__screens_favourites__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__screens_sponsors__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__screens_organizers__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__screens_science__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__screens_programs__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__screens_rsna__ = __webpack_require__(31);












class ScreenManager{

	static init(){
		let screenManager = new ScreenManager();
		screenManager.add(new __WEBPACK_IMPORTED_MODULE_1__screens_users__["a" /* default */](), 'users');
		screenManager.add(new __WEBPACK_IMPORTED_MODULE_2__screens_schedule__["a" /* default */](), 'schedule');
		screenManager.add(new __WEBPACK_IMPORTED_MODULE_3__screens_section__["a" /* default */](), 'section');
		screenManager.add(new __WEBPACK_IMPORTED_MODULE_5__screens_favourites__["a" /* default */](), 'favourites');
		screenManager.add(new __WEBPACK_IMPORTED_MODULE_4__screens_abstracts__["a" /* default */](), 'abstracts');
		screenManager.add(new __WEBPACK_IMPORTED_MODULE_6__screens_sponsors__["a" /* default */](), 'sponsors');
		screenManager.add(new __WEBPACK_IMPORTED_MODULE_7__screens_organizers__["a" /* default */](), 'organizers');
		screenManager.add(new __WEBPACK_IMPORTED_MODULE_8__screens_science__["a" /* default */](), 'science');
		screenManager.add(new __WEBPACK_IMPORTED_MODULE_9__screens_programs__["a" /* default */](), 'programs');
		screenManager.add(new __WEBPACK_IMPORTED_MODULE_10__screens_rsna__["a" /* default */](), 'rsna');
		screenManager.add(new __WEBPACK_IMPORTED_MODULE_0__screens_index__["a" /* default */](), 'index').activate();
	}

	activateScreen(screen){
		document.querySelectorAll('.active[data-screen]').forEach($screen=> $screen.classList.remove('active'));
		screen.$screen.classList.add('active');
	}

	getScreen(id){
		return this.screens[id];
	}

	constructor(){
		this.screens = {};
	}

	add(screen, id){
		screen.init(document.querySelector('[data-screen='+id+']'), this);
		this.screens[id] = screen;
		return screen;
	}
}
/* harmony export (immutable) */ __webpack_exports__["a"] = ScreenManager;


/***/ }),
/* 8 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_screen__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__html_index_html__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__html_index_html___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__html_index_html__);


/* harmony default export */ __webpack_exports__["a"] = (class extends __WEBPACK_IMPORTED_MODULE_0__src_screen__["a" /* default */] {

	constructor() {
		super();
	}

	initialize(){
		this.$content.innerHTML =__WEBPACK_IMPORTED_MODULE_1__html_index_html___default.a;
	}

	click($source, name) {
		switch (name) {
			case 'menu':
				this.screenManager.getScreen($source.dataset.target).activate().setup();
				break;
			case 'external':
				console.log($source.dataset.url)
				window.open($source.dataset.clickUrl, "_system");
				break;
		}
	}


});

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = "<div class=\"logo\"></div>\n<button data-target=\"schedule\" data-click-name=\"menu\"><i class=\"far fa-list-alt\"></i> Program</button>\n<button data-target=\"users\" data-click-name=\"menu\"><i class=\"fa fa-users\"></i>Előadók</button>\n<button data-target=\"abstracts\" data-click-name=\"external\" data-click-url=\"http://www.mrt2018pecs.hu/upload/MRT_2018_ABSZTRAKT.pdf\"><i class=\"far fa-images\"></i>Absztraktok</button>\n<button data-target=\"sponsors\" data-click-name=\"menu\"><i class=\"far fa-thumbs-up\"></i>Szponzoraink</button>\n<button data-target=\"programs\" data-click-name=\"menu\"><i class=\"fa fa-wine-glass\"></i>Egyéb programok</button>\n<button data-target=\"rsna\" data-click-name=\"menu\"><i class=\"fab fa-app-store-ios\"></i>RSNA Live</button>\n<button data-target=\"organizers\" data-click-name=\"menu\"><i class=\"fa fa-user-ninja\"></i>Szervezők</button>\n<button data-target=\"science\" data-click-name=\"menu\"><i class=\"fa fa-user-tie\"></i>Tudományos bizottság</button>";

/***/ }),
/* 10 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_screen__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__data_users__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__data_presentations__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__src_data_reader__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__src_date_formatter__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__data_rooms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__data_schedule__ = __webpack_require__(1);








/* harmony default export */ __webpack_exports__["a"] = (class extends __WEBPACK_IMPORTED_MODULE_0__src_screen__["a" /* default */] {
	initialize() {
		this.leftButtonIcon = "fas fa-angle-left"
		this.leftButtonClick = (event) => {this.screenManager.getScreen('index').activate(true);}
		this.presentations = new __WEBPACK_IMPORTED_MODULE_3__src_data_reader__["a" /* default */](__WEBPACK_IMPORTED_MODULE_2__data_presentations__["a" /* default */]);
		this.rooms = new __WEBPACK_IMPORTED_MODULE_3__src_data_reader__["a" /* default */](__WEBPACK_IMPORTED_MODULE_5__data_rooms__["a" /* default */]);
		this.setup();
	}

	setup() {
		__WEBPACK_IMPORTED_MODULE_1__data_users__["a" /* default */].forEach(user => {
			if (!user.name) return;
			let $user = document.createElement('div');
			$user.classList.add('user');
			$user.innerHTML = `
				<i class="far fa-user"></i> <span class="name">${user.name}</span>
			`;
			this.$content.appendChild($user);

			user.presentations.forEach(presentationId => {
				let $presentation = document.createElement('div');
				$presentation.classList.add('presentation');
				let presentation = new __WEBPACK_IMPORTED_MODULE_3__src_data_reader__["a" /* default */](__WEBPACK_IMPORTED_MODULE_2__data_presentations__["a" /* default */]).get('fullId', presentationId['sectionId']+'.'+presentationId.id);
				let section = new __WEBPACK_IMPORTED_MODULE_3__src_data_reader__["a" /* default */](__WEBPACK_IMPORTED_MODULE_6__data_schedule__["a" /* default */]).get('id', presentationId.sectionId);
				let room = this.rooms.get('id', section.room);
				$presentation.dataset.sectionId = section.id;
				$presentation.dataset.clickName = 'section';

				$presentation.innerHTML += `
							<div class="title">${presentation.title}</div>
							<span class="from">${new __WEBPACK_IMPORTED_MODULE_4__src_date_formatter__["a" /* default */]().getFullDate(presentation.from)} ${new __WEBPACK_IMPORTED_MODULE_4__src_date_formatter__["a" /* default */]().getTime(presentation.from)} / ${room.name} (${room.number}) terem</span> 
							<div class="authors">${presentation.authors} (${presentation.location})</div>
				`;
				this.$content.appendChild($presentation);

			})
		});
	}

	click($source, name) {
		switch (name) {
			case 'section':
				this.screenManager.getScreen('section').activate().setup({sectionId: $source.dataset.sectionId});
				break;
		}
	}

});

/***/ }),
/* 11 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_screen__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__data_schedule__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__data_rooms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__src_data_reader__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__src_date_formatter__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__src_favourites__ = __webpack_require__(18);







/* harmony default export */ __webpack_exports__["a"] = (class extends __WEBPACK_IMPORTED_MODULE_0__src_screen__["a" /* default */] {


	initialize() {
		this.schedule = new __WEBPACK_IMPORTED_MODULE_3__src_data_reader__["a" /* default */](__WEBPACK_IMPORTED_MODULE_1__data_schedule__["a" /* default */]);
		this.rooms = new __WEBPACK_IMPORTED_MODULE_3__src_data_reader__["a" /* default */](__WEBPACK_IMPORTED_MODULE_2__data_rooms__["a" /* default */]);
		this.leftButtonIcon = "fas fa-angle-left"
		this.leftButtonClick = (event) => {this.screenManager.getScreen('index').activate(true);}
		this.dateformatter = new __WEBPACK_IMPORTED_MODULE_4__src_date_formatter__["a" /* default */]();
	}

	setup() {
		this.$content.innerHTML = '';
		let currentday = null;
		this.schedule.data.forEach(sched => {
			let day = sched.from.getFullYear() + '-' + (sched.from.getMonth() + 1) + '-' + sched.from.getDate();
			if (day !== currentday) {
				currentday = day;
				this.renderDayBar(sched.from);
			}
			switch (sched.type) {
				case 'event':
					this.renderEvent(sched);
					break;
				case 'section':
					this.renderSection(sched);
					break;
			}
		});
		this.updateFavButton();
	}

	renderDayBar(date) {
		let $bar = document.createElement('div');
		$bar.classList.add('day-bar');
		$bar.innerHTML = this.dateformatter.getFullDate(date);
		this.$content.appendChild($bar)
	}

	renderEvent(event) {
		let $bar = document.createElement('div');
		$bar.classList.add('event-bar');
		$bar.innerHTML = this.dateformatter.getTime(event.from) + ' - ' + this.dateformatter.getTime(event.to) + ' ' + event.title;
		this.$content.appendChild($bar);

	}

	renderSection(event) {
		let $bar = document.createElement('div');
		$bar.classList.add('section-bar');
		$bar.dataset.clickName = 'section';
		$bar.dataset.sectionId = event.id;
		let room = this.rooms.get('id', event.room);
		$bar.innerHTML = this.dateformatter.getTime(event.from) + ' - ' + this.dateformatter.getTime(event.to) + ' / ' + room.name + ' (' + room.number + ') terem ' + '<h1>' + event.title + '</h1>';
		this.$content.appendChild($bar);
	}

	click($source, name) {
		switch (name) {
			case 'section':
				this.screenManager.getScreen('section').activate().setup({sectionId: $source.dataset.sectionId});
				break;
		}
	}

	updateFavButton(){
		if(__WEBPACK_IMPORTED_MODULE_5__src_favourites__["a" /* default */].isEmpty()){
			this.rightButtonIcon = "";
			this.rightButtonClick = (event) => {}
		}else{

			this.rightButtonIcon = "fa fa-star";
			this.rightButtonClick = (event) => {this.screenManager.getScreen('favourites').activate(true);}
		}
	}

});

/***/ }),
/* 12 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = ([
	{
		id: 1,
		name: 'DONHOFFER',
		number: 3
	},
	{
		id: 2,
		name: 'FLERKÓ',
		number: 4
	},
	{
		id: 3,
		name: 'LISSÁK',
		number: 2
	},
	{
		id: 4,
		name: 'CHOLNOKY',
		number: 1
	}
]);


/***/ }),
/* 13 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_screen__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__data_schedule__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__src_date_formatter__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__data_presentations__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__src_data_reader__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__html_section_html__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__html_section_html___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__html_section_html__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__data_rooms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__src_favourites__ = __webpack_require__(18);









/* harmony default export */ __webpack_exports__["a"] = (class extends __WEBPACK_IMPORTED_MODULE_0__src_screen__["a" /* default */] {


	initialize() {
		this.schedule = new __WEBPACK_IMPORTED_MODULE_4__src_data_reader__["a" /* default */](__WEBPACK_IMPORTED_MODULE_1__data_schedule__["a" /* default */]);
		this.presentations = new __WEBPACK_IMPORTED_MODULE_4__src_data_reader__["a" /* default */](__WEBPACK_IMPORTED_MODULE_3__data_presentations__["a" /* default */])
		this.leftButtonIcon = "fas fa-angle-left"
		this.leftButtonClick = (event) => {this.screenManager.getScreen('schedule').activate(true);}
		this.rooms = new __WEBPACK_IMPORTED_MODULE_4__src_data_reader__["a" /* default */](__WEBPACK_IMPORTED_MODULE_6__data_rooms__["a" /* default */]);

		this.dateformatter = new __WEBPACK_IMPORTED_MODULE_2__src_date_formatter__["a" /* default */]();
		this.$content.innerHTML = __WEBPACK_IMPORTED_MODULE_5__html_section_html___default.a;
	}

	setup(options) {
		this.favourites = __WEBPACK_IMPORTED_MODULE_7__src_favourites__["a" /* default */].get();
		this.section = this.schedule.find({id: options.sectionId})[0];
		let presentations = this.presentations.getAll('sectionId', this.section.id);



		let room = this.rooms.get('id', this.section.room)
		this.$content.querySelector('.date').innerHTML = new __WEBPACK_IMPORTED_MODULE_2__src_date_formatter__["a" /* default */]().getFullDate(this.section.from);
		this.$content.querySelector('.time').innerHTML = new __WEBPACK_IMPORTED_MODULE_2__src_date_formatter__["a" /* default */]().getTime(this.section.from) + ' - ' + new __WEBPACK_IMPORTED_MODULE_2__src_date_formatter__["a" /* default */]().getTime(this.section.to);
		this.$content.querySelector('.location').innerHTML = room.name + ' ('+room.number+') terem';
		this.$content.querySelector('.section-title').innerHTML = this.section.title;
		this.$content.querySelector('.presentations').innerHTML = '';
		presentations.forEach(presentation=>{
			this.addPresentation(presentation);
		});
		this.updateFavButton();
	}

	addPresentation(presentation){
		let $presentation = document.createElement('div');
		$presentation.dataset.clickName = 'fav-toggle';
		$presentation.dataset.fullId = presentation.fullId;
		let icon = this.favourites[presentation.fullId] ? 'fa' : 'far'
		$presentation.innerHTML = `
			<i class="${icon} fa-star"></i>
			<div class="title"><span class="from">${new __WEBPACK_IMPORTED_MODULE_2__src_date_formatter__["a" /* default */]().getTime(presentation.from)}</span> ${presentation.title}</div>
			<div class="authors">${presentation.authors} (${presentation.location})</div>
		
		`;
		this.$content.querySelector('.presentations').appendChild($presentation);
	}

	click($source, name) {
		switch (name) {
			case 'fav-toggle':
				let added = __WEBPACK_IMPORTED_MODULE_7__src_favourites__["a" /* default */].toggle($source.dataset.fullId);

				if(added){
					$source.querySelector('.fa-star').classList.remove('far');
					$source.querySelector('.fa-star').classList.add('fa');
				}else{
					$source.querySelector('.fa-star').classList.remove('fa');
					$source.querySelector('.fa-star').classList.add('far');
				}

				this.updateFavButton();
				break;
		}
	}

	updateFavButton(){
		if(__WEBPACK_IMPORTED_MODULE_7__src_favourites__["a" /* default */].isEmpty()){
			this.rightButtonIcon = "";
			this.rightButtonClick = (event) => {}
		}else{
			this.rightButtonIcon = "fa fa-star";
			this.rightButtonClick = (event) => {this.screenManager.getScreen('favourites').activate(true);}
		}
	}


});

/***/ }),
/* 14 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
let data = [
	{
		"id": "1",
		"sectionId": "1.1",
		"from": "2018-06-21T10:30",
		"to": "2018-06-21T10:50",
		"title": "Hibrid képalkotás",
		"performer": "Pávics László",
		"authors": "Pávics László, Besenyi Zsuzsanna",
		"location": "Szeged"
	},
	{
		"id": "2",
		"sectionId": "1.1",
		"from": "2018-06-21T10:50",
		"to": "2018-06-21T11:00",
		"title": "Hibrid képalkotás a spodylarthropathiák diagnosztikájában",
		"performer": "Besenyi Zsuzsanna",
		"authors": "Besenyi Zsuzsanna, Bakos Annamária, Urbán Szabolcs, Hemelein Rita, Kovács László, Pávics László",
		"location": "Szeged"
	},
	{
		"id": "3",
		"sectionId": "1.1",
		"from": "2018-06-21T11:00",
		"to": "2018-06-21T11:10",
		"title": "Teljes test CT vizsgálattal szerzett kezdeti tapasztalataink myeloma multiplexes betegeknél",
		"performer": "Séllei Ágnes",
		"authors": "Séllei Ágnes, Komáromi Klaudia, Modok Szabolcs, Lengyel Zsuzsanna, Palkó András",
		"location": "Szeged"
	},
	{
		"id": "4",
		"sectionId": "1.1",
		"from": "2018-06-21T11:10",
		"to": "2018-06-21T11:20",
		"title": "A modern képalkotó vizsgálatok alkalmazásának irányelvei myeloma multiplexben",
		"performer": "Győri Gabriella",
		"authors": "Győri Gabriella, Körösmezey Gábor, Gaál-Weisinger Júlia, Tárkányi Ilona, Nagy Zsolt György, Demeter Judit",
		"location": "Budapest"
	},
	{
		"id": "5",
		"sectionId": "1.1",
		"from": "2018-06-21T11:20",
		"to": "2018-06-21T11:30",
		"title": "Hibrid képalkotás a gyakorlatban – tapasztalatok egy SPECT/CT/PET készülékkel",
		"performer": "Urbán Szabolcs",
		"authors": "Urbán Szabolcs, Polanek Tünde, Sipka Gábor, Farkas István, Bakos Annamária, Besenyi Zsuzsanna, Pávics László",
		"location": "Szeged"
	},
	{
		"id": "6",
		"sectionId": "1.1",
		"from": "2018-06-21T11:30",
		"to": "2018-06-21T11:40",
		"title": "18F-FDG PET/CT szerepe a nagyérvasculitisek diagnosztikájában",
		"performer": "Bakos Annamária",
		"authors": "Bakos Annamária, Besenyi Zsuzsanna, Urbán Szabolcs, Farkas István, Sipka Gábor, Hemelein Rita, Kovács László, Pávics László",
		"location": "Szeged"
	},
	{
		"id": "7",
		"sectionId": "1.1",
		"from": "2018-06-21T11:40",
		"to": "2018-06-21T11:50",
		"title": "Cardiovascularis intervenciókat követő gyulladásos szövődmények vizsgálata FDG-PET/CT-VEL",
		"performer": "Farkas István",
		"authors": "Farkas István, Besenyi Zsuzsanna, Sághy László, Bakos Annamária, Sipka Gábor, Pávics László",
		"location": "Szeged"
	},
	{
		"id": "8",
		"sectionId": "1.1",
		"from": "2018-06-21T11:50",
		"to": "2018-06-21T12:00",
		"title": "Szomatosztatin receptor SPECT/CT jelentősége neuroendokrin tumorokban",
		"performer": "Sipka Gábor",
		"authors": "Sipka Gábor, Besenyi Zsuzsanna, Bakos Annamária, Farkas István, Pávics László",
		"location": "Szeged"
	},
	{
		"id": "9",
		"sectionId": "1.1",
		"from": "2018-06-21T12:00",
		"to": "2018-06-21T12:10",
		"title": "Nagy Felbontású Agyi SPECT Leképezés a Technológiai Lehetőségek Tükrében",
		"performer": "Kári Béla",
		"authors": "Kári Béla, Wirth András, Hesz Gábor, Bükki Tamás, Fegyvári András, Czibor Sándor, Dabasi Gabriella, Györke Tamás",
		"location": "Budapest"
	},
	{
		"id": "10",
		"sectionId": "1.1",
		"from": "2018-06-21T12:10",
		"to": "2018-06-21T12:20",
		"title": "AIDS-betegségben előforduló szövődmények képalkotó vizsgálata",
		"performer": "Sári Áron",
		"authors": "Sári Áron, Arany Andrea Szilvia, Győri Gabriella, Járay Anna, Szlávik János",
		"location": "Budapest"
	},
	{
		"id": "11",
		"sectionId": "1.1",
		"from": "2018-06-21T12:20",
		"to": "2018-06-21T12:30",
		"title": "FDG PETCT Reporting: Pearls and Pitfalls",
		"performer": "Paul Duffy",
		"authors": "Paul Duffy",
		"location": "Glasgow"
	},
	{
		"id": "1",
		"sectionId": "1.2",
		"from": "2018-06-21T11:00",
		"to": "2018-06-21T11:15",
		"title": "Aktuális trendek a verőérbetegségek intervenciós radiológiai kezelésében",
		"performer": "Horváth Gábor",
		"authors": "Horváth Gábor",
		"location": "Kronach, Németország"
	},
	{
		"id": "2",
		"sectionId": "1.2",
		"from": "2018-06-21T11:15",
		"to": "2018-06-21T11:25",
		"title": "Az iliaca kissing in-stent restenosisok új, eddig nem ismert rizikófaktora",
		"performer": "Vértes Miklós",
		"authors": "Vértes Miklós, Juhász Ildikó Zsófia, Nguyen Tin Dat, Nemes Balázs, Hüttl Kálmán, Dósa Edit",
		"location": "Budapest"
	},
	{
		"id": "3",
		"sectionId": "1.2",
		"from": "2018-06-21T11:25",
		"to": "2018-06-21T11:35",
		"title": "Akut ischaemiás stroke endovascularis ellátásának jellegzetességei a nagy ér occlusio lokalizációja alapján az Országos Klinikai Idegtudományi Intézetben",
		"performer": "Kis Balázs",
		"authors": "Kis Balázs, Orosz Emma, Nagy András, Nardai Sándor, Gubucz István, Berentei Zsolt, Szikora István",
		"location": "Budapest"
	},
	{
		"id": "4",
		"sectionId": "1.2",
		"from": "2018-06-21T11:35",
		"to": "2018-06-21T11:45",
		"title": "Az infrarenalis aorta stenosisok endovascularis terápiájának eredményessége",
		"performer": "Bérczi Ákos",
		"authors": "Bérczi Ákos, Vértes Miklós, Nemes Balázs, Bérczi Viktor, Hüttl Kálmán, Dósa Edit",
		"location": "Budapest"
	},
	{
		"id": "5",
		"sectionId": "1.2",
		"from": "2018-06-21T11:45",
		"to": "2018-06-21T11:55",
		"title": "Traumás intracraniális artéria carotis interna dissectio intervenciós kezelése",
		"performer": "Oláh Csaba",
		"authors": "Oláh Csaba, Tamáska Péter, Lázár István",
		"location": "Miskolc"
	},
	{
		"id": "6",
		"sectionId": "1.2",
		"from": "2018-06-21T11:55",
		"to": "2018-06-21T12:05",
		"title": "Horner-triásszal jelentkező arteria carotis dissectio es pseudoaneurysma - Esetismertetés",
		"performer": "Hernyes Anita",
		"authors": "Hernyes Anita, Daniel Santirso, Forgó Bianka, Szalontai László, Tárnoki Ádám Domonkos, Tárnoki Dávid László, Garami Zsolt",
		"location": "Budapest, Houston"
	},
	{
		"id": "7",
		"sectionId": "1.2",
		"from": "2018-06-21T12:05",
		"to": "2018-06-21T12:15",
		"title": "Egy szokatlan hátterű arteria carotis communis pseudoaneurysma esete",
		"performer": "Jermendy Ádám",
		"authors": "Jermendy Ádám, Sótonyi Péter, Tóth Attila, Hidi László, Merkely Béla",
		"location": "Budapest"
	},
	{
		"id": "8",
		"sectionId": "1.2",
		"from": "2018-06-21T12:15",
		"to": "2018-06-21T12:25",
		"title": "A carotis stentelés során jelentkező cardiovascularis instabilitás és a kialakult ischaemiás léziók kiterjedtsége közötti összefüggés vizsgálata.",
		"performer": "Csizmadia Sándor",
		"authors": "Csizmadia Sándor, Kaszás Zsófia, Klucsai Róbert, Vörös Erika",
		"location": "Szeged"
	},
	{
		"id": "9",
		"sectionId": "1.2",
		"from": "2018-06-21T12:25",
		"to": "2018-06-21T12:35",
		"title": "Transpedalis behatolással végzett alsó végtagi intervenciók",
		"performer": "Csobay-Novák Csaba",
		"authors": "Csobay-Novák Csaba, Nemes Balázs, Dósa Edit, Sarkadi Hunor, Hüttl Kálmán",
		"location": "Budapest"
	},
	{
		"id": "1",
		"sectionId": "1.3",
		"from": "2018-06-21T10:30",
		"to": "2018-06-21T10:45",
		"title": "Bevezető esetek",
		"performer": "Tárnoki Ádám",
		"authors": "Tárnoki Ádám",
		"location": "Budapest"
	},
	{
		"id": "2",
		"sectionId": "1.3",
		"from": "2018-06-21T10:45",
		"to": "2018-06-21T11:30",
		"title": "Rezidensek esetbemutatásai",
		"performer": "Tárnoki Dávid",
		"authors": "Tárnoki Dávid",
		"location": "Budapest"
	},
	{
		"id": "3",
		"sectionId": "1.3",
		"from": "2018-06-21T11:30",
		"to": "2018-06-21T11:40",
		"title": "Hogyan pályázzunk?",
		"performer": "Tárnoki Ádám",
		"authors": "Tárnoki Ádám",
		"location": "Budapest"
	},
	{
		"id": "4",
		"sectionId": "1.3",
		"from": "2018-06-21T11:40",
		"to": "2018-06-21T11:50",
		"title": "Hogyan készítsünk prezentációt?",
		"performer": "Karlinger Kinga",
		"authors": "Karlinger Kinga",
		"location": "Budapest"
	},
	{
		"id": "5",
		"sectionId": "1.3",
		"from": "2018-06-21T11:50",
		"to": "2018-06-21T12:00",
		"title": "ESOR élménybeszámoló",
		"performer": "Tárnoki Dávid",
		"authors": "Tárnoki Dávid",
		"location": "Budapest"
	},
	{
		"id": "6",
		"sectionId": "1.3",
		"from": "2018-06-21T12:00",
		"to": "2018-06-21T12:10",
		"title": "Pályázati és kutatási lehetőségek",
		"performer": "Lánczi Levente",
		"authors": "Lánczi Levente",
		"location": "Debrecen"
	},
	{
		"id": "7",
		"sectionId": "1.3",
		"from": "2018-06-21T12:10",
		"to": "2018-06-21T12:20",
		"title": "Q&A Workshop",
		"performer": "Lánczi Levente",
		"authors": "Lánczi Levente",
		"location": "Debrecen"
	},
	{
		"id": "8",
		"sectionId": "1.3",
		"from": "2018-06-21T12:20",
		"to": "2018-06-21T12:35",
		"title": "Beton anyagok értékelése humándiagnosztikai képalkotó módszerekkel",
		"performer": "Földes Tamás",
		"authors": "Földes Tamás, Lubloy Éva",
		"location": "Szolnok, Budapest"
	},
	{
		"id": "1",
		"sectionId": "1.4",
		"from": "2018-06-21T14:00",
		"to": "2018-06-21T14:15",
		"title": "ILD betegségek csoportjai",
		"performer": "Tárnoki Dávid László",
		"authors": "Tárnoki Dávid László",
		"location": "Budapest"
	},
	{
		"id": "2",
		"sectionId": "1.4",
		"from": "2018-06-21T14:15",
		"to": "2018-06-21T14:30",
		"title": "IPF és klinikai vonatkozásai",
		"performer": "Monostori Zsuzsanna",
		"authors": "Monostori Zsuzsanna",
		"location": "Budapest"
	},
	{
		"id": "3",
		"sectionId": "1.4",
		"from": "2018-06-21T14:30",
		"to": "2018-06-21T14:45",
		"title": "NSIP minta és ami mögötte lehet",
		"performer": "Kerpel-Fronius Anna",
		"authors": "Kerpel-Fronius Anna",
		"location": "Budapest"
	},
	{
		"id": "4",
		"sectionId": "1.4",
		"from": "2018-06-21T14:45",
		"to": "2018-06-21T15:00",
		"title": "Fibrosissal járó betegségek differenciáldiagnosztikája",
		"performer": "Balázs György",
		"authors": "Balázs György",
		"location": "Budapest"
	},
	{
		"id": "5",
		"sectionId": "1.4",
		"from": "2018-06-21T15:00",
		"to": "2018-06-21T15:10",
		"title": "Tüdőgóc(ok)! Most mit csináljak?",
		"performer": "Székely András",
		"authors": "Székely András, Kerpel-Fronius Anna, Bágyi Péter",
		"location": "Debrecen-Budapest"
	},
	{
		"id": "6",
		"sectionId": "1.4",
		"from": "2018-06-21T15:10",
		"to": "2018-06-21T15:20",
		"title": "Nodularis tüdőbetegségek képalkotása: újdonságok 2018-ban, melyekre figyelnünk kell",
		"performer": "Tárnoki Ádám Domonkos",
		"authors": "Tárnoki Ádám Domonkos, Tárnoki Dávid László, Karlinger Kinga",
		"location": "Budapest"
	},
	{
		"id": "7",
		"sectionId": "1.4",
		"from": "2018-06-21T15:20",
		"to": "2018-06-21T15:30",
		"title": "Képes összefoglaló a CT-vezérlet mellkasi biopsziákról - tippek és trükkök",
		"performer": "Futácsi Balázs",
		"authors": "Futácsi Balázs, Magyar Péter, Gál Magdolna, Bánsághi Zoltán",
		"location": "Budapest"
	},
	{
		"id": "8",
		"sectionId": "1.4",
		"from": "2018-06-21T15:30",
		"to": "2018-06-21T15:40",
		"title": "CT vezérelt percutan transthoracalis tűbiposzia eredményei a Debreceni Klinikán",
		"performer": "Nagy Edit Boglárka",
		"authors": "Nagy Edit Boglárka, Csubák Dávid, Belán Ivett, Veisz Richárd, Tóth Judit",
		"location": "Debrecen"
	},
	{
		"id": "9",
		"sectionId": "1.4",
		"from": "2018-06-21T15:40",
		"to": "2018-06-21T15:50",
		"title": "CT-vezérelt transthoracalis vastagtű-biopsziák szövődményeinek gyakorisága és súlyossága",
		"performer": "Kovács Anita",
		"authors": "Kovács Anita, Milassin Péter, Orbán Krisztina, Pálföldi Regina, Palkó András",
		"location": "Szeged, Deszk"
	},
	{
		"id": "10",
		"sectionId": "1.4",
		"from": "2018-06-21T15:50",
		"to": "2018-06-21T16:00",
		"title": "Transthoracalis biopsziák sikeressége és szövődményei",
		"performer": "Orbán Vince",
		"authors": "Orbán Vince, Villányi Réka, Magyar Péter, Bánsághi Zoltán, Futácsi Balázs",
		"location": "Budapest"
	},
	{
		"id": "1",
		"sectionId": "1.5",
		"from": "2018-06-21T14:00",
		"to": "2018-06-21T14:25",
		"title": "Szakmai megjelenés a digitális térben – Antiszociális média, avagy a kutya sem követ",
		"performer": "Zsiros László Róbert",
		"authors": "Zsiros László Róbert",
		"location": "Budapest"
	},
	{
		"id": "2",
		"sectionId": "1.5",
		"from": "2018-06-21T14:25",
		"to": "2018-06-21T14:45",
		"title": "Gamifikáció a graduális és postgraduális képzésben – Tanulás és motiváció",
		"performer": "Damsa Andrei",
		"authors": "Damsa Andrei",
		"location": "Pécs"
	},
	{
		"id": "3",
		"sectionId": "1.5",
		"from": "2018-06-21T14:45",
		"to": "2018-06-21T15:10",
		"title": "A Radiológiai Képszerkesztés Alapjai – DICOM Szelídítés Alapfokon",
		"performer": "Botz Bálint",
		"authors": "Botz Bálint, Járay Ákos",
		"location": "Pécs"
	},
	{
		"id": "4",
		"sectionId": "1.5",
		"from": "2018-06-21T15:10",
		"to": "2018-06-21T15:30",
		"title": "Oktatási célú videók szerkesztése egyszerűen – Videók a boncasztalon",
		"performer": "Zsiros László Róbert",
		"authors": "Zsiros László Róbert",
		"location": "Budapest"
	},
	{
		"id": "5",
		"sectionId": "1.5",
		"from": "2018-06-21T15:30",
		"to": "2018-06-21T15:40",
		"title": "Radiológia oktatás interaktívan – megvalósítás és gyakorlati tapasztalatok",
		"performer": "Olajos Eszter Ajna",
		"authors": "Olajos Eszter Ajna",
		"location": "Budapest"
	},
	{
		"id": "6",
		"sectionId": "1.5",
		"from": "2018-06-21T15:40",
		"to": "2018-06-21T15:50",
		"title": "3D nyomtatás és virtuális tervezés az egészségügyben",
		"performer": "Varga Péter",
		"authors": "Varga Péter, Maróti Péter, Nyitrai Miklós, Jancsó Gábor, Gasz Balázs",
		"location": "Pécs"
	},
	{
		"id": "7",
		"sectionId": "1.5",
		"from": "2018-06-21T15:50",
		"to": "2018-06-21T16:00",
		"title": "Ultrahang a klinikumban: ahogy eddig kevesen látták",
		"performer": "Klimaj Zoltán",
		"authors": "Klimaj Zoltán, Kondákor B., Németh B., Jakab Zs.",
		"location": "Budapest"
	},
	{
		"id": "1",
		"sectionId": "1.6",
		"from": "2018-06-21T14:00",
		"to": "2018-06-21T14:20",
		"title": "Acute female pelvis",
		"performer": "Derchi",
		"authors": "Derchi, Lorenzo",
		"location": "Genova, Italy"
	},
	{
		"id": "2",
		"sectionId": "1.6",
		"from": "2018-06-21T14:20",
		"to": "2018-06-21T14:40",
		"title": "Ultrahang jelentősége az endometriosis kivizsgálásánál",
		"performer": "Koppán Miklós",
		"authors": "Koppán Miklós",
		"location": "Pécs"
	},
	{
		"id": "3",
		"sectionId": "1.6",
		"from": "2018-06-21T14:40",
		"to": "2018-06-21T15:00",
		"title": "MRI jelentősége az endometriosis kivizsgálásánál",
		"performer": "Nagy Gyöngyi",
		"authors": "Nagy Gyöngyi",
		"location": "Zalaegerszeg"
	},
	{
		"id": "4",
		"sectionId": "1.6",
		"from": "2018-06-21T15:00",
		"to": "2018-06-21T15:10",
		"title": "A myoma embolizáció hatékonysága, kockázatai, eredményessége - tapasztalataink 580 eset kapcsán",
		"performer": "Bérczi Viktor",
		"authors": "Bérczi Viktor, Tóth Ambrus, Kalina Ildikó, Valcseva Éva, Kozics Dóra, Ács Nándor, Tömösváry Zoltán",
		"location": "Budapest"
	},
	{
		"id": "5",
		"sectionId": "1.6",
		"from": "2018-06-21T15:10",
		"to": "2018-06-21T15:20",
		"title": "A strain elasztrográfia szerepe a férfi meddőségi ultrahang kivizsgálásban",
		"performer": "Karczagi Lilla",
		"authors": "Karczagi Lilla, Fejes Zsuzsanna",
		"location": "Szeged"
	},
	{
		"id": "6",
		"sectionId": "1.6",
		"from": "2018-06-21T15:20",
		"to": "2018-06-21T15:30",
		"title": "Kezdeti tapasztalatok a 99mTc-PSMA-SPECT/CT-vel prosztatarákos betegekben",
		"performer": "Farkas István",
		"authors": "Farkas István, Besenyi Zsuzsanna, Maráz Anikó, Bajory Zoltán, Palkó András, Sipka Gábor, Pávics László",
		"location": "Szeged"
	},
	{
		"id": "7",
		"sectionId": "1.6",
		"from": "2018-06-21T15:30",
		"to": "2018-06-21T15:40",
		"title": "Az ortopédiai fém műtermék redukciós algoritmus (O-MAR) hatékonysága kismedencei szervek CT-vizsgálata során",
		"performer": "Kaposi Novák Pál",
		"authors": "Kaposi Novák Pál, Taewoong Youn, Fejes Bernadett, Magyar Péter, Bérczi Viktor",
		"location": "Budapest"
	},
	{
		"id": "8",
		"sectionId": "1.6",
		"from": "2018-06-21T15:40",
		"to": "2018-06-21T15:50",
		"title": "Az MR szerepe a prosztatarák diagnosztikájában",
		"performer": "Kalina Ildikó",
		"authors": "Kalina Ildikó, Fejér Bence, Pölöskei Gergely, Kovács Dániel, Huszár Andor, Wolf Tamás, Borka Katalin, Szűcs Miklós",
		"location": "Budapest"
	},
	{
		"id": "1",
		"sectionId": "2.1",
		"from": "2018-06-22T8:20",
		"to": "2018-06-22T8:50",
		"title": "Neurális Hálók: Elmélet és Gyakorlat",
		"performer": "Dombi Gergely",
		"authors": "Dombi Gergely",
		"location": "Budapest"
	},
	{
		"id": "2",
		"sectionId": "2.1",
		"from": "2018-06-22T8:50",
		"to": "2018-06-22T9:20",
		"title": "Deep learning in clinical practice",
		"performer": "Joris Wakkie",
		"authors": "Joris Wakkie",
		"location": "Amszterdam, Hollandia"
	},
	{
		"id": "3",
		"sectionId": "2.1",
		"from": "2018-06-22T9:20",
		"to": "2018-06-22T9:45",
		"title": "Mesterséges intelligencia-módszerek szerepe az emlővizsgálatokban",
		"performer": "Kecskeméthy Péter",
		"authors": "Kecskeméthy Péter",
		"location": "London, Anglia"
	},
	{
		"id": "4",
		"sectionId": "2.1",
		"from": "2018-06-22T9:45",
		"to": "2018-06-22T9:55",
		"title": "Mesterséges intelligencia az ízületi radiológiában (Siemens Presentation)",
		"performer": "Gál Andor Viktor",
		"authors": "Gál Andor Viktor, Meszlényi Regina, Edőcs József",
		"location": "Budapest, Győr"
	},
	{
		"id": "5",
		"sectionId": "2.1",
		"from": "2018-06-22T9:55",
		"to": "2018-06-22T10:10",
		"title": "Artificial Intelligence and Future Technologies\n",
		"performer": "Samsung",
		"authors": "Samsung Presentation: Artificial Intelligence and Future Technologies",
		"location": "Samsung"
	},
	{
		"id": "6",
		"sectionId": "2.1",
		"from": "2018-06-22T10:10",
		"to": "2018-06-22T10:20",
		"title": "Mesterséges intelligencia bonus és/vagy malus? Az emberi hülyeség új dimenziói is kezdődhetnek?",
		"performer": "Lombay Béla",
		"authors": "Lombay Béla",
		"location": "Miskolc"
	},
	{
		"id": "1",
		"sectionId": "2.2",
		"from": "2018-06-22T8:30",
		"to": "2018-06-22T8:50",
		"title": "A gócos májelváltozások korszerű MR-diagnosztikája",
		"performer": "Palkó András",
		"authors": "Palkó András",
		"location": "Szeged"
	},
	{
		"id": "2",
		"sectionId": "2.2",
		"from": "2018-06-22T8:50",
		"to": "2018-06-22T9:10",
		"title": "Intervenciós radiológia májtumorokban - a magyar módszer",
		"performer": "Doros Attila",
		"authors": "Doros Attila",
		"location": "Budapest"
	},
	{
		"id": "3",
		"sectionId": "2.2",
		"from": "2018-06-22T9:10",
		"to": "2018-06-22T9:30",
		"title": "A májsebész kérdései és vágyai a diagnosztika során",
		"performer": "Papp András",
		"authors": "Papp András",
		"location": "Pécs"
	},
	{
		"id": "4",
		"sectionId": "2.2",
		"from": "2018-06-22T9:30",
		"to": "2018-06-22T9:50",
		"title": "Májspecifikus MR vizsgálat az Uzsoki Kórházban – válogatás differenciál diagnosztikai kihívást jelentő eseteinkből",
		"performer": "Demjén Boglárka",
		"authors": "Demjén Boglárka",
		"location": "Budapest"
	},
	{
		"id": "5",
		"sectionId": "2.2",
		"from": "2018-06-22T9:50",
		"to": "2018-06-22T10:00",
		"title": "A point shear-wave elasztográfia eredménysessége a májfibrózis stádiumainak diagnosztikájában",
		"performer": "Kaposi Novák Pál",
		"authors": "Kaposi Novák Pál, Kucsa András, Abonyi Margit, Bérczi Viktor",
		"location": "Budapest"
	},
	{
		"id": "6",
		"sectionId": "2.2",
		"from": "2018-06-22T10:00",
		"to": "2018-06-22T10:10",
		"title": "A DEB-TACE és a radioembolizáció helye a kolorektális daganatok májáttéteinek ellátásában.",
		"performer": "Bánsághi Zoltán",
		"authors": "Bánsághi Zoltán",
		"location": "Budapest"
	},
	{
		"id": "7",
		"sectionId": "2.2",
		"from": "2018-06-22T10:10",
		"to": "2018-06-22T10:20",
		"title": "Rupturált májtumor minimál invazív ellátása",
		"performer": "Kónya Júlia Anna",
		"authors": "Kónya Júlia Anna, Varga Márk",
		"location": "Győr"
	},
	{
		"id": "1",
		"sectionId": "2.3",
		"from": "2018-06-22T8:30",
		"to": "2018-06-22T8:50",
		"title": "Izomsérülések diagnosztikája - képalkotás traumatológus szemmel. Olyan esetek bemutatása, amikor a képalkotó diagnosztika alapvető fontosságú a kezelési terv felállításához.",
		"performer": "Nőt László Gergely",
		"authors": "Nőt László Gergely",
		"location": "Pécs"
	},
	{
		"id": "2",
		"sectionId": "2.3",
		"from": "2018-06-22T8:50",
		"to": "2018-06-22T9:10",
		"title": "Izomsérülések ultrahang vizsgálata",
		"performer": "Farbaky Zsófia",
		"authors": "Farbaky Zsófia",
		"location": "Budapest"
	},
	{
		"id": "3",
		"sectionId": "2.3",
		"from": "2018-06-22T9:10",
		"to": "2018-06-22T9:30",
		"title": "Izomsérülések MR diagnosztikája",
		"performer": "Hetényi Szabolcs",
		"authors": "Hetényi Szabolcs",
		"location": "Budapest"
	},
	{
		"id": "4",
		"sectionId": "2.3",
		"from": "2018-06-22T9:30",
		"to": "2018-06-22T9:40",
		"title": "Az MR arthrographia szerepe a glenohumeralis instabilitás diagnosztikájában",
		"performer": "Soltész Judit",
		"authors": "Soltész Judit, Kostyál László",
		"location": "Miskolc"
	},
	{
		"id": "5",
		"sectionId": "2.3",
		"from": "2018-06-22T9:40",
		"to": "2018-06-22T9:50",
		"title": "Keresztszalag pótlás után...",
		"performer": "Tasnádi Tünde",
		"authors": "Tasnádi Tünde, Barta Szabolcs, Tállay András",
		"location": "Békéscsaba, Budapest"
	},
	{
		"id": "6",
		"sectionId": "2.3",
		"from": "2018-06-22T9:50",
		"to": "2018-06-22T10:00",
		"title": "A térdízület az arthroscopia és az MR vizsgálat szemszögéből",
		"performer": "Brzózka Ádám",
		"authors": "Brzózka Ádám, Komáromi Klaudia, Kovács Milán, Polyák Ilona, Palkó András",
		"location": "Szeged"
	},
	{
		"id": "7",
		"sectionId": "2.3",
		"from": "2018-06-22T10:00",
		"to": "2018-06-22T10:10",
		"title": "Lumbalis discus degeneratio kapcsolata a mtDNS kópiaszámmal és a telomér hosszúsággal",
		"performer": "Tárnoki Dávid",
		"authors": "Tárnoki Dávid, Melicher Dóra, Illés Anett, Falus András, Molnár Mária, Szily Marcell, Kovács Dániel, Forgó Bianka, Bérczi Viktor, Kostyál László, Tárnoki Adam, Oláh Csaba",
		"location": "Budapest, Miskolc"
	},
	{
		"id": "8",
		"sectionId": "2.3",
		"from": "2018-06-22T10:10",
		"to": "2018-06-22T10:20",
		"title": "Vállsérülések stádiumbeosztásának összehasonlítása hagyományos röntgendiagnosztika és CT vizsgálat során",
		"performer": "Bánáti Laura",
		"authors": "Bánáti Laura, Palkó András, Polyák Ilona",
		"location": "Szeged"
	},
	{
		"id": "1",
		"sectionId": "2.4",
		"from": "2018-06-22T08:30",
		"to": "2018-06-22T08:50",
		"title": "New developments in vascular ultrasound",
		"performer": "Brkljacic, Boris",
		"authors": "Brkljacic, Boris",
		"location": "Zagreb, Croatia"
	},
	{
		"id": "2",
		"sectionId": "2.4",
		"from": "2018-06-22T08:50",
		"to": "2018-06-22T09:00",
		"title": "A carotis ultrahang és transcranialis doppler metodikák: a houstoni és a magyar protokoll összevetése",
		"performer": "Szalontai László",
		"authors": "Szalontai László, Hernyes Anita, Forgó Bianka, Bérczi Viktor, Tárnoki Dávid László, Tárnoki Ádám Domonkos, Garami Zsolt",
		"location": "Budapest, Houston"
	},
	{
		"id": "3",
		"sectionId": "2.4",
		"from": "2018-06-22T09:00",
		"to": "2018-06-22T09:10",
		"title": "A carotis és femoralis atherosclerosis markereinek összefüggése a telomérhosszal és a mtDNS kópiaszámmal",
		"performer": "Janositz Gréta",
		"authors": "Janositz Gréta, Melicher Dóra, Illés Anett, Gyulay Kata, Jokkel Zsófia, Zsupos Rebeka, Bérczi Viktor, Szabó Helga, Hernyes Anita, Tárnoki Dávid L., Tárnoki Ádám D.",
		"location": "Budapest"
	},
	{
		"id": "4",
		"sectionId": "2.4",
		"from": "2018-06-22T09:10",
		"to": "2018-06-22T09:20",
		"title": "Carotis és vertebralis artériák Doppler görbe elemzése",
		"performer": "Sayed-Ahmad Mustafa",
		"authors": "Sayed-Ahmad Mustafa, Tasnádi Tünde, Botos Zoltán, Tálas Dávid",
		"location": "Békéscsaba"
	},
	{
		"id": "5",
		"sectionId": "2.4",
		"from": "2018-06-22T09:20",
		"to": "2018-06-22T09:30",
		"title": "Genetikai és környezeti hatások az atheroscleroticus fenotípusok kialakulásában: egy követéses ikervizsgálat eredményei",
		"performer": "Tárnoki Ádám",
		"authors": "Tarnoki Adam D, Tarnoki David L, Fagnani Corrado, Medda Emanuela, Pucci Giacomo, Lucatelli Pierleone, Cirelli Carlo, Fanelli Fabrizio, Maurovich-Horvat Pal, Jermendy Adam L, Fejer Bence, Jermendy Gyorgy, Merkely Bela, Baracchini Claudio, Stazi Maria A.",
		"location": "Budapest, Roma, Perugia, Padua"
	},
	{
		"id": "6",
		"sectionId": "2.4",
		"from": "2018-06-22T09:30",
		"to": "2018-06-22T09:40",
		"title": "A carotis és femoralis atheroscleroticus plakkok kialakulásának hátterének átfogó ultrahangos vizsgálata ikrekben",
		"performer": "Szabó Helga",
		"authors": "Szabó Helga, Gyulay Kata, Jokkel Zsófia, Zsupos Rebeka, Janositz Gréta, Bérczi Viktor, Tárnoki Dávid L., Tárnoki Ádám D.",
		"location": "Budapest"
	},
	{
		"id": "7",
		"sectionId": "2.4",
		"from": "2018-06-22T09:40",
		"to": "2018-06-22T09:50",
		"title": "Vaszkuláris diagnosztika új kutatási iránya a Városmajori Klinikán – Kinetikus Képalkotás",
		"performer": "Gyánó Marcell",
		"authors": "Gyánó Marcell, Óriás Viktor, Góg István, Szöllősi Dávid, Szigeti Krisztián, Osváth Szabolcs, Ruzsa Zoltán, Nemes Balázs, Sótonyi Péter",
		"location": "Budapest, Kecskemét"
	},
	{
		"id": "8",
		"sectionId": "2.4",
		"from": "2018-06-22T09:50",
		"to": "2018-06-22T10:00",
		"title": "A pajzsmirigy ultrahang elasztográfiás vizsgálata ikreken",
		"performer": "Fekete Márton",
		"authors": "Fekete Márton, Erdei Mercédesz, Szily Marcell, Dienes András, Persely Alíz, Bitai Zsuzsanna, Hernyes Anita, Stazi Maria Antonietta, Medda Emanuela, Fagnani Corrado, Bérczi Viktor, Tárnoki Ádám Domonkos, Tárnoki Dávid László",
		"location": "Budapest, Ried im Innkreis"
	},
	{
		"id": "9",
		"sectionId": "2.4",
		"from": "2018-06-22T10:00",
		"to": "2018-06-22T10:10",
		"title": "Közös genetikai hatások szerepe az érrendszer különböző területeinek atheroscleroticus megnyilvánulásában",
		"performer": "Hernyes Anita",
		"authors": "Hernyes Anita, Szalontai László, Forgó Bianka, Fejér Bence, Jermendy Ádám, Maurovich-Horvát Pál, Merkely Béla, Jermendy György, Hang A Park, Joohon Sung, Tárnoki Ádám Domonkos, Tárnoki Dávid László",
		"location": "Budapest"
	},
	{
		"id": "10",
		"sectionId": "2.4",
		"from": "2018-06-22T10:10",
		"to": "2018-06-22T10:20",
		"title": "Revolutionary new transducer technology and the future possibilities of medical ultrasound\n",
		"performer": "Takashino Hirano",
		"authors": "Hitachi Inc.",
		"location": "Hitachi Inc."
	},
	{
		"id": "1",
		"sectionId": "2.5",
		"from": "2018-06-22T10:50",
		"to": "2018-06-22T11:20",
		"title": "About ESR activities",
		"performer": "Derchi",
		"authors": "Derchi, Lorenzo",
		"location": "Genova"
	},
	{
		"id": "2",
		"sectionId": "2.5",
		"from": "2018-06-22T11:20",
		"to": "2018-06-22T12:10",
		"title": "The power of routine systematic peer feedback for continuous team learning",
		"performer": "Carlos Schorlemmer",
		"authors": "Carlos Schorlemmer",
		"location": "Barcelona"
	},
	{
		"id": "3",
		"sectionId": "2.5",
		"from": "2018-06-22T12:10",
		"to": "2018-06-22T12:30",
		"title": "TMC Academy: How to bring innovative medical education and clinical practice together",
		"performer": "Robledo",
		"authors": "Robledo, Ricard",
		"location": "Barcelona"
	},
	{
		"id": "1",
		"sectionId": "2.8",
		"from": "2018-06-22T13:45",
		"to": "2018-06-22T14:05",
		"title": "Novel Neuroimaging Biomarkers: Recognizing the Future Paths",
		"performer": "Milos A. Lucic",
		"authors": "Milos A. Lucic",
		"location": "Novi Sad, Serbia"
	},
	{
		"id": "2",
		"sectionId": "2.8",
		"from": "2018-06-22T14:05",
		"to": "2018-06-22T14:35",
		"title": "Radiomics, radiogenomics, radiotranscriptomics, radioproteomics....",
		"performer": "Patay Zoltán",
		"authors": "Patay Zoltán",
		"location": "Memphis, USA"
	},
	{
		"id": "3",
		"sectionId": "2.8",
		"from": "2018-06-22T14:35",
		"to": "2018-06-22T14:45",
		"title": "A hypophysis méretének örökletessége és kapcsolata a testsúly-testmagasság index-szel: egy ikervizsgálat eredményei",
		"performer": "Persely Aliz",
		"authors": "Persely Aliz, Szily Marcell, Dienes András, Fekete Márton, Kovács Dániel Tamás, Hernyes Anita, Karlinger Kinga, Bérczi Viktor, Kim Eunae, Sung Joohon, Hornyák Csilla, Szabó Ádám, Rudas Gábor, Tárnoki Dávid László, Tárnoki Ádám Domonkos",
		"location": "Budapest, Seoul"
	},
	{
		"id": "4",
		"sectionId": "2.8",
		"from": "2018-06-22T14:45",
		"to": "2018-06-22T14:55",
		"title": "Hypoxiás-ischaemiás agykárosodás MR megjelenési formái.",
		"performer": "Csomor Angéla",
		"authors": "Csomor Angéla, Kerekes Fanni, Vörös Erika",
		"location": "Szeged"
	},
	{
		"id": "5",
		"sectionId": "2.8",
		"from": "2018-06-22T14:55",
		"to": "2018-06-22T15:05",
		"title": "A nyaki gerinc degeneratív elváltozásainak örökletessége",
		"performer": "Bitai Zsuzsanna",
		"authors": "Bitai Zsuzsanna, Kovács Dániel Tamás, Eunjin Alicia Woo, Joohon Sung, Hornyák Csilla, Bérczi Viktor, Tárnoki Dávid László, Tárnoki Ádám Domonkos",
		"location": "Budapest, Seoul"
	},
	{
		"id": "6",
		"sectionId": "2.8",
		"from": "2018-06-22T15:05",
		"to": "2018-06-22T15:15",
		"title": "Subarachnoidalis vérzést követő vasospasmus paraszimpatikus ganglion stimulálással történő speciális lézerkezelése",
		"performer": "Oláh Csaba",
		"authors": "Oláh Csaba, Demeter Béla, Kosztopulosz Nikoletta, Rózsa Tamás",
		"location": "Miskolc"
	},
	{
		"id": "7",
		"sectionId": "2.8",
		"from": "2018-06-22T15:15",
		"to": "2018-06-22T15:25",
		"title": "Hyperdenz média jel artéria cerebri média elzáródás nélkül",
		"performer": "Oláh Csaba",
		"authors": "Oláh Csaba, Tamáska Péter, Sas Attila, Valikovics Attila, Lázár István",
		"location": "Miskolc"
	},
	{
		"id": "8",
		"sectionId": "2.8",
		"from": "2018-06-22T15:25",
		"to": "2018-06-22T15:35",
		"title": "Humán mikromozgás és zene: Kísérleti kutatás a mozgási műtermékek csökkentése érdekében Mágneses Rezonancia Képalkotás (MRI) során",
		"performer": "Földes Zsuzsa",
		"authors": "Földes Zsuzsa, Ala-Ruona Esa, Burger Birgitta, Orsi Gergely",
		"location": "Jyväskylä, Pécs"
	},
	{
		"id": "9",
		"sectionId": "2.8",
		"from": "2018-06-22T15:35",
		"to": "2018-06-22T15:45",
		"title": "Műtét előtti beszédaktivációs fMRI vizsgálatok validálása az intrinzik kapcsolati hálózatokról nyert ismeretek segítségével",
		"performer": "Kozák Lajos Rudolf",
		"authors": "L.R. Kozák, G. Gyebnár, A.G. Szabo, A.L. van Graan, P. Barsi, L. Lemieux, G. Rudas",
		"location": "Budapest, London"
	},
	{
		"id": "10",
		"sectionId": "2.8",
		"from": "2018-06-22T15:45",
		"to": "2018-06-22T15:55",
		"title": "A fehérállományi mikrovérzések szubakut szuszceptibilitás súlyozott (SWI) MRI-n átmenetileg láthatatlanná válhatnak patkányban",
		"performer": "Környei Bálint Soma",
		"authors": "Környei Bálint Soma, Tóth Arnold, Balogh Bendegúz, Berente Zoltán, Schwarcz Attila",
		"location": "Pécs"
	},
	{
		"id": "11",
		"sectionId": "2.8",
		"from": "2018-06-22T15:55",
		"to": "2018-06-22T16:05",
		"title": "Az obstruktív alvási apnoe és a lumbalis gerinc degeneratív eltéréseinek a kapcsolata.",
		"performer": "Szily Marcell",
		"authors": "Szily Marcell, Tárnoki Ádám Domonkos, Tárnoki Dávid László, Kovács Dániel Tamás, Forgó Bianka, Kim Eunae, Sung Joohon, De Barros Pinheiro Marina, Kostyál László, Bikov András, Oláh Csaba, Kunos László",
		"location": "Budapest, Seoul, Sydney, Miskolc"
	},
	{
		"id": "12",
		"sectionId": "2.8",
		"from": "2018-06-22T16:05",
		"to": "2018-06-22T16:15",
		"title": "Lumbális gerinc MR-en látott eltérések és a derékfájdalom közötti genetikai és környezeti kapcsolat vizsgálata ikrekben",
		"performer": "Dienes András",
		"authors": "Dienes András, Eunae Kim, Joohon Sung, Kovács Dániel T., Hornyák Csilla, Ferreira Paulo, De Barros Pinheiro Marina, Kostyál László, Oláh Csaba, Bérczi Viktor, Tárnoki Dávid L., Tárnoki Ádám D.",
		"location": "Budapest, Seoul, Sydney, Miskolc"
	},
	{
		"id": "13",
		"sectionId": "2.8",
		"from": "2018-06-22T16:15",
		"to": "2018-06-22T16:25",
		"title": "Ébredéses ischaemiás stroke komplex diagnosztikája és endovascularis ellátása. Esetbemutatás.",
		"performer": "Tamáska Péter",
		"authors": "Tamáska Péter, Kostyál László, Oláh Csaba, Lázár István",
		"location": "Miskolc"
	},
	{
		"id": "1",
		"sectionId": "2.9",
		"from": "2018-06-22T14:00",
		"to": "2018-06-22T14:20",
		"title": "Tomosynthesis and Contrast- Enhanced Mammography – our Experience at Institute of Oncology Ljubljana",
		"performer": "Music, Maja",
		"authors": "Music, Maja",
		"location": "Ljubljana, Slovenia"
	},
	{
		"id": "2",
		"sectionId": "2.9",
		"from": "2018-06-22T14:20",
		"to": "2018-06-22T14:40",
		"title": "Preliminary outcomes of pilot study of breast cancer SCREEning with MR-mammography in women with genetic mutation in Slovakia - SCRIMS project.",
		"performer": "Lehotska V.",
		"authors": "Lehotska V., Rauova K., Vanovcanova L.,",
		"location": "Bratislava, Slovakia"
	},
	{
		"id": "3",
		"sectionId": "2.9",
		"from": "2018-06-22T14:40",
		"to": "2018-06-22T15:00",
		"title": "Az Európai Emlődiagnosztikai Társaság (EUSOBI) oktató, tudományos és szakmapolitikai tevékenységei",
		"performer": "Forrai Gábor",
		"authors": "Forrai Gábor",
		"location": "Budapest"
	},
	{
		"id": "4",
		"sectionId": "2.9",
		"from": "2018-06-22T15:00",
		"to": "2018-06-22T15:10",
		"title": "Emlő-MR-vizsgálatok leletezése. Közös nyelven, egységesen",
		"performer": "Tasnádi Tünde",
		"authors": "Tasnádi Tünde, Forrai Gábor",
		"location": "Békéscsaba, Budapest"
	},
	{
		"id": "5",
		"sectionId": "2.9",
		"from": "2018-06-22T15:10",
		"to": "2018-06-22T15:20",
		"title": "Emlő MR leletsablonok a BI-RADS lexikon alapján Hatékonyan, egyszerűen",
		"performer": "Tasnádi Tünde",
		"authors": "Tasnádi Tünde, Forrai Gábor",
		"location": "Békéscsaba, Budapest"
	},
	{
		"id": "6",
		"sectionId": "2.9",
		"from": "2018-06-22T15:20",
		"to": "2018-06-22T15:30",
		"title": "A teleradiológia szerepe az emlődiagnosztikában",
		"performer": "Fülöp Rita",
		"authors": "Fülöp Rita, Barna Krisztina, Eriksson Johnny",
		"location": "Budapest, Örebro"
	},
	{
		"id": "7",
		"sectionId": "2.9",
		"from": "2018-06-22T15:30",
		"to": "2018-06-22T15:40",
		"title": "Az emlőváladékozás okai és kivizsgálása",
		"performer": "Besenyei Róbert",
		"authors": "Besenyei Róbert, Sebő Éva",
		"location": "Debrecen"
	},
	{
		"id": "8",
		"sectionId": "2.9",
		"from": "2018-06-22T15:40",
		"to": "2018-06-22T15:50",
		"title": "Tomoszintézis jelentősége a komplex emlődiagnosztikában",
		"performer": "Vámos Izabella",
		"authors": "Vámos Izabella, Sebő Éva",
		"location": "Debrecen"
	},
	{
		"id": "9",
		"sectionId": "2.9",
		"from": "2018-06-22T15:50",
		"to": "2018-06-22T16:00",
		"title": "Szentinel program, komplex emlődiagnosztika",
		"performer": "Szalai Gábor",
		"authors": "Szalai Gábor, Schmidt Erzsébet, Zámbó Katalin, Pavlovics Gábor, Kálmán Endre",
		"location": "Pécs"
	},
	{
		"id": "10",
		"sectionId": "2.9",
		"from": "2018-06-22T16:00",
		"to": "2018-06-22T16:10",
		"title": "Kezdeti tapasztalataink emlő tomosynthesissel",
		"performer": "Milics Margit",
		"authors": "Milics Margit",
		"location": "Zalaegerszeg"
	},
	{
		"id": "11",
		"sectionId": "2.9",
		"from": "2018-06-22T16:10",
		"to": "2018-06-22T16:20",
		"title": "Mammográfiás sugáregészségügyi minőségügyi kézikönyv",
		"performer": "Porubszky Tamás",
		"authors": "Porubszky Tamás, Elek Richárd, Battyány István, Váradi Csaba, Péntek Zoltán, Horváth Kitti",
		"location": "Budapest, Pécs, Szekszárd"
	},
	{
		"id": "12",
		"sectionId": "2.9",
		"from": "2018-06-22T16:20",
		"to": "2018-06-22T16:30",
		"title": "Kontrasztanyag halmozásos direct digitális mammográfia (CEDM)",
		"performer": "Nahm Krisztina",
		"authors": "Nahm Krisztina, Riedl Erika",
		"location": "Budapest, Vác"
	},
	{
		"id": "1",
		"sectionId": "2.10",
		"from": "2018-06-22T13:45",
		"to": "2018-06-22T14:15",
		"title": "Transzlációs Medicina: vissza a jövőbe",
		"performer": "Hegyi Péter",
		"authors": "Hegyi Péter",
		"location": "Pécs"
	},
	{
		"id": "2",
		"sectionId": "2.10",
		"from": "2018-06-22T14:15",
		"to": "2018-06-22T14:30",
		"title": "A pancreas cystosus daganatai",
		"performer": "Weninger Csaba",
		"authors": "Weninger Csaba",
		"location": "Arvika, Svédország"
	},
	{
		"id": "3",
		"sectionId": "2.10",
		"from": "2018-06-22T14:30",
		"to": "2018-06-22T14:40",
		"title": "Intraarteriális kalcium stimuláció szerepe a pancreas inzulinomák diagnosztikájában",
		"performer": "Tóth Judit",
		"authors": "Tóth Judit, Belán Ivett, Balogh Erika, Verebi Enikő, Nagy Endre",
		"location": "Debrecen"
	},
	{
		"id": "4",
		"sectionId": "2.10",
		"from": "2018-06-22T14:40",
		"to": "2018-06-22T15:00",
		"title": "Akut vékonybél",
		"performer": "Kárteszi Hedvig",
		"authors": "Kárteszi Hedvig",
		"location": "Bristol"
	},
	{
		"id": "5",
		"sectionId": "2.10",
		"from": "2018-06-22T15:00",
		"to": "2018-06-22T15:10",
		"title": "A vékonybélfalban kimutatható diffúziógátlási mintázatok összefüggése a Crohn betegség súlyosságával",
		"performer": "Faluhelyi Nándor",
		"authors": "Faluhelyi Nándor, Farkas Orsolya, Bogner Péter",
		"location": "Pécs"
	},
	{
		"id": "6",
		"sectionId": "2.10",
		"from": "2018-06-22T15:10",
		"to": "2018-06-22T15:20",
		"title": "A Crohn betegség MR diagnosztikája",
		"performer": "Kiss Ildikó",
		"authors": "Kiss Ildikó., Borbély Mónika., Lénárt Zsuzsanna, Palkó András",
		"location": "Szeged"
	},
	{
		"id": "7",
		"sectionId": "2.10",
		"from": "2018-06-22T15:20",
		"to": "2018-06-22T15:30",
		"title": "Ritka anomália gyakori tünetek mögött",
		"performer": "Csete Mónika",
		"authors": "Csete Mónika, Szukits Sándor",
		"location": "Komló, Pécs"
	},
	{
		"id": "8",
		"sectionId": "2.10",
		"from": "2018-06-22T15:30",
		"to": "2018-06-22T15:40",
		"title": "Aorto-duodenalis fistula sürgősségi képalkotása",
		"performer": "Bézi István",
		"authors": "Bézi István, Székely András, Pusztai Ferenc, Szücs István, Vinnai Gyula, Bágyi Péter",
		"location": "Debrecen"
	},
	{
		"id": "9",
		"sectionId": "2.10",
		"from": "2018-06-22T15:40",
		"to": "2018-06-22T15:50",
		"title": "Tompa hasi sérülés okozta hasfali sérvek jelentősége",
		"performer": "Gyömbér Edit",
		"authors": "Gyömbér Edit, Callum Stove, Paul Duffy",
		"location": "Glasgow"
	},
	{
		"id": "10",
		"sectionId": "2.10",
		"from": "2018-06-22T15:50",
		"to": "2018-06-22T16:00",
		"title": "Apophenia a hasban – a Pokélabda esete",
		"performer": "Bagi Alexandra",
		"authors": "Bagi Alexandra, Borbola György",
		"location": "Mosonmagyaróvár, Békéscsaba"
	},
	{
		"id": "11",
		"sectionId": "2.10",
		"from": "2018-06-22T16:00",
		"to": "2018-06-22T16:10",
		"title": "A rectum alsó harmadában fekvő malignus elváltozások MR diagnosztikai problémái",
		"performer": "Kalina Ildikó",
		"authors": "Kalina Ildikó, Turtóczki Kolos, Kaposi Pál, István Gábor, Zaránd Attila, Bérczi, Viktor",
		"location": "Budapest"
	},
	{
		"id": "12",
		"sectionId": "2.10",
		"from": "2018-06-22T16:10",
		"to": "2018-06-22T16:20",
		"title": "Új kezelési stratégia neoadjuváns terápia után komplett klinikai választ adó, rectum tumoros betegeknél",
		"performer": "Hoffer Krisztina",
		"authors": "Hoffer Krisztina, Rakos Gyula",
		"location": "Sopron"
	},
	{
		"id": "13",
		"sectionId": "2.10",
		"from": "2018-06-22T16:20",
		"to": "2018-06-22T16:30",
		"title": "Talált tárgyak radiológia osztálya - Avagy előkerült idegentestek 4 eset kapcsán",
		"performer": "Dankházi Levente",
		"authors": "Dankházi Levente, Szabó Albert, Bartek Péter",
		"location": "Győr"
	},
	{
		"id": "1",
		"sectionId": "3.1",
		"from": "2018-06-23T8:20",
		"to": "2018-06-23T8:35",
		"title": "Gyakorlati képalkotás akut ischaemiás stroke-ban: módszerek, lehetőségek és irányelvek",
		"performer": "Várallyay Péter",
		"authors": "Várallyay Péter",
		"location": "Budapest"
	},
	{
		"id": "2",
		"sectionId": "3.1",
		"from": "2018-06-23T8:35",
		"to": "2018-06-23T8:50",
		"title": "Fejlett képalkotási módszerek és alkalmazási területeik akut ishaemiás stroke-ban",
		"performer": "Rostás Tamás",
		"authors": "Rostás Tamás, Kövér Ferenc",
		"location": "Pécs"
	},
	{
		"id": "3",
		"sectionId": "3.1",
		"from": "2018-06-23T8:50",
		"to": "2018-06-23T9:05",
		"title": "Neurointervenciós rekanalizációs módszerek és eredményeik akut ischaemiás stroke-ban",
		"performer": "Szólics Alex",
		"authors": "Szólics Alex",
		"location": "Pécs"
	},
	{
		"id": "4",
		"sectionId": "3.1",
		"from": "2018-06-23T9:05",
		"to": "2018-06-23T9:20",
		"title": "Akut ischaemiás stroke intervenciós kezelésének hazai helyzete a nemzetközi tapasztalatok és irányelvek fényében",
		"performer": "Szikora István",
		"authors": "Szikora István, Kis Balázs, Vadász Ágnes, Gubucz István, Berentei Zsolt, Nardai Sándor",
		"location": "Budapest"
	},
	{
		"id": "5",
		"sectionId": "3.1",
		"from": "2018-06-23T9:20",
		"to": "2018-06-23T9:28",
		"title": "Akut ischaemiás stroke endovascularis kezelésének eredményei az Országos Klinikai Idegtudományi Intézetben",
		"performer": "Kis Balázs",
		"authors": "Kis Balázs, Vadász Ágnes, Nagy András, Nardai Sándor, Gubucz István, Berentei Zsolt, Szikora István",
		"location": "Budapest"
	},
	{
		"id": "6",
		"sectionId": "3.1",
		"from": "2018-06-23T9:28",
		"to": "2018-06-23T9:36",
		"title": "Mechanikus thrombectomiával szerzett kezdeti tapasztalataink",
		"performer": "Tamáska Péter",
		"authors": "Tamáska Péter, Ráski Gergely, Oláh Csaba, Pataki Ákos, Koncz Júlia, Lázár István",
		"location": "Miskolc"
	},
	{
		"id": "7",
		"sectionId": "3.1",
		"from": "2018-06-23T9:36",
		"to": "2018-06-23T9:42",
		"title": "Mechanikus thrombectomia helyzete Győrben",
		"performer": "Bartek Péter",
		"authors": "Bartek Péter, Szabó Albert, Garab Gergely",
		"location": "Győr"
	},
	{
		"id": "8",
		"sectionId": "3.1",
		"from": "2018-06-23T9:42",
		"to": "2018-06-23T9:50",
		"title": "Kezdeti tapasztalataink az endovascularis stroke ellátás során",
		"performer": "Nagy Csaba",
		"authors": "Nagy Csaba, Király István",
		"location": "Szombathely"
	},
	{
		"id": "9",
		"sectionId": "3.1",
		"from": "2018-06-23T9:50",
		"to": "2018-06-23T9:58",
		"title": "Akut ischaemiás stroke kezelés: thrombectomia eredmények Debrecen",
		"performer": "Tóth Judit",
		"authors": "Tóth Judit, Belán Ivett, Veisz Richárd, Fekete István",
		"location": "Debrecen"
	},
	{
		"id": "10",
		"sectionId": "3.1",
		"from": "2018-06-23T9:58",
		"to": "2018-06-23T10:06",
		"title": "Áttekintés a pécsi thrombectomia ellátás első három évéről",
		"performer": "Lenzsér Gábor",
		"authors": "Lenzsér Gábor",
		"location": "Pécs"
	},
	{
		"id": "11",
		"sectionId": "3.1",
		"from": "2018-06-23T10:06",
		"to": "2018-06-23T10:15",
		"title": "Az ischémiás stroke endovaszkuláris kezelésével szerzett tapasztalatok a Somogy Megyei Kaposi Mór Oktatókórházban.",
		"performer": "Vajda Zsolt",
		"authors": "Vajda Zsolt, Nagy Csaba, Lenzsér Gábor, Horváth Gyula, Bajzik Gábor, Radnai Péter, Repa Imre, Nagy Ferenc",
		"location": "Kaposvár"
	},
	{
		"id": "1",
		"sectionId": "3.2",
		"from": "2018-06-23T8:30",
		"to": "2018-06-23T8:40",
		"title": "A legújabb, TNM 8 osztályozás alapja, prognosztikát befolyásoló általános szempontjai",
		"performer": "Gődény Mária",
		"authors": "Gődény Mária",
		"location": "Budapest"
	},
	{
		"id": "2",
		"sectionId": "3.2",
		"from": "2018-06-23T8:40",
		"to": "2018-06-23T8:50",
		"title": "Változások a fej-nyaki daganatok stádium meghatározásában a TNM-8 alapján",
		"performer": "Léránt Gergely",
		"authors": "Léránt Gergely, Gődény Mária",
		"location": "Budapest)"
	},
	{
		"id": "3",
		"sectionId": "3.2",
		"from": "2018-06-23T8:50",
		"to": "2018-06-23T9:00",
		"title": "A gastrointestinalis rendszert érintő változások a UICC/AJCC 8. TNM rendszerében",
		"performer": "Jederán Éva",
		"authors": "Jederán Éva, Ujlaki Mátyás, Bahéry Mária",
		"location": "Budapest"
	},
	{
		"id": "4",
		"sectionId": "3.2",
		"from": "2018-06-23T9:00",
		"to": "2018-06-23T9:10",
		"title": "Változások a máj és epeúti daganatok stádium meghatározásában a TNM-8 alapján",
		"performer": "Csemez Imre",
		"authors": "Csemez Imre, Gődény Mária",
		"location": "Budapest"
	},
	{
		"id": "5",
		"sectionId": "3.2",
		"from": "2018-06-23T9:10",
		"to": "2018-06-23T9:20",
		"title": "Pancreas tumorok módosított TNM klasszifikációja",
		"performer": "Kárteszi Hedvig",
		"authors": "Kárteszi, Hedvig",
		"location": "Bristol"
	},
	{
		"id": "6",
		"sectionId": "3.2",
		"from": "2018-06-23T9:20",
		"to": "2018-06-23T9:30",
		"title": "Újdonságok a mellkasi tumorok TNM beosztásában",
		"performer": "Kerpel-Fronius Anna",
		"authors": "Kerpel-Fronius, Anna",
		"location": "Budapest"
	},
	{
		"id": "7",
		"sectionId": "3.2",
		"from": "2018-06-23T9:30",
		"to": "2018-06-23T9:40",
		"title": "Változások a csont- és lágyrészdaganatok stádium meghatározásában a TNM-8 alapján",
		"performer": "Manninger Sándor",
		"authors": "Manninger Sándor, Gődény Mária",
		"location": "Budapest"
	},
	{
		"id": "8",
		"sectionId": "3.2",
		"from": "2018-06-23T9:40",
		"to": "2018-06-23T9:50",
		"title": "Változások a melanoma malignum stádium meghatározásában a TNM-8 alapján.",
		"performer": "Bőcs Katalin",
		"authors": "Bőcs Katalin, Plótár Vanda, Liszkay Gabriella",
		"location": "Budapest"
	},
	{
		"id": "9",
		"sectionId": "3.2",
		"from": "2018-06-23T9:50",
		"to": "2018-06-23T10:00",
		"title": "Emlő tumorok TNM besorolása - UICC/AJCC 8. kiadása alapján",
		"performer": "Kovács Eszter",
		"authors": "Kovács Eszter, Bidlek Mária, Gődény Mária",
		"location": "Budapest"
	},
	{
		"id": "10",
		"sectionId": "3.2",
		"from": "2018-06-23T10:00",
		"to": "2018-06-23T10:10",
		"title": "Változások a nőgyógyászati daganatok stádium meghatározásában TNM-8 alapján",
		"performer": "Horváth Katalin",
		"authors": "Horváth Katalin",
		"location": "Budapest"
	},
	{
		"id": "11",
		"sectionId": "3.2",
		"from": "2018-06-23T10:10",
		"to": "2018-06-23T10:20",
		"title": "Változások a húgy-ivar szervek daganatainak stádium meghatározásában",
		"performer": "Oláhné Petri Klára",
		"authors": "Oláhné Petri Klára, Lóránd Ágnes, Bíró Krisztina",
		"location": "Budapest"
	},
	{
		"id": "1",
		"sectionId": "3.3",
		"from": "2018-06-23T8:30",
		"to": "2018-06-23T9:50",
		"title": "Magyar Radiológia workshop",
		"performer": "Harkányi Zoltán",
		"authors": "Harkányi Zoltán, Palkó András, Tárnoki Ádám, Béki János",
		"location": ""
	},
	{
		"id": "2",
		"sectionId": "3.3",
		"from": "2018-06-23T9:50",
		"to": "2018-06-23T10:30",
		"title": "A Zsebők című dokumentumfilm vetítése",
		"performer": "Harkányi Zoltán",
		"authors": "Bevezeti Harkányi Zoltán és Szekulesz Péter",
		"location": ""
	},
	{
		"id": "1",
		"sectionId": "3.4",
		"from": "2018-06-23T10:50",
		"to": "2018-06-23T11:00",
		"title": "Klinikai Audit, 2018 – az áttörés éve? (ESR Standard and Audits Committee)",
		"performer": "Vargha András",
		"authors": "Vargha András",
		"location": "Hidegség"
	},
	{
		"id": "2",
		"sectionId": "3.4",
		"from": "2018-06-23T11:00",
		"to": "2018-06-23T11:10",
		"title": "Hibaforrások és tévedési lehetőségek a leletezésben (ESR Standard and Audits Committee)",
		"performer": "Vargha András",
		"authors": "Vargha András",
		"location": "Hidegség"
	},
	{
		"id": "3",
		"sectionId": "3.4",
		"from": "2018-06-23T11:10",
		"to": "2018-06-23T11:33",
		"title": "Az emlődiagnosztika leggyakoribb szakmai műhibái, az emlőszűrés problémái, leletkiadási és beleegyezési hibák, leggyakoribb lelethibák.",
		"performer": "Forrai Gábor",
		"authors": "Forrai Gábor",
		"location": "Budapest"
	},
	{
		"id": "4",
		"sectionId": "3.4",
		"from": "2018-06-23T11:33",
		"to": "2018-06-23T11:50",
		"title": "Orvosi műhibák a jogász szemével",
		"performer": "Kovács György",
		"authors": "Kovács György",
		"location": "Budapest"
	},
	{
		"id": "5",
		"sectionId": "3.4",
		"from": "2018-06-23T11:50",
		"to": "2018-06-23T12:00",
		"title": "CT dózismonitorozás a mindennapi gyakorlatban",
		"performer": "Bágyi Péter",
		"authors": "Bágyi Péter, Balázs Ervin, Dankó Zsolt, Balkay László",
		"location": "Debrecen"
	},
	{
		"id": "6",
		"sectionId": "3.4",
		"from": "2018-06-23T12:00",
		"to": "2018-06-23T12:10",
		"title": "Mit keres a fizikus a röntgenben?",
		"performer": "Porubszky Tamás",
		"authors": "Porubszky Tamás",
		"location": "Budapest"
	},
	{
		"id": "7",
		"sectionId": "3.4",
		"from": "2018-06-23T12:10",
		"to": "2018-06-23T12:30",
		"title": "HCCM Injection Protocol for Radiation Dose Reduction in CTA and Chest",
		"performer": "Prof. Martin Krix",
		"authors": "Ewopharma - Bracco symposium: Prof. Martin Krix, Global Medical & Regulatory Affairs, Bracco Imaging Deutschland",
		"location": "Konstanz, Germany"
	},
	{
		"id": "8",
		"sectionId": "3.4",
		"from": "2018-06-23T12:30",
		"to": "2018-06-23T12:40",
		"title": "Egy év percről percre – az idő relatív",
		"performer": "Kostyál László",
		"authors": "Kostyál László",
		"location": "Miskolc"
	},
	{
		"id": "9",
		"sectionId": "3.4",
		"from": "2018-06-23T12:40",
		"to": "2018-06-23T12:50",
		"title": "Minőségbiztosítás a szakorvosképzésben",
		"performer": "Lánczi Levente István",
		"authors": "Lánczi Levente István, Pusztai Ferenc, Bágyi Péter",
		"location": "Debrecen"
	},
	{
		"id": "10",
		"sectionId": "3.4",
		"from": "2018-06-23T12:50",
		"to": "2018-06-23T13:00",
		"title": "Kettős leletezés, peer-review a mindennapi gyakorlatban",
		"performer": "Bágyi Péter",
		"authors": "Bágyi Péter, Pusztai Ferenc, Lánczi Levente István",
		"location": "Debrecen"
	},
	{
		"id": "1",
		"sectionId": "3.5",
		"from": "2018-06-23T10:50",
		"to": "2018-06-23T11:00",
		"title": "A szív CT és/vagy MR vizsgálata. Mikor melyik? Milyen sorrendben?",
		"performer": "Tóth Levente",
		"authors": "Tóth Levente",
		"location": "Pécs"
	},
	{
		"id": "2",
		"sectionId": "3.5",
		"from": "2018-06-23T11:00",
		"to": "2018-06-23T11:10",
		"title": "Szívritmus szabályozó készülékek a mellkas röntgenen - Amit radiológusként érdemes tudnunk",
		"performer": "Panajotu Alexisz",
		"authors": "Panajotu Alexisz",
		"location": "Budapest"
	},
	{
		"id": "3",
		"sectionId": "3.5",
		"from": "2018-06-23T11:10",
		"to": "2018-06-23T11:20",
		"title": "Myocarditis és/vagy ARVC: kontroll szív MR vizsgálat ICD beültetés után",
		"performer": "Tóth Attila",
		"authors": "Tóth Attila, Clemens Marcell, Suhai Ferenc Imre, Czimbalmos Csilla, Csécs Ibolya, Dohy Zsófia, Vágó Hajnalka, Simor Tamás, Édes István, Hüttl Kálmán, Merkely Béla",
		"location": "Budapest, Debrecen, Pécs"
	},
	{
		"id": "4",
		"sectionId": "3.5",
		"from": "2018-06-23T11:20",
		"to": "2018-06-23T11:30",
		"title": "A SZÍV 18F-FDG-PET/CT VIZSGÁLATA KÖTŐSZÖVETI BETEGSÉGBEN",
		"performer": "Besenyi Zsuzsanna",
		"authors": "Besenyi Zsuzsanna, Ágoston Gergely, Bakos Annamária, Hemelein Rita, Varga Albert, Kovács László, Pávics László",
		"location": "Szeged"
	},
	{
		"id": "5",
		"sectionId": "3.5",
		"from": "2018-06-23T11:30",
		"to": "2018-06-23T11:40",
		"title": "Pulmonális hipertenzió",
		"performer": "Várady Edit",
		"authors": "Várady Edit",
		"location": "Pécs"
	},
	{
		"id": "6",
		"sectionId": "3.5",
		"from": "2018-06-23T11:40",
		"to": "2018-06-23T11:50",
		"title": "Szív mágneses rezonanciás vizsgálat diagnosztikus szerepe malignus kamrai ritmuszavarok esetén",
		"performer": "Suhai Ferenc Imre",
		"authors": "Suhai Ferenc Imre, Szabó Liliána, Czimbalmos, Csilla, Csécs Ibolya, Tóth Attila, Becker Dávid2, Zima Endre, Gellér László, Hüttl Kálmán, Balázs György, Merkely Béla, Vágó Hajnalka",
		"location": "Budapest"
	},
	{
		"id": "7",
		"sectionId": "3.5",
		"from": "2018-06-23T11:50",
		"to": "2018-06-23T12:00",
		"title": "Mellékleletek TAVI CT angiográfiás vizsgálatok során",
		"performer": "Szukits Sándor",
		"authors": "Szukits Sándor, Tóth Levente, Várady Edit",
		"location": "Pécs"
	},
	{
		"id": "5",
		"sectionId": "3.5",
		"from": "2018-06-23T12:00",
		"to": "2018-06-23T12:10",
		"title": "Mycardiális bridge jelentőségének vizsgálata koszorúér áthidaló műtétek esetében coronaria CT angiográfián alapuló 3D tervezés tükrében",
		"performer": "Wlasitsch-Nagy Zsófia",
		"authors": "Wlasitsch-Nagy Zsófia, Szukits Sándor, Lénárd László, Bogner Péter, Varga Péter, Gasz Balázs, Várady Edit",
		"location": "Pécs"
	},
	{
		"id": "6",
		"sectionId": "3.5",
		"from": "2018-06-23T12:10",
		"to": "2018-06-23T12:20",
		"title": "Modified Coronary Calcium Scoring: Role in Coronary Artery Disease Risk Management of Patients Enrolled in a Lung Cancer Screening Program by Low-Dose CT",
		"performer": "Virágh Károly",
		"authors": "Virágh Károly, Lo Jocelyn, Sica Gregory, Gáspár Emese",
		"location": "New York, Los Angeles"
	},
	{
		"id": "7",
		"sectionId": "3.5",
		"from": "2018-06-23T12:20",
		"to": "2018-06-23T12:35",
		"title": "Új generációs szív CT, első tapasztalatok",
		"performer": "Maurovich-Horvat Pál",
		"authors": "Maurovich-Horvat Pál",
		"location": "Budapest"
	},
	{
		"id": "1",
		"sectionId": "3.6",
		"from": "2018-06-23T10:50",
		"to": "2018-06-23T11:10",
		"title": "A kortalan (határtalan) gyermekradiológia",
		"performer": "Lombay Béla",
		"authors": "Lombay Béla",
		"location": "Miskolc"
	},
	{
		"id": "2",
		"sectionId": "3.6",
		"from": "2018-06-23T11:10",
		"to": "2018-06-23T11:20",
		"title": "Gyermekkori csípőtáji csont-izületi gyulladás",
		"performer": "Polovitzer Mária",
		"authors": "Polovitzer Mária, Bartók Márta, Ráskai Csaba",
		"location": "Budapest"
	},
	{
		"id": "3",
		"sectionId": "3.6",
		"from": "2018-06-23T11:20",
		"to": "2018-06-23T11:30",
		"title": "A gyermekkori traumás hasi sérülések CEUS vizsgálata",
		"performer": "Koller Orsolya",
		"authors": "Koller Orsolya, Harkányi Zoltán, Hegyi Gábor",
		"location": "Budapest"
	},
	{
		"id": "4",
		"sectionId": "3.6",
		"from": "2018-06-23T11:30",
		"to": "2018-06-23T11:40",
		"title": "Az urachus záródási rendellenességeinek áttekintése",
		"performer": "Kiss Tünde",
		"authors": "Kiss Tünde, Bartók Márta, Weber Gabriella",
		"location": "Budapest"
	},
	{
		"id": "5",
		"sectionId": "3.6",
		"from": "2018-06-23T11:40",
		"to": "2018-06-23T11:50",
		"title": "Congenitalis hyperinzulinaemiás hypoglycaemia – gyermekkori focalis elváltozás a pancreasban",
		"performer": "Várkonyi Ildikó",
		"authors": "Várkonyi Ildikó, Balogh Lídia, Luczay Andrea, Harsányi László, Borka Katalin, Kálmán Attila",
		"location": "Budapest"
	},
	{
		"id": "6",
		"sectionId": "3.6",
		"from": "2018-06-23T11:50",
		"to": "2018-06-23T12:00",
		"title": "Vesesérülés ritka esete",
		"performer": "Héjj Ildikó Mária",
		"authors": "Héjj Ildikó Mária, Morvai Zsuzsanna, Harkányi Zoltán",
		"location": "Budapest"
	},
	{
		"id": "7",
		"sectionId": "3.6",
		"from": "2018-06-23T12:00",
		"to": "2018-06-23T12:10",
		"title": "Koponya UH vizsgálatok",
		"performer": "Mohay Gabriella",
		"authors": "Mohay Gabriella",
		"location": "Pécs"
	},
	{
		"id": "8",
		"sectionId": "3.6",
		"from": "2018-06-23T12:10",
		"to": "2018-06-23T12:20",
		"title": "A hypoxiás károsodás és az intracranialis vérzések hatása a fejlődésneurológiai kimenetelre: a korai MR prognosztikai szerepe neonatalis hypoxiás ischaemiás encephalopathiában",
		"performer": "Lakatos Andrea",
		"authors": "Lakatos Andrea, Kolossváry Márton, Szabó Miklós, Jermendy Ágnes, Barta Hajnalka, Gyebnár Gyula, Rudas Gábor, Kozák Lajos Rudolf",
		"location": "Budapest"
	},
	{
		"id": "9",
		"sectionId": "3.6",
		"from": "2018-06-23T12:20",
		"to": "2018-06-23T12:30",
		"title": "Többszervi elváltozást okozó ritka gastroenteritis",
		"performer": "Juhász Emese",
		"authors": "Juhász Emese, Falus Ágnes, Molnár Diana, Polovitzer Mária, Lőrincz Margit",
		"location": "Budapest"
	},
	{
		"id": "10",
		"sectionId": "3.6",
		"from": "2018-06-23T12:30",
		"to": "2018-06-23T12:40",
		"title": "Ritka mellkasi térfoglaló folyamat",
		"performer": "Szabó Ágota",
		"authors": "Szabó Ágota, Molnár Diana, Balázs György, Kálmán Attila",
		"location": "Budapest"
	},
	{
		"id": "11",
		"sectionId": "3.6",
		"from": "2018-06-23T12:40",
		"to": "2018-06-23T12:50",
		"title": "Choledochus cysta ritka szövődménye – esetbemutatás",
		"performer": "Nyitrai Anna",
		"authors": "Nyitrai Anna, Várkonyi Ildikó, Seszták Tímea, Kis Imre",
		"location": "Budapest"
	},
	{
		"id": "12",
		"sectionId": "3.6",
		"from": "2018-06-23T12:50",
		"to": "2018-06-23T13:00",
		"title": "Arteria femoralis profunda posttraumás pseuroaneurysmája",
		"performer": "Molnár Diána",
		"authors": "Molnár Diána, Balázs György, Csobay-Novák Csaba",
		"location": "Budapest"
	},
	{
		"id": "1",
		"sectionId": "3.7",
		"from": "2018-06-23T10:50",
		"to": "2018-06-23T12:00",
		"title": "Kvíz",
		"performer": "",
		"authors": "Olajos Eszter és az esetgazdák",
		"location": ""
	}
];

data.forEach(v=>{

	if(v.from.length != 16){
		v.from = [v.from.slice(0, 11), '0', v.from.slice(11)].join('')
	}
	if(v.to.length != 16){
		v.to = [v.to.slice(0, 11), '0', v.to.slice(11)].join('')
	}

	v.from = new Date(v.from);
	v.to = new Date(v.to);
	v.fullId = v.sectionId+'.'+v.id;
});

data.sort(
	(a,b)=>{
		if(a.from < b.from) return -1;
		if(a.from > b.from) return 1;
		return 0
	}
);


/* harmony default export */ __webpack_exports__["a"] = (data);

/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = "<div class=\"date\"></div>\n<div class=\"time-location\"><span class=\"time\"></span> <span class=\"location\"></span></div>\n<div class=\"section-title\"></div>\n<div class=\"presentations\">\n\n</div>";

/***/ }),
/* 16 */,
/* 17 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
let data = [
	{
		"name": "Pávics László",
		"presentations": [
			{
				"id": "1",
				"sectionId": "1.1"
			}
		]
	},
	{
		"name": "Besenyi Zsuzsanna",
		"presentations": [
			{
				"id": "2",
				"sectionId": "1.1"
			},
			{
				"id": "4",
				"sectionId": "3.5"
			}
		]
	},
	{
		"name": "Séllei Ágnes",
		"presentations": [
			{
				"id": "3",
				"sectionId": "1.1"
			}
		]
	},
	{
		"name": "Győri Gabriella",
		"presentations": [
			{
				"id": "4",
				"sectionId": "1.1"
			}
		]
	},
	{
		"name": "Urbán Szabolcs",
		"presentations": [
			{
				"id": "5",
				"sectionId": "1.1"
			}
		]
	},
	{
		"name": "Bakos Annamária",
		"presentations": [
			{
				"id": "6",
				"sectionId": "1.1"
			}
		]
	},
	{
		"name": "Farkas István",
		"presentations": [
			{
				"id": "7",
				"sectionId": "1.1"
			},
			{
				"id": "6",
				"sectionId": "1.6"
			}
		]
	},
	{
		"name": "Sipka Gábor",
		"presentations": [
			{
				"id": "8",
				"sectionId": "1.1"
			}
		]
	},
	{
		"name": "Kári Béla",
		"presentations": [
			{
				"id": "9",
				"sectionId": "1.1"
			}
		]
	},
	{
		"name": "Sári Áron",
		"presentations": [
			{
				"id": "10",
				"sectionId": "1.1"
			}
		]
	},
	{
		"name": "Paul Duffy",
		"presentations": [
			{
				"id": "11",
				"sectionId": "1.1"
			}
		]
	},
	{
		"name": "Horváth Gábor",
		"presentations": [
			{
				"id": "1",
				"sectionId": "1.2"
			}
		]
	},
	{
		"name": "Vértes Miklós",
		"presentations": [
			{
				"id": "2",
				"sectionId": "1.2"
			}
		]
	},
	{
		"name": "Kis Balázs",
		"presentations": [
			{
				"id": "3",
				"sectionId": "1.2"
			},
			{
				"id": "5",
				"sectionId": "3.1"
			}
		]
	},
	{
		"name": "Bérczi Ákos",
		"presentations": [
			{
				"id": "4",
				"sectionId": "1.2"
			}
		]
	},
	{
		"name": "Oláh Csaba",
		"presentations": [
			{
				"id": "5",
				"sectionId": "1.2"
			},
			{
				"id": "6",
				"sectionId": "2.8"
			},
			{
				"id": "7",
				"sectionId": "2.8"
			}
		]
	},
	{
		"name": "Hernyes Anita",
		"presentations": [
			{
				"id": "6",
				"sectionId": "1.2"
			},
			{
				"id": "9",
				"sectionId": "2.4"
			}
		]
	},
	{
		"name": "Jermendy Ádám",
		"presentations": [
			{
				"id": "7",
				"sectionId": "1.2"
			}
		]
	},
	{
		"name": "Csizmadia Sándor",
		"presentations": [
			{
				"id": "8",
				"sectionId": "1.2"
			}
		]
	},
	{
		"name": "Csobay-Novák Csaba",
		"presentations": [
			{
				"id": "9",
				"sectionId": "1.2"
			}
		]
	},
	{
		"name": "",
		"presentations": [
			{
				"id": "1",
				"sectionId": "1.3"
			},
			{
				"id": "2",
				"sectionId": "1.3"
			},
			{
				"id": "7",
				"sectionId": "1.3"
			},
			{
				"id": "1",
				"sectionId": "3.7"
			}
		]
	},
	{
		"name": "Tárnoki Ádám",
		"presentations": [
			{
				"id": "3",
				"sectionId": "1.3"
			},
			{
				"id": "5",
				"sectionId": "2.4"
			}
		]
	},
	{
		"name": "Karlinger Kinga",
		"presentations": [
			{
				"id": "4",
				"sectionId": "1.3"
			}
		]
	},
	{
		"name": "Tárnoki Dávid",
		"presentations": [
			{
				"id": "5",
				"sectionId": "1.3"
			},
			{
				"id": "7",
				"sectionId": "2.3"
			}
		]
	},
	{
		"name": "Lánczi Levente",
		"presentations": [
			{
				"id": "6",
				"sectionId": "1.3"
			}
		]
	},
	{
		"name": "Földes Tamás",
		"presentations": [
			{
				"id": "8",
				"sectionId": "1.3"
			}
		]
	},
	{
		"name": "Tárnoki Dávid László",
		"presentations": [
			{
				"id": "1",
				"sectionId": "1.4"
			}
		]
	},
	{
		"name": "Monostori Zsuzsanna",
		"presentations": [
			{
				"id": "2",
				"sectionId": "1.4"
			}
		]
	},
	{
		"name": "Kerpel-Fronius Anna",
		"presentations": [
			{
				"id": "3",
				"sectionId": "1.4"
			},
			{
				"id": "6",
				"sectionId": "3.2"
			}
		]
	},
	{
		"name": "Balázs György",
		"presentations": [
			{
				"id": "4",
				"sectionId": "1.4"
			}
		]
	},
	{
		"name": "Székely András",
		"presentations": [
			{
				"id": "5",
				"sectionId": "1.4"
			}
		]
	},
	{
		"name": "Tárnoki Ádám Domonkos",
		"presentations": [
			{
				"id": "6",
				"sectionId": "1.4"
			}
		]
	},
	{
		"name": "Futácsi Balázs",
		"presentations": [
			{
				"id": "7",
				"sectionId": "1.4"
			}
		]
	},
	{
		"name": "Nagy Edit Boglárka",
		"presentations": [
			{
				"id": "8",
				"sectionId": "1.4"
			}
		]
	},
	{
		"name": "Kovács Anita",
		"presentations": [
			{
				"id": "9",
				"sectionId": "1.4"
			}
		]
	},
	{
		"name": "Orbán Vince",
		"presentations": [
			{
				"id": "10",
				"sectionId": "1.4"
			}
		]
	},
	{
		"name": "Zsiros László Róbert",
		"presentations": [
			{
				"id": "1",
				"sectionId": "1.5"
			},
			{
				"id": "4",
				"sectionId": "1.5"
			}
		]
	},
	{
		"name": "Damsa Andrei",
		"presentations": [
			{
				"id": "2",
				"sectionId": "1.5"
			}
		]
	},
	{
		"name": "Botz Bálint",
		"presentations": [
			{
				"id": "3",
				"sectionId": "1.5"
			}
		]
	},
	{
		"name": "Olajos Eszter Ajna",
		"presentations": [
			{
				"id": "5",
				"sectionId": "1.5"
			}
		]
	},
	{
		"name": "Varga Péter",
		"presentations": [
			{
				"id": "6",
				"sectionId": "1.5"
			}
		]
	},
	{
		"name": "Klimaj Zoltán",
		"presentations": [
			{
				"id": "7",
				"sectionId": "1.5"
			}
		]
	},
	{
		"name": "Derchi",
		"presentations": [
			{
				"id": "1",
				"sectionId": "1.6"
			},
			{
				"id": "1",
				"sectionId": "2.5"
			}
		]
	},
	{
		"name": "Koppán Miklós",
		"presentations": [
			{
				"id": "2",
				"sectionId": "1.6"
			}
		]
	},
	{
		"name": "Nagy Gyöngyi",
		"presentations": [
			{
				"id": "3",
				"sectionId": "1.6"
			}
		]
	},
	{
		"name": "Bérczi Viktor",
		"presentations": [
			{
				"id": "4",
				"sectionId": "1.6"
			}
		]
	},
	{
		"name": "Karczagi Lilla",
		"presentations": [
			{
				"id": "5",
				"sectionId": "1.6"
			}
		]
	},
	{
		"name": "Kaposi Novák Pál",
		"presentations": [
			{
				"id": "7",
				"sectionId": "1.6"
			},
			{
				"id": "5",
				"sectionId": "2.2"
			}
		]
	},
	{
		"name": "Kalina Ildikó",
		"presentations": [
			{
				"id": "8",
				"sectionId": "1.6"
			},
			{
				"id": "11",
				"sectionId": "2.10"
			}
		]
	},
	{
		"name": "Dombi Gergely",
		"presentations": [
			{
				"id": "1",
				"sectionId": "2.1"
			}
		]
	},
	{
		"name": "Joris Wakkie",
		"presentations": [
			{
				"id": "2",
				"sectionId": "2.1"
			}
		]
	},
	{
		"name": "Kecskeméthy Péter",
		"presentations": [
			{
				"id": "3",
				"sectionId": "2.1"
			}
		]
	},
	{
		"name": "Gál Andor Viktor",
		"presentations": [
			{
				"id": "4",
				"sectionId": "2.1"
			}
		]
	},
	{
		"name": "Samsung",
		"presentations": [
			{
				"id": "5",
				"sectionId": "2.1"
			}
		]
	},
	{
		"name": "Lombay Béla",
		"presentations": [
			{
				"id": "6",
				"sectionId": "2.1"
			},
			{
				"id": "1",
				"sectionId": "3.6"
			}
		]
	},
	{
		"name": "Palkó András",
		"presentations": [
			{
				"id": "1",
				"sectionId": "2.2"
			}
		]
	},
	{
		"name": "Doros Attila",
		"presentations": [
			{
				"id": "2",
				"sectionId": "2.2"
			}
		]
	},
	{
		"name": "Papp András",
		"presentations": [
			{
				"id": "3",
				"sectionId": "2.2"
			}
		]
	},
	{
		"name": "Demjén Boglárka",
		"presentations": [
			{
				"id": "4",
				"sectionId": "2.2"
			}
		]
	},
	{
		"name": "Bánsághi Zoltán",
		"presentations": [
			{
				"id": "6",
				"sectionId": "2.2"
			}
		]
	},
	{
		"name": "Kónya Júlia Anna",
		"presentations": [
			{
				"id": "7",
				"sectionId": "2.2"
			}
		]
	},
	{
		"name": "Nőt László Gergely",
		"presentations": [
			{
				"id": "1",
				"sectionId": "2.3"
			}
		]
	},
	{
		"name": "Farbaky Zsófia",
		"presentations": [
			{
				"id": "2",
				"sectionId": "2.3"
			}
		]
	},
	{
		"name": "Hetényi Szabolcs",
		"presentations": [
			{
				"id": "3",
				"sectionId": "2.3"
			}
		]
	},
	{
		"name": "Soltész Judit",
		"presentations": [
			{
				"id": "4",
				"sectionId": "2.3"
			}
		]
	},
	{
		"name": "Tasnádi Tünde",
		"presentations": [
			{
				"id": "5",
				"sectionId": "2.3"
			},
			{
				"id": "4",
				"sectionId": "2.9"
			},
			{
				"id": "5",
				"sectionId": "2.9"
			}
		]
	},
	{
		"name": "Brzózka Ádám",
		"presentations": [
			{
				"id": "6",
				"sectionId": "2.3"
			}
		]
	},
	{
		"name": "Bánáti Laura",
		"presentations": [
			{
				"id": "8",
				"sectionId": "2.3"
			}
		]
	},
	{
		"name": "Brkljacic, Boris",
		"presentations": [
			{
				"id": "1",
				"sectionId": "2.4"
			}
		]
	},
	{
		"name": "Szalontai László",
		"presentations": [
			{
				"id": "2",
				"sectionId": "2.4"
			}
		]
	},
	{
		"name": "Janositz Gréta",
		"presentations": [
			{
				"id": "3",
				"sectionId": "2.4"
			}
		]
	},
	{
		"name": "Sayed-Ahmad Mustafa",
		"presentations": [
			{
				"id": "4",
				"sectionId": "2.4"
			}
		]
	},
	{
		"name": "Szabó Helga",
		"presentations": [
			{
				"id": "6",
				"sectionId": "2.4"
			}
		]
	},
	{
		"name": "Gyánó Marcell",
		"presentations": [
			{
				"id": "7",
				"sectionId": "2.4"
			}
		]
	},
	{
		"name": "Fekete Márton",
		"presentations": [
			{
				"id": "8",
				"sectionId": "2.4"
			}
		]
	},
	{
		"name": "Hitachi prezentáció",
		"presentations": [
			{
				"id": "10",
				"sectionId": "2.4"
			}
		]
	},
	{
		"name": "Carlos Schorlemmer",
		"presentations": [
			{
				"id": "2",
				"sectionId": "2.5"
			}
		]
	},
	{
		"name": "Robledo",
		"presentations": [
			{
				"id": "3",
				"sectionId": "2.5"
			}
		]
	},
	{
		"name": "Milos A. Lucic",
		"presentations": [
			{
				"id": "1",
				"sectionId": "2.8"
			}
		]
	},
	{
		"name": "Patay Zoltán",
		"presentations": [
			{
				"id": "2",
				"sectionId": "2.8"
			}
		]
	},
	{
		"name": "Persely Aliz",
		"presentations": [
			{
				"id": "3",
				"sectionId": "2.8"
			}
		]
	},
	{
		"name": "Csomor Angéla",
		"presentations": [
			{
				"id": "4",
				"sectionId": "2.8"
			}
		]
	},
	{
		"name": "Bitai Zsuzsanna",
		"presentations": [
			{
				"id": "5",
				"sectionId": "2.8"
			}
		]
	},
	{
		"name": "Földes Zsuzsa",
		"presentations": [
			{
				"id": "8",
				"sectionId": "2.8"
			}
		]
	},
	{
		"name": "Kozák Lajos Rudolf",
		"presentations": [
			{
				"id": "9",
				"sectionId": "2.8"
			}
		]
	},
	{
		"name": "Környei Bálint Soma",
		"presentations": [
			{
				"id": "10",
				"sectionId": "2.8"
			}
		]
	},
	{
		"name": "Szily Marcell",
		"presentations": [
			{
				"id": "11",
				"sectionId": "2.8"
			}
		]
	},
	{
		"name": "Dienes András",
		"presentations": [
			{
				"id": "12",
				"sectionId": "2.8"
			}
		]
	},
	{
		"name": "Tamáska Péter",
		"presentations": [
			{
				"id": "13",
				"sectionId": "2.8"
			},
			{
				"id": "6",
				"sectionId": "3.1"
			}
		]
	},
	{
		"name": "Music, Maja",
		"presentations": [
			{
				"id": "1",
				"sectionId": "2.9"
			}
		]
	},
	{
		"name": "Lehotska V.",
		"presentations": [
			{
				"id": "2",
				"sectionId": "2.9"
			}
		]
	},
	{
		"name": "Forrai Gábor",
		"presentations": [
			{
				"id": "3",
				"sectionId": "2.9"
			},
			{
				"id": "3",
				"sectionId": "3.4"
			}
		]
	},
	{
		"name": "Fülöp Rita",
		"presentations": [
			{
				"id": "6",
				"sectionId": "2.9"
			}
		]
	},
	{
		"name": "Besenyei Róbert",
		"presentations": [
			{
				"id": "7",
				"sectionId": "2.9"
			}
		]
	},
	{
		"name": "Vámos Izabella",
		"presentations": [
			{
				"id": "8",
				"sectionId": "2.9"
			}
		]
	},
	{
		"name": "Szalai Gábor",
		"presentations": [
			{
				"id": "9",
				"sectionId": "2.9"
			}
		]
	},
	{
		"name": "Milics Margit",
		"presentations": [
			{
				"id": "10",
				"sectionId": "2.9"
			}
		]
	},
	{
		"name": "Porubszky Tamás",
		"presentations": [
			{
				"id": "11",
				"sectionId": "2.9"
			},
			{
				"id": "6",
				"sectionId": "3.4"
			}
		]
	},
	{
		"name": "Nahm Krisztina",
		"presentations": [
			{
				"id": "12",
				"sectionId": "2.9"
			}
		]
	},
	{
		"name": "Hegyi Péter",
		"presentations": [
			{
				"id": "1",
				"sectionId": "2.10"
			}
		]
	},
	{
		"name": "Weninger Csaba",
		"presentations": [
			{
				"id": "2",
				"sectionId": "2.10"
			}
		]
	},
	{
		"name": "Tóth Judit",
		"presentations": [
			{
				"id": "3",
				"sectionId": "2.10"
			},
			{
				"id": "9",
				"sectionId": "3.1"
			}
		]
	},
	{
		"name": "Kárteszi Hedvig",
		"presentations": [
			{
				"id": "4",
				"sectionId": "2.10"
			},
			{
				"id": "5",
				"sectionId": "3.2"
			}
		]
	},
	{
		"name": "Faluhelyi Nándor",
		"presentations": [
			{
				"id": "5",
				"sectionId": "2.10"
			}
		]
	},
	{
		"name": "Kiss Ildikó",
		"presentations": [
			{
				"id": "6",
				"sectionId": "2.10"
			}
		]
	},
	{
		"name": "Csete Mónika",
		"presentations": [
			{
				"id": "7",
				"sectionId": "2.10"
			}
		]
	},
	{
		"name": "Bézi István",
		"presentations": [
			{
				"id": "8",
				"sectionId": "2.10"
			}
		]
	},
	{
		"name": "Gyömbér Edit",
		"presentations": [
			{
				"id": "9",
				"sectionId": "2.10"
			}
		]
	},
	{
		"name": "Bagi Alexandra",
		"presentations": [
			{
				"id": "10",
				"sectionId": "2.10"
			}
		]
	},
	{
		"name": "Hoffer Krisztina",
		"presentations": [
			{
				"id": "12",
				"sectionId": "2.10"
			}
		]
	},
	{
		"name": "Dankházi Levente",
		"presentations": [
			{
				"id": "13",
				"sectionId": "2.10"
			}
		]
	},
	{
		"name": "Várallyay Péter",
		"presentations": [
			{
				"id": "1",
				"sectionId": "3.1"
			}
		]
	},
	{
		"name": "Rostás Tamás",
		"presentations": [
			{
				"id": "2",
				"sectionId": "3.1"
			}
		]
	},
	{
		"name": "Szólics Alex",
		"presentations": [
			{
				"id": "3",
				"sectionId": "3.1"
			}
		]
	},
	{
		"name": "Szikora István",
		"presentations": [
			{
				"id": "4",
				"sectionId": "3.1"
			}
		]
	},
	{
		"name": "Bartek Péter",
		"presentations": [
			{
				"id": "7",
				"sectionId": "3.1"
			}
		]
	},
	{
		"name": "Nagy Csaba",
		"presentations": [
			{
				"id": "8",
				"sectionId": "3.1"
			}
		]
	},
	{
		"name": "Lenzsér Gábor",
		"presentations": [
			{
				"id": "10",
				"sectionId": "3.1"
			}
		]
	},
	{
		"name": "Vajda Zsolt",
		"presentations": [
			{
				"id": "11",
				"sectionId": "3.1"
			}
		]
	},
	{
		"name": "Gődény Mária",
		"presentations": [
			{
				"id": "1",
				"sectionId": "3.2"
			}
		]
	},
	{
		"name": "Léránt Gergely",
		"presentations": [
			{
				"id": "2",
				"sectionId": "3.2"
			}
		]
	},
	{
		"name": "Jederán Éva",
		"presentations": [
			{
				"id": "3",
				"sectionId": "3.2"
			}
		]
	},
	{
		"name": "Csemez Imre",
		"presentations": [
			{
				"id": "4",
				"sectionId": "3.2"
			}
		]
	},
	{
		"name": "Manninger Sándor",
		"presentations": [
			{
				"id": "7",
				"sectionId": "3.2"
			}
		]
	},
	{
		"name": "Bőcs Katalin",
		"presentations": [
			{
				"id": "8",
				"sectionId": "3.2"
			}
		]
	},
	{
		"name": "Kovács Eszter",
		"presentations": [
			{
				"id": "9",
				"sectionId": "3.2"
			}
		]
	},
	{
		"name": "Horváth Katalin",
		"presentations": [
			{
				"id": "10",
				"sectionId": "3.2"
			}
		]
	},
	{
		"name": "Oláhné Petri Klára",
		"presentations": [
			{
				"id": "11",
				"sectionId": "3.2"
			}
		]
	},
	{
		"name": "Harkányi Zoltán",
		"presentations": [
			{
				"id": "1",
				"sectionId": "3.3"
			},
			{
				"id": "2",
				"sectionId": "3.3"
			}
		]
	},
	{
		"name": "Vargha András",
		"presentations": [
			{
				"id": "1",
				"sectionId": "3.4"
			},
			{
				"id": "2",
				"sectionId": "3.4"
			}
		]
	},
	{
		"name": "Kovács György",
		"presentations": [
			{
				"id": "4",
				"sectionId": "3.4"
			}
		]
	},
	{
		"name": "Bágyi Péter",
		"presentations": [
			{
				"id": "5",
				"sectionId": "3.4"
			},
			{
				"id": "10",
				"sectionId": "3.4"
			}
		]
	},
	{
		"name": "Ewopharma-Bracco Symposium",
		"presentations": [
			{
				"id": "7",
				"sectionId": "3.4"
			}
		]
	},
	{
		"name": "Kostyál László",
		"presentations": [
			{
				"id": "8",
				"sectionId": "3.4"
			}
		]
	},
	{
		"name": "Lánczi Levente István",
		"presentations": [
			{
				"id": "9",
				"sectionId": "3.4"
			}
		]
	},
	{
		"name": "Tóth Levente",
		"presentations": [
			{
				"id": "1",
				"sectionId": "3.5"
			}
		]
	},
	{
		"name": "Panajotu Alexisz",
		"presentations": [
			{
				"id": "2",
				"sectionId": "3.5"
			}
		]
	},
	{
		"name": "Tóth Attila",
		"presentations": [
			{
				"id": "3",
				"sectionId": "3.5"
			}
		]
	},
	{
		"name": "Várady Edit",
		"presentations": [
			{
				"id": "5",
				"sectionId": "3.5"
			}
		]
	},
	{
		"name": "Suhai Ferenc Imre",
		"presentations": [
			{
				"id": "6",
				"sectionId": "3.5"
			}
		]
	},
	{
		"name": "Szukits Sándor",
		"presentations": [
			{
				"id": "7",
				"sectionId": "3.5"
			}
		]
	},
	{
		"name": "Wlasitsch-Nagy Zsófia",
		"presentations": [
			{
				"id": "5",
				"sectionId": "3.5"
			}
		]
	},
	{
		"name": "Virágh Károly",
		"presentations": [
			{
				"id": "6",
				"sectionId": "3.5"
			}
		]
	},
	{
		"name": "Maurovich-Horvat Pál",
		"presentations": [
			{
				"id": "7",
				"sectionId": "3.5"
			}
		]
	},
	{
		"name": "Polovitzer Mária",
		"presentations": [
			{
				"id": "2",
				"sectionId": "3.6"
			}
		]
	},
	{
		"name": "Koller Orsolya",
		"presentations": [
			{
				"id": "3",
				"sectionId": "3.6"
			}
		]
	},
	{
		"name": "Kiss Tünde",
		"presentations": [
			{
				"id": "4",
				"sectionId": "3.6"
			}
		]
	},
	{
		"name": "Várkonyi Ildikó",
		"presentations": [
			{
				"id": "5",
				"sectionId": "3.6"
			}
		]
	},
	{
		"name": "Héjj Ildikó Mária",
		"presentations": [
			{
				"id": "6",
				"sectionId": "3.6"
			}
		]
	},
	{
		"name": "Mohay Gabriella",
		"presentations": [
			{
				"id": "7",
				"sectionId": "3.6"
			}
		]
	},
	{
		"name": "Lakatos Andrea",
		"presentations": [
			{
				"id": "8",
				"sectionId": "3.6"
			}
		]
	},
	{
		"name": "Juhász Emese",
		"presentations": [
			{
				"id": "9",
				"sectionId": "3.6"
			}
		]
	},
	{
		"name": "Szabó Ágota",
		"presentations": [
			{
				"id": "10",
				"sectionId": "3.6"
			}
		]
	},
	{
		"name": "Nyitrai Anna",
		"presentations": [
			{
				"id": "11",
				"sectionId": "3.6"
			}
		]
	},
	{
		"name": "Molnár Diána",
		"presentations": [
			{
				"id": "12",
				"sectionId": "3.6"
			}
		]
	}
];

data.sort(
	(a,b)=>{
		if(a.name < b.name) return -1;
		if(a.name > b.name) return 1;
		return 0
	}
);

/* harmony default export */ __webpack_exports__["a"] = (data);



/***/ }),
/* 18 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
class Favourites{
	static toggle(id){
		let favs = Favourites.get();
		if(typeof favs[id] !== 'undefined'){
			delete favs[id];
			Favourites.persist(favs);
			return false;
		}else{
			favs[id] = true;
			Favourites.persist(favs);
			return true;
		}
	}

	static get(){
		let string = window.localStorage.getItem('favourites');
		let favs;
		try{
			favs = JSON.parse(string);
			if(favs === null) favs = {};
		}catch(exception){
			favs = {};
			Favourites.persist(favs);
		}
		return favs;
	}

	static persist(data){
		window.localStorage.setItem('favourites', JSON.stringify(data));
	}

	static isEmpty() {
		let obj = Favourites.get();
		for(let prop in obj) {
			if(obj.hasOwnProperty(prop))
				return false;
		}

		return JSON.stringify(obj) === JSON.stringify({});
	}
}
/* harmony export (immutable) */ __webpack_exports__["a"] = Favourites;


/***/ }),
/* 19 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_screen__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__data_schedule__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__src_date_formatter__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__data_presentations__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__src_data_reader__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__html_favourites_html__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__html_favourites_html___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__html_favourites_html__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__data_rooms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__src_favourites__ = __webpack_require__(18);









/* harmony default export */ __webpack_exports__["a"] = (class extends __WEBPACK_IMPORTED_MODULE_0__src_screen__["a" /* default */] {


	initialize() {
		this.schedule = new __WEBPACK_IMPORTED_MODULE_4__src_data_reader__["a" /* default */](__WEBPACK_IMPORTED_MODULE_1__data_schedule__["a" /* default */]);
		this.presentations = new __WEBPACK_IMPORTED_MODULE_4__src_data_reader__["a" /* default */](__WEBPACK_IMPORTED_MODULE_3__data_presentations__["a" /* default */])
		this.leftButtonIcon = "fas fa-angle-left"
		this.leftButtonClick = (event) => {this.screenManager.getScreen('schedule').activate(true);}
		this.rooms = new __WEBPACK_IMPORTED_MODULE_4__src_data_reader__["a" /* default */](__WEBPACK_IMPORTED_MODULE_6__data_rooms__["a" /* default */]);

		this.dateformatter = new __WEBPACK_IMPORTED_MODULE_2__src_date_formatter__["a" /* default */]();
		this.$content.innerHTML = __WEBPACK_IMPORTED_MODULE_5__html_favourites_html___default.a;
	}

	setup(options) {
		this.favourites = __WEBPACK_IMPORTED_MODULE_7__src_favourites__["a" /* default */].get();

		this.$content.querySelector('.presentations').innerHTML = '';

		for(let favId in this.favourites){
			let presentation = this.presentations.get('fullId', favId);
			this.addPresentation(presentation);
		}

	}

	addPresentation(presentation){
		let $presentation = document.createElement('div');
		$presentation.dataset.clickName = 'fav-toggle';
		$presentation.dataset.fullId = presentation.fullId;
		let icon = this.favourites[presentation.fullId] ? 'fa' : 'far'
		let section = this.schedule.get('id', presentation.sectionId);
		let dateFormatter = new __WEBPACK_IMPORTED_MODULE_2__src_date_formatter__["a" /* default */]();
		let room = this.rooms.get('id', section.room);
		$presentation.innerHTML = `
			<i class="${icon} fa-star"></i>
			<div class="title"><span class="from">${new __WEBPACK_IMPORTED_MODULE_2__src_date_formatter__["a" /* default */]().getTime(presentation.from)}</span> ${presentation.title}</div>
			<div class="authors">${presentation.authors} (${presentation.location})</div>
			<div class="room">${room.name} (${room.number}) terem<br>${dateFormatter.getFullDate(presentation.from)} ${dateFormatter.getTime(presentation.from)} - ${dateFormatter.getTime(presentation.to)}</div>
		
		`;
		this.$content.querySelector('.presentations').appendChild($presentation);
	}

	click($source, name) {
		switch (name) {
			case 'fav-toggle':
				let added = __WEBPACK_IMPORTED_MODULE_7__src_favourites__["a" /* default */].toggle($source.dataset.fullId);

				if(added){
					$source.querySelector('.fa-star').classList.remove('far');
					$source.querySelector('.fa-star').classList.add('fa');
				}else{
					$source.querySelector('.fa-star').classList.remove('fa');
					$source.querySelector('.fa-star').classList.add('far');
				}

				break;
		}
	}



});

/***/ }),
/* 20 */
/***/ (function(module, exports) {

module.exports = "<div class=\"presentations\">\n\n</div>";

/***/ }),
/* 21 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_screen__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__html_abstracts_html__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__html_abstracts_html___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__html_abstracts_html__);


/* harmony default export */ __webpack_exports__["a"] = (class extends __WEBPACK_IMPORTED_MODULE_0__src_screen__["a" /* default */] {

	constructor() {
		super();
	}

	initialize(){
		this.leftButtonIcon = "fas fa-angle-left"
		this.leftButtonClick = (event) => {this.screenManager.getScreen('index').activate(true);}
		this.$content.innerHTML = __WEBPACK_IMPORTED_MODULE_1__html_abstracts_html___default.a;
	}

});

/***/ }),
/* 22 */
/***/ (function(module, exports) {

module.exports = "<iframe src=\"./MRT_2018_ABSZTRAKT.pdf\"></iframe>";

/***/ }),
/* 23 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_screen__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__html_sponsors_html__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__html_sponsors_html___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__html_sponsors_html__);


/* harmony default export */ __webpack_exports__["a"] = (class extends __WEBPACK_IMPORTED_MODULE_0__src_screen__["a" /* default */] {

	constructor() {
		super();
	}

	initialize(){
		this.leftButtonIcon = "fas fa-angle-left"
		this.leftButtonClick = (event) => {this.screenManager.getScreen('index').activate(true);}
		this.$content.innerHTML = __WEBPACK_IMPORTED_MODULE_1__html_sponsors_html___default.a;
	}

});

/***/ }),
/* 24 */
/***/ (function(module, exports) {

module.exports = "<h1>Kiemelt szponzor</h1>\nGE Healthcare<br>\n\n<h1>Főszponzorok</h1>\nAstromedic<br>\nBayer Hungária<br>\nBoehringer Ingelheim<br>\nCook Medical Hungary<br>\nEwopharma (Bracco)<br>\nForex Medical<br>\nHitachi Medical Systems<br>\nRadizone Diagnost-X<br>\nSiemens Healthineers<br>\n\n<h1>Szponzorok</h1>\nAffidea Magyarország<br>\nBELUX Csoport<br>\nBiometica+MIDES<br>\nBluemed<br>\nDoktor24<br>\nEIZO Austria<br>\nEuromedic Pharma<br>\nFIBRE Medical<br>\nICONOMIX<br>\nKépdoktor<br>\nKheiron Medical Technologies<br>\nMedimat<br>\nMedipixel (Samsung)<br>\nOrszágos Teleradiológiai Rendszer<br>\nNovelmedix<br>\nPécsi Diagnosztikai Központ<br>\nPécsi Tudományegyetem Általános Orvostudományi Kar<br>\nPremed Pharma<br>\nPremied G-Med<br>\nRADITEC<br>\nSonarmed (Samsung)<br>\nSW-IT<br>\nTARASCO<br>\nTelemedicine Clinic – Barcelona<br>\nT&amp;G Health<br>";

/***/ }),
/* 25 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_screen__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__html_organizers_html__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__html_organizers_html___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__html_organizers_html__);


/* harmony default export */ __webpack_exports__["a"] = (class extends __WEBPACK_IMPORTED_MODULE_0__src_screen__["a" /* default */] {

	constructor() {
		super();
	}

	initialize(){
		this.leftButtonIcon = "fas fa-angle-left"
		this.leftButtonClick = (event) => {this.screenManager.getScreen('index').activate(true);}
		this.$content.innerHTML = __WEBPACK_IMPORTED_MODULE_1__html_organizers_html___default.a;
	}

});

/***/ }),
/* 26 */
/***/ (function(module, exports) {

module.exports = "<h1>Szervező bizottság</h1>\nElnök: Prof. dr. Bogner Péter<br>\nHajduné Udvarácz Veronika<br>\nDr. Kiefer Éva<br>\nKiefer Zoltán<br>\nKittkáné Bódi Katalin<br>\nDr. Olajos Eszter<br>\nPataki Beatrix<br>\nTensi Aviation Kft.<br>\nDr. Weninger Csaba<br>\nZöldi Péter<br>";

/***/ }),
/* 27 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_screen__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__html_science_html__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__html_science_html___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__html_science_html__);


/* harmony default export */ __webpack_exports__["a"] = (class extends __WEBPACK_IMPORTED_MODULE_0__src_screen__["a" /* default */] {

	constructor() {
		super();
	}

	initialize(){
		this.leftButtonIcon = "fas fa-angle-left"
		this.leftButtonClick = (event) => {this.screenManager.getScreen('index').activate(true);}
		this.$content.innerHTML = __WEBPACK_IMPORTED_MODULE_1__html_science_html___default.a;
	}

});

/***/ }),
/* 28 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_screen__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__html_programs_html__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__html_programs_html___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__html_programs_html__);


/* harmony default export */ __webpack_exports__["a"] = (class extends __WEBPACK_IMPORTED_MODULE_0__src_screen__["a" /* default */] {

	constructor() {
		super();
	}

	initialize(){
		this.leftButtonIcon = "fas fa-angle-left"
		this.leftButtonClick = (event) => {this.screenManager.getScreen('index').activate(true);}
		this.$content.innerHTML = __WEBPACK_IMPORTED_MODULE_1__html_programs_html___default.a;
	}

	click($source, name) {
		switch (name) {
			case 'external':
				console.log($source.dataset.url)
				window.open($source.dataset.clickUrl, "_system");
				break;
		}
	}
});

/***/ }),
/* 29 */
/***/ (function(module, exports) {

module.exports = "<h1>Tudományos bizottság</h1>\nProf. dr. Bogner Péter<br>\nDr. Battyáni István<br>\nDr. Rostás Tamás<br>\nProf. dr. Berényi Ervin<br>\nDr. Bágyi Péter<br>\nProf. dr. Bérczi Viktor<br>\nProf. dr. Gődény Mária<br>\nProf. dr. Szikora István<br>\nProf. dr. Barsi Péter<br>\nProf. dr. Palkó András<br>\nDr. Járay Ákos<br>\nDr. Hetényi Szabolcs<br>\nDr. Forrai Gábor<br>\nDr. Tárnoki Dávid<br>\nDr. Tárnoki Ádám<br>";

/***/ }),
/* 30 */
/***/ (function(module, exports) {

module.exports = "A konferencia résztvevői számára 2018. június 21-én 18:30 és 21:00 óra közötti időben\nmegnyitja kapuját a Középkori Egyetem, mely névkitűzővel, vagy a konferenciára történő\nhivatkozással ingyenesen, egyénileg látogatható.<br>\n<a href=\"#\" data-click-name=\"external\" data-click-url=\"http://www.kozepkoriegyetem.hu/\">Középkori Egyetem</a>\n";

/***/ }),
/* 31 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_screen__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__html_rsna_html__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__html_rsna_html___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__html_rsna_html__);


/* harmony default export */ __webpack_exports__["a"] = (class extends __WEBPACK_IMPORTED_MODULE_0__src_screen__["a" /* default */] {

	constructor() {
		super();
	}

	initialize(){
		this.leftButtonIcon = "fas fa-angle-left"
		this.leftButtonClick = (event) => {this.screenManager.getScreen('index').activate(true);}
		this.$content.innerHTML = __WEBPACK_IMPORTED_MODULE_1__html_rsna_html___default.a;
	}
	click($source, name) {
		switch (name) {
			case 'external':
				console.log($source.dataset.url)
				window.open($source.dataset.clickUrl, "_system");
				break;
		}
	}
});

/***/ }),
/* 32 */
/***/ (function(module, exports) {

module.exports = "<center>\n\tAz RSNA Live rendszer biztosítja a 06.23. szombaton 12:50-14:00 között a CHOLNOKY (1) Teremben megrendezésre kerülő\n\tKvíz megoldását.<br>\n\tA Kvíz támogatója:<br>\n\tSiemens Healthiness<br>\n\t<a href=\"#\" data-click-name=\"external\" data-click-url=\"https://live.rsna.org/\">Játéktér</a><br>\n\t<a href=\"#\" data-click-name=\"external\" data-click-url=\"http://www.mrt2018pecs.hu/upload/RSNA_LIVE.pdf\">Részletes\n\t\tinformációk</a>\n</center>";

/***/ })
/******/ ]);