<?php

$string = file_get_contents('data.csv');
$lines = explode("\n", $string);

print_r($lines);

$presentations = [];
$events = [];
$sections = [];
$users = [];

foreach($lines as $line){
	$data = str_getcsv($line, "\t", '');
	if($data[1] == '*'){
		$events[] = $data;
	}elseif($data[1] && $data[2]){
		$presentations[] = $data;
	}elseif ($data[1]){
		$sections[] = $data;
	}
}



foreach ($presentations as $key=>$value){
	$presentations[$key] = [
		"id"=>$value[2],
		"sectionId"=>$value[1],
		"from"=>'2018-06-'.trim($value[4]).'T'.str_pad(trim($value[6]), 5, '0', STR_PAD_LEFT),
		"to"=>'2018-06-'.trim($value[4]).'T'.str_pad(trim($value[7]), 5, '0', STR_PAD_LEFT),
		"title"=>trim($value[11]),
		"performer"=>trim($value[8]),
		"authors"=>trim($value[9]),
		"location"=>trim($value[10]),
	];
}

$userstmp = [];
foreach ($presentations as $presentation){
	if(!array_key_exists($presentation['performer'], $userstmp)){
		$userstmp[$presentation['performer']] = ['name'=>$presentation['performer'], 'presentations'=>[]];
	}
	$userstmp[$presentation['performer']]['presentations'][] = ['id'=>$presentation['id'], 'sectionId'=>$presentation['sectionId']];
}

foreach ($userstmp as $user) $users[] = $user;



$schedule = [];

foreach ($sections as $key=>$value){
	$schedule[] = [
		"type"=>'section',
		"from"=>null,
		"to"=>null,
		"title"=> trim(substr($value[6], 10)),
		"room"=>trim($value[5]),
		"id"=>trim($value[1])
		];
}

foreach ($schedule as $key=>$section){
	foreach ($presentations as $presentation){
		if($presentation['sectionId'] == $section['id']){
			if($schedule[$key]['from'] === null || $schedule[$key]['from']>$presentation['from']) $schedule[$key]['from'] = $presentation['from'];
			if($schedule[$key]['to'] === null || $schedule[$key]['to']<$presentation['to']) $schedule[$key]['to'] = $presentation['to'];
		}
	}
}

foreach ($events as $key=>$value){
	$schedule[] = [
		"type"=>'event',
		"from"=>'2018-06-'.trim($value[4]).'T'.trim($value[6]),
		"to"=>'2018-06-'.trim($value[4]).'T'.trim($value[7]),
		"title"=>trim($value[8])
	];
}


//print_r($schedule);
//file_put_contents('schedule.js', json_encode($schedule, JSON_UNESCAPED_UNICODE + JSON_PRETTY_PRINT + JSON_UNESCAPED_SLASHES));
file_put_contents('users.js', json_encode($users, JSON_UNESCAPED_UNICODE + JSON_PRETTY_PRINT + JSON_UNESCAPED_SLASHES));
//file_put_contents('presentations.js', json_encode($presentations, JSON_UNESCAPED_UNICODE + JSON_PRETTY_PRINT + JSON_UNESCAPED_SLASHES));

/*
 * 	{
		id: '1',
		sectionId: '1.1',
		from: new Date('2018-06-21T10:30'),
		to: new Date('2018-06-21T10:50'),
		title: 'Hibrid  képalkotás',
		performer: 'Pávics László',
		authors: 'Pávics László, Besenyi Zsuzsanna',
		location: 'Szeged'
	}

	{
		type: 'section',
		from: new Date('2018-06-21T10:30'),
		to: new Date('2018-06-21T12:45'),
		title: 'Ifjúsági bizottság workshop',
		room: 3,
		id: '1.3'
	},
	{
		type: 'event',
		from: new Date('2018-06-21T12:40'),
		to: new Date('2018-06-21T14:00'),
		title: 'Ebédszünet',
	}
 */