export default class Favourites{
	static toggle(id){
		let favs = Favourites.get();
		if(typeof favs[id] !== 'undefined'){
			delete favs[id];
			Favourites.persist(favs);
			return false;
		}else{
			favs[id] = true;
			Favourites.persist(favs);
			return true;
		}
	}

	static get(){
		let string = window.localStorage.getItem('favourites');
		let favs;
		try{
			favs = JSON.parse(string);
			if(favs === null) favs = {};
		}catch(exception){
			favs = {};
			Favourites.persist(favs);
		}
		return favs;
	}

	static persist(data){
		window.localStorage.setItem('favourites', JSON.stringify(data));
	}

	static isEmpty() {
		let obj = Favourites.get();
		for(let prop in obj) {
			if(obj.hasOwnProperty(prop))
				return false;
		}

		return JSON.stringify(obj) === JSON.stringify({});
	}
}