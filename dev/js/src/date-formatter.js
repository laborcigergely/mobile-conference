export default class {

	constructor() {
		this.months = 'január,február,március,április,május,június,július,augusztus,szeptember,október,november,december'.split(',');
		this.days = 'vasárnap,hétfő,kedd,szerda,csütörtök,péntek,szombat'.split(',');
	}

	getFullDate(date) {
		let day = date.getDate();
		let monthIndex = date.getMonth();
		let year = date.getFullYear();
		return year + '. ' + this.months[monthIndex] + ' ' + day + '., '+this.days[date.getDay()];
	}

	getTime(date){
		return date.getHours().toString().padStart(2,'0')+':'+date.getMinutes().toString().padStart(2,'0');
	}
}