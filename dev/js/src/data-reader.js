export default class DataReader {
	constructor(data) {
		this.data = data;
		/*
		this.data = data.sort((a, b) => {
			if (a[this.sort] < b[this.sort]) return -1;
			if (a[this.sort] > b[this.sort]) return 1;
			return 0;
		});*/
	}

	find(options) {

		let source = [];
		this.data.forEach(value => {source.push(value)});
		let matches = [];

		for (let field in options) {
			let pattern = options[field];
			let re = new RegExp(pattern);
			matches = [];
			source.forEach(element => {
				if (re.test(element[field])) {
					matches.push(element);
				}
			});
			source = matches;
		}

		return matches;
	}

	get(field, value){
		let found = null;
		this.data.forEach(row=>{
			if(row[field] == value) found = row;
		});
		return found;
	}

	getAll(field, value){
		let found = [];
		this.data.forEach(row=>{
			if(row[field] == value) found.push(row);
		});
		return found;
	}

	findStartsWith(field, pattern){
		let search = {};
		search[field] = DataReader.escapeRegExp(pattern + '.')+'(.*)';
		return this.find(search);
	}

	static escapeRegExp(str) {
		return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
	}
}