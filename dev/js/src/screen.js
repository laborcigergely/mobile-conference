export default class Screen{

	init($screen, screenManager){
		this.$screen = $screen;
		this.screenManager = screenManager;

		this.$header = document.createElement('header');
		this.$header.innerHTML = '<span class="left"></span>' + '<span class="title"></span>' +'<span class="right"></span>'
		this.title = this.$screen.dataset.title;
		this.$screen.appendChild(this.$header);



		this.$content = document.createElement('article');
		this.$screen.appendChild(this.$content);

		if(this.getTemplate('screen')) this.$content.innerHTML = this.getTemplate('screen');

		this.initialize();

		['click'].forEach(eventName=> {

			this.$header.querySelector('span.left').addEventListener(eventName, event=>this.leftButtonClick(event));
			this.$header.querySelector('span.right').addEventListener(eventName, event=>this.rightButtonClick(event));

			this.$screen.addEventListener(eventName, event => {
				let $source = event.target;
				do {
					if ($source.dataset && typeof $source.dataset.clickName !== 'undefined') {
						this.click($source, $source.dataset.clickName, event);
						event.preventDefault();
						event.stopPropagation();
						break;
					} else {
						$source = $source.parentElement;
					}
				} while ($source !== this.$screen.parentElement);
			})
		});
	}

	initialize(){}
	setup(options){}

	activate(options = false){
		if(options !== false) { this.setup(options); }
		this.$content.scrollTop = 0;
		this.screenManager.activateScreen(this);
		return this;
	}

	set title(value){
		this.$header.querySelector('.title').innerHTML = value;
	}

	get title(){
		return this.$header.querySelector('.title').innerHTML;
	}

	set leftButtonIcon(icon){
		this.$header.querySelector('.left').innerHTML = '<i class="'+icon+'"></i>';
	}

	set rightButtonIcon(icon){
		this.$header.querySelector('.right').innerHTML = '<i class="'+icon+'"></i>';
	}

	leftButtonClick(event){}
	rightButtonClick(event){}

	getTemplate(name){
		let $template = this.$screen.querySelector('template[name='+name+']');
		if($template) return $template.innerHTML;
		return null;
	}

	click($source, name, event){
		console.log($source, name, event);
	}


}