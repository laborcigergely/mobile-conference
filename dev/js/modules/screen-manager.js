import Index from "../screens/index";
import Users from "../screens/users";
import Schedule from "../screens/schedule";
import Section from "../screens/section";
import Abstracts from "../screens/abstracts";
import Favourites from "../screens/favourites";
import Sponsors from "../screens/sponsors";
import Organizers from "../screens/organizers";
import Science from "../screens/science";
import Programs from "../screens/programs";
import RSNA from "../screens/rsna";

export default class ScreenManager{

	static init(){
		let screenManager = new ScreenManager();
		screenManager.add(new Users(), 'users');
		screenManager.add(new Schedule(), 'schedule');
		screenManager.add(new Section(), 'section');
		screenManager.add(new Favourites(), 'favourites');
		screenManager.add(new Abstracts(), 'abstracts');
		screenManager.add(new Sponsors(), 'sponsors');
		screenManager.add(new Organizers(), 'organizers');
		screenManager.add(new Science(), 'science');
		screenManager.add(new Programs(), 'programs');
		screenManager.add(new RSNA(), 'rsna');
		screenManager.add(new Index(), 'index').activate();
	}

	activateScreen(screen){
		document.querySelectorAll('.active[data-screen]').forEach($screen=> $screen.classList.remove('active'));
		screen.$screen.classList.add('active');
	}

	getScreen(id){
		return this.screens[id];
	}

	constructor(){
		this.screens = {};
	}

	add(screen, id){
		screen.init(document.querySelector('[data-screen='+id+']'), this);
		this.screens[id] = screen;
		return screen;
	}
}