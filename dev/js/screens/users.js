import Screen from "../src/screen";
import users from "../data/users";
import presentations from "../data/presentations";
import DataReader from "../src/data-reader";
import DateFormatter from "../src/date-formatter";
import rooms from "../data/rooms";
import sections from "../data/schedule";

export default class extends Screen {
	initialize() {
		this.leftButtonIcon = "fas fa-angle-left"
		this.leftButtonClick = (event) => {this.screenManager.getScreen('index').activate(true);}
		this.presentations = new DataReader(presentations);
		this.rooms = new DataReader(rooms);
		this.setup();
	}

	setup() {
		users.forEach(user => {
			if (!user.name) return;
			let $user = document.createElement('div');
			$user.classList.add('user');
			$user.innerHTML = `
				<i class="far fa-user"></i> <span class="name">${user.name}</span>
			`;
			this.$content.appendChild($user);

			user.presentations.forEach(presentationId => {
				let $presentation = document.createElement('div');
				$presentation.classList.add('presentation');
				let presentation = new DataReader(presentations).get('fullId', presentationId['sectionId']+'.'+presentationId.id);
				let section = new DataReader(sections).get('id', presentationId.sectionId);
				let room = this.rooms.get('id', section.room);
				$presentation.dataset.sectionId = section.id;
				$presentation.dataset.clickName = 'section';

				$presentation.innerHTML += `
							<div class="title">${presentation.title}</div>
							<span class="from">${new DateFormatter().getFullDate(presentation.from)} ${new DateFormatter().getTime(presentation.from)} / ${room.name} (${room.number}) terem</span> 
							<div class="authors">${presentation.authors} (${presentation.location})</div>
				`;
				this.$content.appendChild($presentation);

			})
		});
	}

	click($source, name) {
		switch (name) {
			case 'section':
				this.screenManager.getScreen('section').activate().setup({sectionId: $source.dataset.sectionId});
				break;
		}
	}

}