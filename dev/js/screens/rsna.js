import Screen from "../src/screen";
import html from "../html/rsna.html"
export default class extends Screen {

	constructor() {
		super();
	}

	initialize(){
		this.leftButtonIcon = "fas fa-angle-left"
		this.leftButtonClick = (event) => {this.screenManager.getScreen('index').activate(true);}
		this.$content.innerHTML = html;
	}
	click($source, name) {
		switch (name) {
			case 'external':
				console.log($source.dataset.url)
				window.open($source.dataset.clickUrl, "_system");
				break;
		}
	}
}