import Screen from "../src/screen";
import schedule from "../data/schedule";
import DateFormatter from "../src/date-formatter";
import presentations from "../data/presentations";
import DataReader from "../src/data-reader";
import favouritesHTML from "../html/favourites.html";
import rooms from "../data/rooms";
import Favourites from "../src/favourites";

export default class extends Screen {


	initialize() {
		this.schedule = new DataReader(schedule);
		this.presentations = new DataReader(presentations)
		this.leftButtonIcon = "fas fa-angle-left"
		this.leftButtonClick = (event) => {this.screenManager.getScreen('schedule').activate(true);}
		this.rooms = new DataReader(rooms);

		this.dateformatter = new DateFormatter();
		this.$content.innerHTML = favouritesHTML;
	}

	setup(options) {
		this.favourites = Favourites.get();

		this.$content.querySelector('.presentations').innerHTML = '';

		for(let favId in this.favourites){
			let presentation = this.presentations.get('fullId', favId);
			this.addPresentation(presentation);
		}

	}

	addPresentation(presentation){
		let $presentation = document.createElement('div');
		$presentation.dataset.clickName = 'fav-toggle';
		$presentation.dataset.fullId = presentation.fullId;
		let icon = this.favourites[presentation.fullId] ? 'fa' : 'far'
		let section = this.schedule.get('id', presentation.sectionId);
		let dateFormatter = new DateFormatter();
		let room = this.rooms.get('id', section.room);
		$presentation.innerHTML = `
			<i class="${icon} fa-star"></i>
			<div class="title"><span class="from">${new DateFormatter().getTime(presentation.from)}</span> ${presentation.title}</div>
			<div class="authors">${presentation.authors} (${presentation.location})</div>
			<div class="room">${room.name} (${room.number}) terem<br>${dateFormatter.getFullDate(presentation.from)} ${dateFormatter.getTime(presentation.from)} - ${dateFormatter.getTime(presentation.to)}</div>
		
		`;
		this.$content.querySelector('.presentations').appendChild($presentation);
	}

	click($source, name) {
		switch (name) {
			case 'fav-toggle':
				let added = Favourites.toggle($source.dataset.fullId);

				if(added){
					$source.querySelector('.fa-star').classList.remove('far');
					$source.querySelector('.fa-star').classList.add('fa');
				}else{
					$source.querySelector('.fa-star').classList.remove('fa');
					$source.querySelector('.fa-star').classList.add('far');
				}

				break;
		}
	}



}