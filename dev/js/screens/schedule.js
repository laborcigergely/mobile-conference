import Screen from "../src/screen";
import schedule from "../data/schedule";
import rooms from "../data/rooms";
import DataReader from "../src/data-reader";
import DateFormatter from "../src/date-formatter";
import Favourites from "../src/favourites";

export default class extends Screen {


	initialize() {
		this.schedule = new DataReader(schedule);
		this.rooms = new DataReader(rooms);
		this.leftButtonIcon = "fas fa-angle-left"
		this.leftButtonClick = (event) => {this.screenManager.getScreen('index').activate(true);}
		this.dateformatter = new DateFormatter();
	}

	setup() {
		this.$content.innerHTML = '';
		let currentday = null;
		this.schedule.data.forEach(sched => {
			let day = sched.from.getFullYear() + '-' + (sched.from.getMonth() + 1) + '-' + sched.from.getDate();
			if (day !== currentday) {
				currentday = day;
				this.renderDayBar(sched.from);
			}
			switch (sched.type) {
				case 'event':
					this.renderEvent(sched);
					break;
				case 'section':
					this.renderSection(sched);
					break;
			}
		});
		this.updateFavButton();
	}

	renderDayBar(date) {
		let $bar = document.createElement('div');
		$bar.classList.add('day-bar');
		$bar.innerHTML = this.dateformatter.getFullDate(date);
		this.$content.appendChild($bar)
	}

	renderEvent(event) {
		let $bar = document.createElement('div');
		$bar.classList.add('event-bar');
		$bar.innerHTML = this.dateformatter.getTime(event.from) + ' - ' + this.dateformatter.getTime(event.to) + ' ' + event.title;
		this.$content.appendChild($bar);

	}

	renderSection(event) {
		let $bar = document.createElement('div');
		$bar.classList.add('section-bar');
		$bar.dataset.clickName = 'section';
		$bar.dataset.sectionId = event.id;
		let room = this.rooms.get('id', event.room);
		$bar.innerHTML = this.dateformatter.getTime(event.from) + ' - ' + this.dateformatter.getTime(event.to) + ' / ' + room.name + ' (' + room.number + ') terem ' + '<h1>' + event.title + '</h1>';
		this.$content.appendChild($bar);
	}

	click($source, name) {
		switch (name) {
			case 'section':
				this.screenManager.getScreen('section').activate().setup({sectionId: $source.dataset.sectionId});
				break;
		}
	}

	updateFavButton(){
		if(Favourites.isEmpty()){
			this.rightButtonIcon = "";
			this.rightButtonClick = (event) => {}
		}else{

			this.rightButtonIcon = "fa fa-star";
			this.rightButtonClick = (event) => {this.screenManager.getScreen('favourites').activate(true);}
		}
	}

}