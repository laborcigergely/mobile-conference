import Screen from "../src/screen";
import indexHtml from "../html/index.html"
export default class extends Screen {

	constructor() {
		super();
	}

	initialize(){
		this.$content.innerHTML =indexHtml;
	}

	click($source, name) {
		switch (name) {
			case 'menu':
				this.screenManager.getScreen($source.dataset.target).activate().setup();
				break;
			case 'external':
				console.log($source.dataset.url)
				window.open($source.dataset.clickUrl, "_system");
				break;
		}
	}


}