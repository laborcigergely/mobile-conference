import Screen from "../src/screen";
import schedule from "../data/schedule";
import DateFormatter from "../src/date-formatter";
import presentations from "../data/presentations";
import DataReader from "../src/data-reader";
import sectionHTML from "../html/section.html";
import rooms from "../data/rooms";
import Favourites from "../src/favourites";

export default class extends Screen {


	initialize() {
		this.schedule = new DataReader(schedule);
		this.presentations = new DataReader(presentations)
		this.leftButtonIcon = "fas fa-angle-left"
		this.leftButtonClick = (event) => {this.screenManager.getScreen('schedule').activate(true);}
		this.rooms = new DataReader(rooms);

		this.dateformatter = new DateFormatter();
		this.$content.innerHTML = sectionHTML;
	}

	setup(options) {
		this.favourites = Favourites.get();
		this.section = this.schedule.find({id: options.sectionId})[0];
		let presentations = this.presentations.getAll('sectionId', this.section.id);



		let room = this.rooms.get('id', this.section.room)
		this.$content.querySelector('.date').innerHTML = new DateFormatter().getFullDate(this.section.from);
		this.$content.querySelector('.time').innerHTML = new DateFormatter().getTime(this.section.from) + ' - ' + new DateFormatter().getTime(this.section.to);
		this.$content.querySelector('.location').innerHTML = room.name + ' ('+room.number+') terem';
		this.$content.querySelector('.section-title').innerHTML = this.section.title;
		this.$content.querySelector('.presentations').innerHTML = '';
		presentations.forEach(presentation=>{
			this.addPresentation(presentation);
		});
		this.updateFavButton();
	}

	addPresentation(presentation){
		let $presentation = document.createElement('div');
		$presentation.dataset.clickName = 'fav-toggle';
		$presentation.dataset.fullId = presentation.fullId;
		let icon = this.favourites[presentation.fullId] ? 'fa' : 'far'
		$presentation.innerHTML = `
			<i class="${icon} fa-star"></i>
			<div class="title"><span class="from">${new DateFormatter().getTime(presentation.from)}</span> ${presentation.title}</div>
			<div class="authors">${presentation.authors} (${presentation.location})</div>
		
		`;
		this.$content.querySelector('.presentations').appendChild($presentation);
	}

	click($source, name) {
		switch (name) {
			case 'fav-toggle':
				let added = Favourites.toggle($source.dataset.fullId);

				if(added){
					$source.querySelector('.fa-star').classList.remove('far');
					$source.querySelector('.fa-star').classList.add('fa');
				}else{
					$source.querySelector('.fa-star').classList.remove('fa');
					$source.querySelector('.fa-star').classList.add('far');
				}

				this.updateFavButton();
				break;
		}
	}

	updateFavButton(){
		if(Favourites.isEmpty()){
			this.rightButtonIcon = "";
			this.rightButtonClick = (event) => {}
		}else{
			this.rightButtonIcon = "fa fa-star";
			this.rightButtonClick = (event) => {this.screenManager.getScreen('favourites').activate(true);}
		}
	}


}