import Screen from "../src/screen";
import html from "../html/abstracts.html"
export default class extends Screen {

	constructor() {
		super();
	}

	initialize(){
		this.leftButtonIcon = "fas fa-angle-left"
		this.leftButtonClick = (event) => {this.screenManager.getScreen('index').activate(true);}
		this.$content.innerHTML = html;
	}

}