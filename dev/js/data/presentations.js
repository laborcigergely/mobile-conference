let data = [
	{
		"id": "1",
		"sectionId": "1.1",
		"from": "2018-06-21T10:30",
		"to": "2018-06-21T10:50",
		"title": "Hibrid képalkotás",
		"performer": "Pávics László",
		"authors": "Pávics László, Besenyi Zsuzsanna",
		"location": "Szeged"
	},
	{
		"id": "2",
		"sectionId": "1.1",
		"from": "2018-06-21T10:50",
		"to": "2018-06-21T11:00",
		"title": "Hibrid képalkotás a spodylarthropathiák diagnosztikájában",
		"performer": "Besenyi Zsuzsanna",
		"authors": "Besenyi Zsuzsanna, Bakos Annamária, Urbán Szabolcs, Hemelein Rita, Kovács László, Pávics László",
		"location": "Szeged"
	},
	{
		"id": "3",
		"sectionId": "1.1",
		"from": "2018-06-21T11:00",
		"to": "2018-06-21T11:10",
		"title": "Teljes test CT vizsgálattal szerzett kezdeti tapasztalataink myeloma multiplexes betegeknél",
		"performer": "Séllei Ágnes",
		"authors": "Séllei Ágnes, Komáromi Klaudia, Modok Szabolcs, Lengyel Zsuzsanna, Palkó András",
		"location": "Szeged"
	},
	{
		"id": "4",
		"sectionId": "1.1",
		"from": "2018-06-21T11:10",
		"to": "2018-06-21T11:20",
		"title": "A modern képalkotó vizsgálatok alkalmazásának irányelvei myeloma multiplexben",
		"performer": "Győri Gabriella",
		"authors": "Győri Gabriella, Körösmezey Gábor, Gaál-Weisinger Júlia, Tárkányi Ilona, Nagy Zsolt György, Demeter Judit",
		"location": "Budapest"
	},
	{
		"id": "5",
		"sectionId": "1.1",
		"from": "2018-06-21T11:20",
		"to": "2018-06-21T11:30",
		"title": "Hibrid képalkotás a gyakorlatban – tapasztalatok egy SPECT/CT/PET készülékkel",
		"performer": "Urbán Szabolcs",
		"authors": "Urbán Szabolcs, Polanek Tünde, Sipka Gábor, Farkas István, Bakos Annamária, Besenyi Zsuzsanna, Pávics László",
		"location": "Szeged"
	},
	{
		"id": "6",
		"sectionId": "1.1",
		"from": "2018-06-21T11:30",
		"to": "2018-06-21T11:40",
		"title": "18F-FDG PET/CT szerepe a nagyérvasculitisek diagnosztikájában",
		"performer": "Bakos Annamária",
		"authors": "Bakos Annamária, Besenyi Zsuzsanna, Urbán Szabolcs, Farkas István, Sipka Gábor, Hemelein Rita, Kovács László, Pávics László",
		"location": "Szeged"
	},
	{
		"id": "7",
		"sectionId": "1.1",
		"from": "2018-06-21T11:40",
		"to": "2018-06-21T11:50",
		"title": "Cardiovascularis intervenciókat követő gyulladásos szövődmények vizsgálata FDG-PET/CT-VEL",
		"performer": "Farkas István",
		"authors": "Farkas István, Besenyi Zsuzsanna, Sághy László, Bakos Annamária, Sipka Gábor, Pávics László",
		"location": "Szeged"
	},
	{
		"id": "8",
		"sectionId": "1.1",
		"from": "2018-06-21T11:50",
		"to": "2018-06-21T12:00",
		"title": "Szomatosztatin receptor SPECT/CT jelentősége neuroendokrin tumorokban",
		"performer": "Sipka Gábor",
		"authors": "Sipka Gábor, Besenyi Zsuzsanna, Bakos Annamária, Farkas István, Pávics László",
		"location": "Szeged"
	},
	{
		"id": "9",
		"sectionId": "1.1",
		"from": "2018-06-21T12:00",
		"to": "2018-06-21T12:10",
		"title": "Nagy Felbontású Agyi SPECT Leképezés a Technológiai Lehetőségek Tükrében",
		"performer": "Kári Béla",
		"authors": "Kári Béla, Wirth András, Hesz Gábor, Bükki Tamás, Fegyvári András, Czibor Sándor, Dabasi Gabriella, Györke Tamás",
		"location": "Budapest"
	},
	{
		"id": "10",
		"sectionId": "1.1",
		"from": "2018-06-21T12:10",
		"to": "2018-06-21T12:20",
		"title": "AIDS-betegségben előforduló szövődmények képalkotó vizsgálata",
		"performer": "Sári Áron",
		"authors": "Sári Áron, Arany Andrea Szilvia, Győri Gabriella, Járay Anna, Szlávik János",
		"location": "Budapest"
	},
	{
		"id": "11",
		"sectionId": "1.1",
		"from": "2018-06-21T12:20",
		"to": "2018-06-21T12:30",
		"title": "FDG PETCT Reporting: Pearls and Pitfalls",
		"performer": "Paul Duffy",
		"authors": "Paul Duffy",
		"location": "Glasgow"
	},
	{
		"id": "1",
		"sectionId": "1.2",
		"from": "2018-06-21T11:00",
		"to": "2018-06-21T11:15",
		"title": "Aktuális trendek a verőérbetegségek intervenciós radiológiai kezelésében",
		"performer": "Horváth Gábor",
		"authors": "Horváth Gábor",
		"location": "Kronach, Németország"
	},
	{
		"id": "2",
		"sectionId": "1.2",
		"from": "2018-06-21T11:15",
		"to": "2018-06-21T11:25",
		"title": "Az iliaca kissing in-stent restenosisok új, eddig nem ismert rizikófaktora",
		"performer": "Vértes Miklós",
		"authors": "Vértes Miklós, Juhász Ildikó Zsófia, Nguyen Tin Dat, Nemes Balázs, Hüttl Kálmán, Dósa Edit",
		"location": "Budapest"
	},
	{
		"id": "3",
		"sectionId": "1.2",
		"from": "2018-06-21T11:25",
		"to": "2018-06-21T11:35",
		"title": "Akut ischaemiás stroke endovascularis ellátásának jellegzetességei a nagy ér occlusio lokalizációja alapján az Országos Klinikai Idegtudományi Intézetben",
		"performer": "Kis Balázs",
		"authors": "Kis Balázs, Orosz Emma, Nagy András, Nardai Sándor, Gubucz István, Berentei Zsolt, Szikora István",
		"location": "Budapest"
	},
	{
		"id": "4",
		"sectionId": "1.2",
		"from": "2018-06-21T11:35",
		"to": "2018-06-21T11:45",
		"title": "Az infrarenalis aorta stenosisok endovascularis terápiájának eredményessége",
		"performer": "Bérczi Ákos",
		"authors": "Bérczi Ákos, Vértes Miklós, Nemes Balázs, Bérczi Viktor, Hüttl Kálmán, Dósa Edit",
		"location": "Budapest"
	},
	{
		"id": "5",
		"sectionId": "1.2",
		"from": "2018-06-21T11:45",
		"to": "2018-06-21T11:55",
		"title": "Traumás intracraniális artéria carotis interna dissectio intervenciós kezelése",
		"performer": "Oláh Csaba",
		"authors": "Oláh Csaba, Tamáska Péter, Lázár István",
		"location": "Miskolc"
	},
	{
		"id": "6",
		"sectionId": "1.2",
		"from": "2018-06-21T11:55",
		"to": "2018-06-21T12:05",
		"title": "Horner-triásszal jelentkező arteria carotis dissectio es pseudoaneurysma - Esetismertetés",
		"performer": "Hernyes Anita",
		"authors": "Hernyes Anita, Daniel Santirso, Forgó Bianka, Szalontai László, Tárnoki Ádám Domonkos, Tárnoki Dávid László, Garami Zsolt",
		"location": "Budapest, Houston"
	},
	{
		"id": "7",
		"sectionId": "1.2",
		"from": "2018-06-21T12:05",
		"to": "2018-06-21T12:15",
		"title": "Egy szokatlan hátterű arteria carotis communis pseudoaneurysma esete",
		"performer": "Jermendy Ádám",
		"authors": "Jermendy Ádám, Sótonyi Péter, Tóth Attila, Hidi László, Merkely Béla",
		"location": "Budapest"
	},
	{
		"id": "8",
		"sectionId": "1.2",
		"from": "2018-06-21T12:15",
		"to": "2018-06-21T12:25",
		"title": "A carotis stentelés során jelentkező cardiovascularis instabilitás és a kialakult ischaemiás léziók kiterjedtsége közötti összefüggés vizsgálata.",
		"performer": "Csizmadia Sándor",
		"authors": "Csizmadia Sándor, Kaszás Zsófia, Klucsai Róbert, Vörös Erika",
		"location": "Szeged"
	},
	{
		"id": "9",
		"sectionId": "1.2",
		"from": "2018-06-21T12:25",
		"to": "2018-06-21T12:35",
		"title": "Transpedalis behatolással végzett alsó végtagi intervenciók",
		"performer": "Csobay-Novák Csaba",
		"authors": "Csobay-Novák Csaba, Nemes Balázs, Dósa Edit, Sarkadi Hunor, Hüttl Kálmán",
		"location": "Budapest"
	},
	{
		"id": "1",
		"sectionId": "1.3",
		"from": "2018-06-21T10:30",
		"to": "2018-06-21T10:45",
		"title": "Bevezető esetek",
		"performer": "Tárnoki Ádám",
		"authors": "Tárnoki Ádám",
		"location": "Budapest"
	},
	{
		"id": "2",
		"sectionId": "1.3",
		"from": "2018-06-21T10:45",
		"to": "2018-06-21T11:30",
		"title": "Rezidensek esetbemutatásai",
		"performer": "Tárnoki Dávid",
		"authors": "Tárnoki Dávid",
		"location": "Budapest"
	},
	{
		"id": "3",
		"sectionId": "1.3",
		"from": "2018-06-21T11:30",
		"to": "2018-06-21T11:40",
		"title": "Hogyan pályázzunk?",
		"performer": "Tárnoki Ádám",
		"authors": "Tárnoki Ádám",
		"location": "Budapest"
	},
	{
		"id": "4",
		"sectionId": "1.3",
		"from": "2018-06-21T11:40",
		"to": "2018-06-21T11:50",
		"title": "Hogyan készítsünk prezentációt?",
		"performer": "Karlinger Kinga",
		"authors": "Karlinger Kinga",
		"location": "Budapest"
	},
	{
		"id": "5",
		"sectionId": "1.3",
		"from": "2018-06-21T11:50",
		"to": "2018-06-21T12:00",
		"title": "ESOR élménybeszámoló",
		"performer": "Tárnoki Dávid",
		"authors": "Tárnoki Dávid",
		"location": "Budapest"
	},
	{
		"id": "6",
		"sectionId": "1.3",
		"from": "2018-06-21T12:00",
		"to": "2018-06-21T12:10",
		"title": "Pályázati és kutatási lehetőségek",
		"performer": "Lánczi Levente",
		"authors": "Lánczi Levente",
		"location": "Debrecen"
	},
	{
		"id": "7",
		"sectionId": "1.3",
		"from": "2018-06-21T12:10",
		"to": "2018-06-21T12:20",
		"title": "Q&A Workshop",
		"performer": "Lánczi Levente",
		"authors": "Lánczi Levente",
		"location": "Debrecen"
	},
	{
		"id": "8",
		"sectionId": "1.3",
		"from": "2018-06-21T12:20",
		"to": "2018-06-21T12:35",
		"title": "Beton anyagok értékelése humándiagnosztikai képalkotó módszerekkel",
		"performer": "Földes Tamás",
		"authors": "Földes Tamás, Lubloy Éva",
		"location": "Szolnok, Budapest"
	},
	{
		"id": "1",
		"sectionId": "1.4",
		"from": "2018-06-21T14:00",
		"to": "2018-06-21T14:15",
		"title": "ILD betegségek csoportjai",
		"performer": "Tárnoki Dávid László",
		"authors": "Tárnoki Dávid László",
		"location": "Budapest"
	},
	{
		"id": "2",
		"sectionId": "1.4",
		"from": "2018-06-21T14:15",
		"to": "2018-06-21T14:30",
		"title": "IPF és klinikai vonatkozásai",
		"performer": "Monostori Zsuzsanna",
		"authors": "Monostori Zsuzsanna",
		"location": "Budapest"
	},
	{
		"id": "3",
		"sectionId": "1.4",
		"from": "2018-06-21T14:30",
		"to": "2018-06-21T14:45",
		"title": "NSIP minta és ami mögötte lehet",
		"performer": "Kerpel-Fronius Anna",
		"authors": "Kerpel-Fronius Anna",
		"location": "Budapest"
	},
	{
		"id": "4",
		"sectionId": "1.4",
		"from": "2018-06-21T14:45",
		"to": "2018-06-21T15:00",
		"title": "Fibrosissal járó betegségek differenciáldiagnosztikája",
		"performer": "Balázs György",
		"authors": "Balázs György",
		"location": "Budapest"
	},
	{
		"id": "5",
		"sectionId": "1.4",
		"from": "2018-06-21T15:00",
		"to": "2018-06-21T15:10",
		"title": "Tüdőgóc(ok)! Most mit csináljak?",
		"performer": "Székely András",
		"authors": "Székely András, Kerpel-Fronius Anna, Bágyi Péter",
		"location": "Debrecen-Budapest"
	},
	{
		"id": "6",
		"sectionId": "1.4",
		"from": "2018-06-21T15:10",
		"to": "2018-06-21T15:20",
		"title": "Nodularis tüdőbetegségek képalkotása: újdonságok 2018-ban, melyekre figyelnünk kell",
		"performer": "Tárnoki Ádám Domonkos",
		"authors": "Tárnoki Ádám Domonkos, Tárnoki Dávid László, Karlinger Kinga",
		"location": "Budapest"
	},
	{
		"id": "7",
		"sectionId": "1.4",
		"from": "2018-06-21T15:20",
		"to": "2018-06-21T15:30",
		"title": "Képes összefoglaló a CT-vezérlet mellkasi biopsziákról - tippek és trükkök",
		"performer": "Futácsi Balázs",
		"authors": "Futácsi Balázs, Magyar Péter, Gál Magdolna, Bánsághi Zoltán",
		"location": "Budapest"
	},
	{
		"id": "8",
		"sectionId": "1.4",
		"from": "2018-06-21T15:30",
		"to": "2018-06-21T15:40",
		"title": "CT vezérelt percutan transthoracalis tűbiposzia eredményei a Debreceni Klinikán",
		"performer": "Nagy Edit Boglárka",
		"authors": "Nagy Edit Boglárka, Csubák Dávid, Belán Ivett, Veisz Richárd, Tóth Judit",
		"location": "Debrecen"
	},
	{
		"id": "9",
		"sectionId": "1.4",
		"from": "2018-06-21T15:40",
		"to": "2018-06-21T15:50",
		"title": "CT-vezérelt transthoracalis vastagtű-biopsziák szövődményeinek gyakorisága és súlyossága",
		"performer": "Kovács Anita",
		"authors": "Kovács Anita, Milassin Péter, Orbán Krisztina, Pálföldi Regina, Palkó András",
		"location": "Szeged, Deszk"
	},
	{
		"id": "10",
		"sectionId": "1.4",
		"from": "2018-06-21T15:50",
		"to": "2018-06-21T16:00",
		"title": "Transthoracalis biopsziák sikeressége és szövődményei",
		"performer": "Orbán Vince",
		"authors": "Orbán Vince, Villányi Réka, Magyar Péter, Bánsághi Zoltán, Futácsi Balázs",
		"location": "Budapest"
	},
	{
		"id": "1",
		"sectionId": "1.5",
		"from": "2018-06-21T14:00",
		"to": "2018-06-21T14:25",
		"title": "Szakmai megjelenés a digitális térben – Antiszociális média, avagy a kutya sem követ",
		"performer": "Zsiros László Róbert",
		"authors": "Zsiros László Róbert",
		"location": "Budapest"
	},
	{
		"id": "2",
		"sectionId": "1.5",
		"from": "2018-06-21T14:25",
		"to": "2018-06-21T14:45",
		"title": "Gamifikáció a graduális és postgraduális képzésben – Tanulás és motiváció",
		"performer": "Damsa Andrei",
		"authors": "Damsa Andrei",
		"location": "Pécs"
	},
	{
		"id": "3",
		"sectionId": "1.5",
		"from": "2018-06-21T14:45",
		"to": "2018-06-21T15:10",
		"title": "A Radiológiai Képszerkesztés Alapjai – DICOM Szelídítés Alapfokon",
		"performer": "Botz Bálint",
		"authors": "Botz Bálint, Járay Ákos",
		"location": "Pécs"
	},
	{
		"id": "4",
		"sectionId": "1.5",
		"from": "2018-06-21T15:10",
		"to": "2018-06-21T15:30",
		"title": "Oktatási célú videók szerkesztése egyszerűen – Videók a boncasztalon",
		"performer": "Zsiros László Róbert",
		"authors": "Zsiros László Róbert",
		"location": "Budapest"
	},
	{
		"id": "5",
		"sectionId": "1.5",
		"from": "2018-06-21T15:30",
		"to": "2018-06-21T15:40",
		"title": "Radiológia oktatás interaktívan – megvalósítás és gyakorlati tapasztalatok",
		"performer": "Olajos Eszter Ajna",
		"authors": "Olajos Eszter Ajna",
		"location": "Budapest"
	},
	{
		"id": "6",
		"sectionId": "1.5",
		"from": "2018-06-21T15:40",
		"to": "2018-06-21T15:50",
		"title": "3D nyomtatás és virtuális tervezés az egészségügyben",
		"performer": "Varga Péter",
		"authors": "Varga Péter, Maróti Péter, Nyitrai Miklós, Jancsó Gábor, Gasz Balázs",
		"location": "Pécs"
	},
	{
		"id": "7",
		"sectionId": "1.5",
		"from": "2018-06-21T15:50",
		"to": "2018-06-21T16:00",
		"title": "Ultrahang a klinikumban: ahogy eddig kevesen látták",
		"performer": "Klimaj Zoltán",
		"authors": "Klimaj Zoltán, Kondákor B., Németh B., Jakab Zs.",
		"location": "Budapest"
	},
	{
		"id": "1",
		"sectionId": "1.6",
		"from": "2018-06-21T14:00",
		"to": "2018-06-21T14:20",
		"title": "Acute female pelvis",
		"performer": "Derchi",
		"authors": "Derchi, Lorenzo",
		"location": "Genova, Italy"
	},
	{
		"id": "2",
		"sectionId": "1.6",
		"from": "2018-06-21T14:20",
		"to": "2018-06-21T14:40",
		"title": "Ultrahang jelentősége az endometriosis kivizsgálásánál",
		"performer": "Koppán Miklós",
		"authors": "Koppán Miklós",
		"location": "Pécs"
	},
	{
		"id": "3",
		"sectionId": "1.6",
		"from": "2018-06-21T14:40",
		"to": "2018-06-21T15:00",
		"title": "MRI jelentősége az endometriosis kivizsgálásánál",
		"performer": "Nagy Gyöngyi",
		"authors": "Nagy Gyöngyi",
		"location": "Zalaegerszeg"
	},
	{
		"id": "4",
		"sectionId": "1.6",
		"from": "2018-06-21T15:00",
		"to": "2018-06-21T15:10",
		"title": "A myoma embolizáció hatékonysága, kockázatai, eredményessége - tapasztalataink 580 eset kapcsán",
		"performer": "Bérczi Viktor",
		"authors": "Bérczi Viktor, Tóth Ambrus, Kalina Ildikó, Valcseva Éva, Kozics Dóra, Ács Nándor, Tömösváry Zoltán",
		"location": "Budapest"
	},
	{
		"id": "5",
		"sectionId": "1.6",
		"from": "2018-06-21T15:10",
		"to": "2018-06-21T15:20",
		"title": "A strain elasztrográfia szerepe a férfi meddőségi ultrahang kivizsgálásban",
		"performer": "Karczagi Lilla",
		"authors": "Karczagi Lilla, Fejes Zsuzsanna",
		"location": "Szeged"
	},
	{
		"id": "6",
		"sectionId": "1.6",
		"from": "2018-06-21T15:20",
		"to": "2018-06-21T15:30",
		"title": "Kezdeti tapasztalatok a 99mTc-PSMA-SPECT/CT-vel prosztatarákos betegekben",
		"performer": "Farkas István",
		"authors": "Farkas István, Besenyi Zsuzsanna, Maráz Anikó, Bajory Zoltán, Palkó András, Sipka Gábor, Pávics László",
		"location": "Szeged"
	},
	{
		"id": "7",
		"sectionId": "1.6",
		"from": "2018-06-21T15:30",
		"to": "2018-06-21T15:40",
		"title": "Az ortopédiai fém műtermék redukciós algoritmus (O-MAR) hatékonysága kismedencei szervek CT-vizsgálata során",
		"performer": "Kaposi Novák Pál",
		"authors": "Kaposi Novák Pál, Taewoong Youn, Fejes Bernadett, Magyar Péter, Bérczi Viktor",
		"location": "Budapest"
	},
	{
		"id": "8",
		"sectionId": "1.6",
		"from": "2018-06-21T15:40",
		"to": "2018-06-21T15:50",
		"title": "Az MR szerepe a prosztatarák diagnosztikájában",
		"performer": "Kalina Ildikó",
		"authors": "Kalina Ildikó, Fejér Bence, Pölöskei Gergely, Kovács Dániel, Huszár Andor, Wolf Tamás, Borka Katalin, Szűcs Miklós",
		"location": "Budapest"
	},
	{
		"id": "1",
		"sectionId": "2.1",
		"from": "2018-06-22T8:20",
		"to": "2018-06-22T8:50",
		"title": "Neurális Hálók: Elmélet és Gyakorlat",
		"performer": "Dombi Gergely",
		"authors": "Dombi Gergely",
		"location": "Budapest"
	},
	{
		"id": "2",
		"sectionId": "2.1",
		"from": "2018-06-22T8:50",
		"to": "2018-06-22T9:20",
		"title": "Deep learning in clinical practice",
		"performer": "Joris Wakkie",
		"authors": "Joris Wakkie",
		"location": "Amszterdam, Hollandia"
	},
	{
		"id": "3",
		"sectionId": "2.1",
		"from": "2018-06-22T9:20",
		"to": "2018-06-22T9:45",
		"title": "Mesterséges intelligencia-módszerek szerepe az emlővizsgálatokban",
		"performer": "Kecskeméthy Péter",
		"authors": "Kecskeméthy Péter",
		"location": "London, Anglia"
	},
	{
		"id": "4",
		"sectionId": "2.1",
		"from": "2018-06-22T9:45",
		"to": "2018-06-22T9:55",
		"title": "Mesterséges intelligencia az ízületi radiológiában (Siemens Presentation)",
		"performer": "Gál Andor Viktor",
		"authors": "Gál Andor Viktor, Meszlényi Regina, Edőcs József",
		"location": "Budapest, Győr"
	},
	{
		"id": "5",
		"sectionId": "2.1",
		"from": "2018-06-22T9:55",
		"to": "2018-06-22T10:10",
		"title": "Artificial Intelligence and Future Technologies\n",
		"performer": "Samsung",
		"authors": "Samsung Presentation: Artificial Intelligence and Future Technologies",
		"location": "Samsung"
	},
	{
		"id": "6",
		"sectionId": "2.1",
		"from": "2018-06-22T10:10",
		"to": "2018-06-22T10:20",
		"title": "Mesterséges intelligencia bonus és/vagy malus? Az emberi hülyeség új dimenziói is kezdődhetnek?",
		"performer": "Lombay Béla",
		"authors": "Lombay Béla",
		"location": "Miskolc"
	},
	{
		"id": "1",
		"sectionId": "2.2",
		"from": "2018-06-22T8:30",
		"to": "2018-06-22T8:50",
		"title": "A gócos májelváltozások korszerű MR-diagnosztikája",
		"performer": "Palkó András",
		"authors": "Palkó András",
		"location": "Szeged"
	},
	{
		"id": "2",
		"sectionId": "2.2",
		"from": "2018-06-22T8:50",
		"to": "2018-06-22T9:10",
		"title": "Intervenciós radiológia májtumorokban - a magyar módszer",
		"performer": "Doros Attila",
		"authors": "Doros Attila",
		"location": "Budapest"
	},
	{
		"id": "3",
		"sectionId": "2.2",
		"from": "2018-06-22T9:10",
		"to": "2018-06-22T9:30",
		"title": "A májsebész kérdései és vágyai a diagnosztika során",
		"performer": "Papp András",
		"authors": "Papp András",
		"location": "Pécs"
	},
	{
		"id": "4",
		"sectionId": "2.2",
		"from": "2018-06-22T9:30",
		"to": "2018-06-22T9:50",
		"title": "Májspecifikus MR vizsgálat az Uzsoki Kórházban – válogatás differenciál diagnosztikai kihívást jelentő eseteinkből",
		"performer": "Demjén Boglárka",
		"authors": "Demjén Boglárka",
		"location": "Budapest"
	},
	{
		"id": "5",
		"sectionId": "2.2",
		"from": "2018-06-22T9:50",
		"to": "2018-06-22T10:00",
		"title": "A point shear-wave elasztográfia eredménysessége a májfibrózis stádiumainak diagnosztikájában",
		"performer": "Kaposi Novák Pál",
		"authors": "Kaposi Novák Pál, Kucsa András, Abonyi Margit, Bérczi Viktor",
		"location": "Budapest"
	},
	{
		"id": "6",
		"sectionId": "2.2",
		"from": "2018-06-22T10:00",
		"to": "2018-06-22T10:10",
		"title": "A DEB-TACE és a radioembolizáció helye a kolorektális daganatok májáttéteinek ellátásában.",
		"performer": "Bánsághi Zoltán",
		"authors": "Bánsághi Zoltán",
		"location": "Budapest"
	},
	{
		"id": "7",
		"sectionId": "2.2",
		"from": "2018-06-22T10:10",
		"to": "2018-06-22T10:20",
		"title": "Rupturált májtumor minimál invazív ellátása",
		"performer": "Kónya Júlia Anna",
		"authors": "Kónya Júlia Anna, Varga Márk",
		"location": "Győr"
	},
	{
		"id": "1",
		"sectionId": "2.3",
		"from": "2018-06-22T8:30",
		"to": "2018-06-22T8:50",
		"title": "Izomsérülések diagnosztikája - képalkotás traumatológus szemmel. Olyan esetek bemutatása, amikor a képalkotó diagnosztika alapvető fontosságú a kezelési terv felállításához.",
		"performer": "Nőt László Gergely",
		"authors": "Nőt László Gergely",
		"location": "Pécs"
	},
	{
		"id": "2",
		"sectionId": "2.3",
		"from": "2018-06-22T8:50",
		"to": "2018-06-22T9:10",
		"title": "Izomsérülések ultrahang vizsgálata",
		"performer": "Farbaky Zsófia",
		"authors": "Farbaky Zsófia",
		"location": "Budapest"
	},
	{
		"id": "3",
		"sectionId": "2.3",
		"from": "2018-06-22T9:10",
		"to": "2018-06-22T9:30",
		"title": "Izomsérülések MR diagnosztikája",
		"performer": "Hetényi Szabolcs",
		"authors": "Hetényi Szabolcs",
		"location": "Budapest"
	},
	{
		"id": "4",
		"sectionId": "2.3",
		"from": "2018-06-22T9:30",
		"to": "2018-06-22T9:40",
		"title": "Az MR arthrographia szerepe a glenohumeralis instabilitás diagnosztikájában",
		"performer": "Soltész Judit",
		"authors": "Soltész Judit, Kostyál László",
		"location": "Miskolc"
	},
	{
		"id": "5",
		"sectionId": "2.3",
		"from": "2018-06-22T9:40",
		"to": "2018-06-22T9:50",
		"title": "Keresztszalag pótlás után...",
		"performer": "Tasnádi Tünde",
		"authors": "Tasnádi Tünde, Barta Szabolcs, Tállay András",
		"location": "Békéscsaba, Budapest"
	},
	{
		"id": "6",
		"sectionId": "2.3",
		"from": "2018-06-22T9:50",
		"to": "2018-06-22T10:00",
		"title": "A térdízület az arthroscopia és az MR vizsgálat szemszögéből",
		"performer": "Brzózka Ádám",
		"authors": "Brzózka Ádám, Komáromi Klaudia, Kovács Milán, Polyák Ilona, Palkó András",
		"location": "Szeged"
	},
	{
		"id": "7",
		"sectionId": "2.3",
		"from": "2018-06-22T10:00",
		"to": "2018-06-22T10:10",
		"title": "Lumbalis discus degeneratio kapcsolata a mtDNS kópiaszámmal és a telomér hosszúsággal",
		"performer": "Tárnoki Dávid",
		"authors": "Tárnoki Dávid, Melicher Dóra, Illés Anett, Falus András, Molnár Mária, Szily Marcell, Kovács Dániel, Forgó Bianka, Bérczi Viktor, Kostyál László, Tárnoki Adam, Oláh Csaba",
		"location": "Budapest, Miskolc"
	},
	{
		"id": "8",
		"sectionId": "2.3",
		"from": "2018-06-22T10:10",
		"to": "2018-06-22T10:20",
		"title": "Vállsérülések stádiumbeosztásának összehasonlítása hagyományos röntgendiagnosztika és CT vizsgálat során",
		"performer": "Bánáti Laura",
		"authors": "Bánáti Laura, Palkó András, Polyák Ilona",
		"location": "Szeged"
	},
	{
		"id": "1",
		"sectionId": "2.4",
		"from": "2018-06-22T08:30",
		"to": "2018-06-22T08:50",
		"title": "New developments in vascular ultrasound",
		"performer": "Brkljacic, Boris",
		"authors": "Brkljacic, Boris",
		"location": "Zagreb, Croatia"
	},
	{
		"id": "2",
		"sectionId": "2.4",
		"from": "2018-06-22T08:50",
		"to": "2018-06-22T09:00",
		"title": "A carotis ultrahang és transcranialis doppler metodikák: a houstoni és a magyar protokoll összevetése",
		"performer": "Szalontai László",
		"authors": "Szalontai László, Hernyes Anita, Forgó Bianka, Bérczi Viktor, Tárnoki Dávid László, Tárnoki Ádám Domonkos, Garami Zsolt",
		"location": "Budapest, Houston"
	},
	{
		"id": "3",
		"sectionId": "2.4",
		"from": "2018-06-22T09:00",
		"to": "2018-06-22T09:10",
		"title": "A carotis és femoralis atherosclerosis markereinek összefüggése a telomérhosszal és a mtDNS kópiaszámmal",
		"performer": "Janositz Gréta",
		"authors": "Janositz Gréta, Melicher Dóra, Illés Anett, Gyulay Kata, Jokkel Zsófia, Zsupos Rebeka, Bérczi Viktor, Szabó Helga, Hernyes Anita, Tárnoki Dávid L., Tárnoki Ádám D.",
		"location": "Budapest"
	},
	{
		"id": "4",
		"sectionId": "2.4",
		"from": "2018-06-22T09:10",
		"to": "2018-06-22T09:20",
		"title": "Carotis és vertebralis artériák Doppler görbe elemzése",
		"performer": "Sayed-Ahmad Mustafa",
		"authors": "Sayed-Ahmad Mustafa, Tasnádi Tünde, Botos Zoltán, Tálas Dávid",
		"location": "Békéscsaba"
	},
	{
		"id": "5",
		"sectionId": "2.4",
		"from": "2018-06-22T09:20",
		"to": "2018-06-22T09:30",
		"title": "Genetikai és környezeti hatások az atheroscleroticus fenotípusok kialakulásában: egy követéses ikervizsgálat eredményei",
		"performer": "Tárnoki Ádám",
		"authors": "Tarnoki Adam D, Tarnoki David L, Fagnani Corrado, Medda Emanuela, Pucci Giacomo, Lucatelli Pierleone, Cirelli Carlo, Fanelli Fabrizio, Maurovich-Horvat Pal, Jermendy Adam L, Fejer Bence, Jermendy Gyorgy, Merkely Bela, Baracchini Claudio, Stazi Maria A.",
		"location": "Budapest, Roma, Perugia, Padua"
	},
	{
		"id": "6",
		"sectionId": "2.4",
		"from": "2018-06-22T09:30",
		"to": "2018-06-22T09:40",
		"title": "A carotis és femoralis atheroscleroticus plakkok kialakulásának hátterének átfogó ultrahangos vizsgálata ikrekben",
		"performer": "Szabó Helga",
		"authors": "Szabó Helga, Gyulay Kata, Jokkel Zsófia, Zsupos Rebeka, Janositz Gréta, Bérczi Viktor, Tárnoki Dávid L., Tárnoki Ádám D.",
		"location": "Budapest"
	},
	{
		"id": "7",
		"sectionId": "2.4",
		"from": "2018-06-22T09:40",
		"to": "2018-06-22T09:50",
		"title": "Vaszkuláris diagnosztika új kutatási iránya a Városmajori Klinikán – Kinetikus Képalkotás",
		"performer": "Gyánó Marcell",
		"authors": "Gyánó Marcell, Óriás Viktor, Góg István, Szöllősi Dávid, Szigeti Krisztián, Osváth Szabolcs, Ruzsa Zoltán, Nemes Balázs, Sótonyi Péter",
		"location": "Budapest, Kecskemét"
	},
	{
		"id": "8",
		"sectionId": "2.4",
		"from": "2018-06-22T09:50",
		"to": "2018-06-22T10:00",
		"title": "A pajzsmirigy ultrahang elasztográfiás vizsgálata ikreken",
		"performer": "Fekete Márton",
		"authors": "Fekete Márton, Erdei Mercédesz, Szily Marcell, Dienes András, Persely Alíz, Bitai Zsuzsanna, Hernyes Anita, Stazi Maria Antonietta, Medda Emanuela, Fagnani Corrado, Bérczi Viktor, Tárnoki Ádám Domonkos, Tárnoki Dávid László",
		"location": "Budapest, Ried im Innkreis"
	},
	{
		"id": "9",
		"sectionId": "2.4",
		"from": "2018-06-22T10:00",
		"to": "2018-06-22T10:10",
		"title": "Közös genetikai hatások szerepe az érrendszer különböző területeinek atheroscleroticus megnyilvánulásában",
		"performer": "Hernyes Anita",
		"authors": "Hernyes Anita, Szalontai László, Forgó Bianka, Fejér Bence, Jermendy Ádám, Maurovich-Horvát Pál, Merkely Béla, Jermendy György, Hang A Park, Joohon Sung, Tárnoki Ádám Domonkos, Tárnoki Dávid László",
		"location": "Budapest"
	},
	{
		"id": "10",
		"sectionId": "2.4",
		"from": "2018-06-22T10:10",
		"to": "2018-06-22T10:20",
		"title": "Revolutionary new transducer technology and the future possibilities of medical ultrasound\n",
		"performer": "Takashino Hirano",
		"authors": "Hitachi Inc.",
		"location": "Hitachi Inc."
	},
	{
		"id": "1",
		"sectionId": "2.5",
		"from": "2018-06-22T10:50",
		"to": "2018-06-22T11:20",
		"title": "About ESR activities",
		"performer": "Derchi",
		"authors": "Derchi, Lorenzo",
		"location": "Genova"
	},
	{
		"id": "2",
		"sectionId": "2.5",
		"from": "2018-06-22T11:20",
		"to": "2018-06-22T12:10",
		"title": "The power of routine systematic peer feedback for continuous team learning",
		"performer": "Carlos Schorlemmer",
		"authors": "Carlos Schorlemmer",
		"location": "Barcelona"
	},
	{
		"id": "3",
		"sectionId": "2.5",
		"from": "2018-06-22T12:10",
		"to": "2018-06-22T12:30",
		"title": "TMC Academy: How to bring innovative medical education and clinical practice together",
		"performer": "Robledo",
		"authors": "Robledo, Ricard",
		"location": "Barcelona"
	},
	{
		"id": "1",
		"sectionId": "2.8",
		"from": "2018-06-22T13:45",
		"to": "2018-06-22T14:05",
		"title": "Novel Neuroimaging Biomarkers: Recognizing the Future Paths",
		"performer": "Milos A. Lucic",
		"authors": "Milos A. Lucic",
		"location": "Novi Sad, Serbia"
	},
	{
		"id": "2",
		"sectionId": "2.8",
		"from": "2018-06-22T14:05",
		"to": "2018-06-22T14:35",
		"title": "Radiomics, radiogenomics, radiotranscriptomics, radioproteomics....",
		"performer": "Patay Zoltán",
		"authors": "Patay Zoltán",
		"location": "Memphis, USA"
	},
	{
		"id": "3",
		"sectionId": "2.8",
		"from": "2018-06-22T14:35",
		"to": "2018-06-22T14:45",
		"title": "A hypophysis méretének örökletessége és kapcsolata a testsúly-testmagasság index-szel: egy ikervizsgálat eredményei",
		"performer": "Persely Aliz",
		"authors": "Persely Aliz, Szily Marcell, Dienes András, Fekete Márton, Kovács Dániel Tamás, Hernyes Anita, Karlinger Kinga, Bérczi Viktor, Kim Eunae, Sung Joohon, Hornyák Csilla, Szabó Ádám, Rudas Gábor, Tárnoki Dávid László, Tárnoki Ádám Domonkos",
		"location": "Budapest, Seoul"
	},
	{
		"id": "4",
		"sectionId": "2.8",
		"from": "2018-06-22T14:45",
		"to": "2018-06-22T14:55",
		"title": "Hypoxiás-ischaemiás agykárosodás MR megjelenési formái.",
		"performer": "Csomor Angéla",
		"authors": "Csomor Angéla, Kerekes Fanni, Vörös Erika",
		"location": "Szeged"
	},
	{
		"id": "5",
		"sectionId": "2.8",
		"from": "2018-06-22T14:55",
		"to": "2018-06-22T15:05",
		"title": "A nyaki gerinc degeneratív elváltozásainak örökletessége",
		"performer": "Bitai Zsuzsanna",
		"authors": "Bitai Zsuzsanna, Kovács Dániel Tamás, Eunjin Alicia Woo, Joohon Sung, Hornyák Csilla, Bérczi Viktor, Tárnoki Dávid László, Tárnoki Ádám Domonkos",
		"location": "Budapest, Seoul"
	},
	{
		"id": "6",
		"sectionId": "2.8",
		"from": "2018-06-22T15:05",
		"to": "2018-06-22T15:15",
		"title": "Subarachnoidalis vérzést követő vasospasmus paraszimpatikus ganglion stimulálással történő speciális lézerkezelése",
		"performer": "Oláh Csaba",
		"authors": "Oláh Csaba, Demeter Béla, Kosztopulosz Nikoletta, Rózsa Tamás",
		"location": "Miskolc"
	},
	{
		"id": "7",
		"sectionId": "2.8",
		"from": "2018-06-22T15:15",
		"to": "2018-06-22T15:25",
		"title": "Hyperdenz média jel artéria cerebri média elzáródás nélkül",
		"performer": "Oláh Csaba",
		"authors": "Oláh Csaba, Tamáska Péter, Sas Attila, Valikovics Attila, Lázár István",
		"location": "Miskolc"
	},
	{
		"id": "8",
		"sectionId": "2.8",
		"from": "2018-06-22T15:25",
		"to": "2018-06-22T15:35",
		"title": "Humán mikromozgás és zene: Kísérleti kutatás a mozgási műtermékek csökkentése érdekében Mágneses Rezonancia Képalkotás (MRI) során",
		"performer": "Földes Zsuzsa",
		"authors": "Földes Zsuzsa, Ala-Ruona Esa, Burger Birgitta, Orsi Gergely",
		"location": "Jyväskylä, Pécs"
	},
	{
		"id": "9",
		"sectionId": "2.8",
		"from": "2018-06-22T15:35",
		"to": "2018-06-22T15:45",
		"title": "Műtét előtti beszédaktivációs fMRI vizsgálatok validálása az intrinzik kapcsolati hálózatokról nyert ismeretek segítségével",
		"performer": "Kozák Lajos Rudolf",
		"authors": "L.R. Kozák, G. Gyebnár, A.G. Szabo, A.L. van Graan, P. Barsi, L. Lemieux, G. Rudas",
		"location": "Budapest, London"
	},
	{
		"id": "10",
		"sectionId": "2.8",
		"from": "2018-06-22T15:45",
		"to": "2018-06-22T15:55",
		"title": "A fehérállományi mikrovérzések szubakut szuszceptibilitás súlyozott (SWI) MRI-n átmenetileg láthatatlanná válhatnak patkányban",
		"performer": "Környei Bálint Soma",
		"authors": "Környei Bálint Soma, Tóth Arnold, Balogh Bendegúz, Berente Zoltán, Schwarcz Attila",
		"location": "Pécs"
	},
	{
		"id": "11",
		"sectionId": "2.8",
		"from": "2018-06-22T15:55",
		"to": "2018-06-22T16:05",
		"title": "Az obstruktív alvási apnoe és a lumbalis gerinc degeneratív eltéréseinek a kapcsolata.",
		"performer": "Szily Marcell",
		"authors": "Szily Marcell, Tárnoki Ádám Domonkos, Tárnoki Dávid László, Kovács Dániel Tamás, Forgó Bianka, Kim Eunae, Sung Joohon, De Barros Pinheiro Marina, Kostyál László, Bikov András, Oláh Csaba, Kunos László",
		"location": "Budapest, Seoul, Sydney, Miskolc"
	},
	{
		"id": "12",
		"sectionId": "2.8",
		"from": "2018-06-22T16:05",
		"to": "2018-06-22T16:15",
		"title": "Lumbális gerinc MR-en látott eltérések és a derékfájdalom közötti genetikai és környezeti kapcsolat vizsgálata ikrekben",
		"performer": "Dienes András",
		"authors": "Dienes András, Eunae Kim, Joohon Sung, Kovács Dániel T., Hornyák Csilla, Ferreira Paulo, De Barros Pinheiro Marina, Kostyál László, Oláh Csaba, Bérczi Viktor, Tárnoki Dávid L., Tárnoki Ádám D.",
		"location": "Budapest, Seoul, Sydney, Miskolc"
	},
	{
		"id": "13",
		"sectionId": "2.8",
		"from": "2018-06-22T16:15",
		"to": "2018-06-22T16:25",
		"title": "Ébredéses ischaemiás stroke komplex diagnosztikája és endovascularis ellátása. Esetbemutatás.",
		"performer": "Tamáska Péter",
		"authors": "Tamáska Péter, Kostyál László, Oláh Csaba, Lázár István",
		"location": "Miskolc"
	},
	{
		"id": "1",
		"sectionId": "2.9",
		"from": "2018-06-22T14:00",
		"to": "2018-06-22T14:20",
		"title": "Tomosynthesis and Contrast- Enhanced Mammography – our Experience at Institute of Oncology Ljubljana",
		"performer": "Music, Maja",
		"authors": "Music, Maja",
		"location": "Ljubljana, Slovenia"
	},
	{
		"id": "2",
		"sectionId": "2.9",
		"from": "2018-06-22T14:20",
		"to": "2018-06-22T14:40",
		"title": "Preliminary outcomes of pilot study of breast cancer SCREEning with MR-mammography in women with genetic mutation in Slovakia - SCRIMS project.",
		"performer": "Lehotska V.",
		"authors": "Lehotska V., Rauova K., Vanovcanova L.,",
		"location": "Bratislava, Slovakia"
	},
	{
		"id": "3",
		"sectionId": "2.9",
		"from": "2018-06-22T14:40",
		"to": "2018-06-22T15:00",
		"title": "Az Európai Emlődiagnosztikai Társaság (EUSOBI) oktató, tudományos és szakmapolitikai tevékenységei",
		"performer": "Forrai Gábor",
		"authors": "Forrai Gábor",
		"location": "Budapest"
	},
	{
		"id": "4",
		"sectionId": "2.9",
		"from": "2018-06-22T15:00",
		"to": "2018-06-22T15:10",
		"title": "Emlő-MR-vizsgálatok leletezése. Közös nyelven, egységesen",
		"performer": "Tasnádi Tünde",
		"authors": "Tasnádi Tünde, Forrai Gábor",
		"location": "Békéscsaba, Budapest"
	},
	{
		"id": "5",
		"sectionId": "2.9",
		"from": "2018-06-22T15:10",
		"to": "2018-06-22T15:20",
		"title": "Emlő MR leletsablonok a BI-RADS lexikon alapján Hatékonyan, egyszerűen",
		"performer": "Tasnádi Tünde",
		"authors": "Tasnádi Tünde, Forrai Gábor",
		"location": "Békéscsaba, Budapest"
	},
	{
		"id": "6",
		"sectionId": "2.9",
		"from": "2018-06-22T15:20",
		"to": "2018-06-22T15:30",
		"title": "A teleradiológia szerepe az emlődiagnosztikában",
		"performer": "Fülöp Rita",
		"authors": "Fülöp Rita, Barna Krisztina, Eriksson Johnny",
		"location": "Budapest, Örebro"
	},
	{
		"id": "7",
		"sectionId": "2.9",
		"from": "2018-06-22T15:30",
		"to": "2018-06-22T15:40",
		"title": "Az emlőváladékozás okai és kivizsgálása",
		"performer": "Besenyei Róbert",
		"authors": "Besenyei Róbert, Sebő Éva",
		"location": "Debrecen"
	},
	{
		"id": "8",
		"sectionId": "2.9",
		"from": "2018-06-22T15:40",
		"to": "2018-06-22T15:50",
		"title": "Tomoszintézis jelentősége a komplex emlődiagnosztikában",
		"performer": "Vámos Izabella",
		"authors": "Vámos Izabella, Sebő Éva",
		"location": "Debrecen"
	},
	{
		"id": "9",
		"sectionId": "2.9",
		"from": "2018-06-22T15:50",
		"to": "2018-06-22T16:00",
		"title": "Szentinel program, komplex emlődiagnosztika",
		"performer": "Szalai Gábor",
		"authors": "Szalai Gábor, Schmidt Erzsébet, Zámbó Katalin, Pavlovics Gábor, Kálmán Endre",
		"location": "Pécs"
	},
	{
		"id": "10",
		"sectionId": "2.9",
		"from": "2018-06-22T16:00",
		"to": "2018-06-22T16:10",
		"title": "Kezdeti tapasztalataink emlő tomosynthesissel",
		"performer": "Milics Margit",
		"authors": "Milics Margit",
		"location": "Zalaegerszeg"
	},
	{
		"id": "11",
		"sectionId": "2.9",
		"from": "2018-06-22T16:10",
		"to": "2018-06-22T16:20",
		"title": "Mammográfiás sugáregészségügyi minőségügyi kézikönyv",
		"performer": "Porubszky Tamás",
		"authors": "Porubszky Tamás, Elek Richárd, Battyány István, Váradi Csaba, Péntek Zoltán, Horváth Kitti",
		"location": "Budapest, Pécs, Szekszárd"
	},
	{
		"id": "12",
		"sectionId": "2.9",
		"from": "2018-06-22T16:20",
		"to": "2018-06-22T16:30",
		"title": "Kontrasztanyag halmozásos direct digitális mammográfia (CEDM)",
		"performer": "Nahm Krisztina",
		"authors": "Nahm Krisztina, Riedl Erika",
		"location": "Budapest, Vác"
	},
	{
		"id": "1",
		"sectionId": "2.10",
		"from": "2018-06-22T13:45",
		"to": "2018-06-22T14:15",
		"title": "Transzlációs Medicina: vissza a jövőbe",
		"performer": "Hegyi Péter",
		"authors": "Hegyi Péter",
		"location": "Pécs"
	},
	{
		"id": "2",
		"sectionId": "2.10",
		"from": "2018-06-22T14:15",
		"to": "2018-06-22T14:30",
		"title": "A pancreas cystosus daganatai",
		"performer": "Weninger Csaba",
		"authors": "Weninger Csaba",
		"location": "Arvika, Svédország"
	},
	{
		"id": "3",
		"sectionId": "2.10",
		"from": "2018-06-22T14:30",
		"to": "2018-06-22T14:40",
		"title": "Intraarteriális kalcium stimuláció szerepe a pancreas inzulinomák diagnosztikájában",
		"performer": "Tóth Judit",
		"authors": "Tóth Judit, Belán Ivett, Balogh Erika, Verebi Enikő, Nagy Endre",
		"location": "Debrecen"
	},
	{
		"id": "4",
		"sectionId": "2.10",
		"from": "2018-06-22T14:40",
		"to": "2018-06-22T15:00",
		"title": "Akut vékonybél",
		"performer": "Kárteszi Hedvig",
		"authors": "Kárteszi Hedvig",
		"location": "Bristol"
	},
	{
		"id": "5",
		"sectionId": "2.10",
		"from": "2018-06-22T15:00",
		"to": "2018-06-22T15:10",
		"title": "A vékonybélfalban kimutatható diffúziógátlási mintázatok összefüggése a Crohn betegség súlyosságával",
		"performer": "Faluhelyi Nándor",
		"authors": "Faluhelyi Nándor, Farkas Orsolya, Bogner Péter",
		"location": "Pécs"
	},
	{
		"id": "6",
		"sectionId": "2.10",
		"from": "2018-06-22T15:10",
		"to": "2018-06-22T15:20",
		"title": "A Crohn betegség MR diagnosztikája",
		"performer": "Kiss Ildikó",
		"authors": "Kiss Ildikó., Borbély Mónika., Lénárt Zsuzsanna, Palkó András",
		"location": "Szeged"
	},
	{
		"id": "7",
		"sectionId": "2.10",
		"from": "2018-06-22T15:20",
		"to": "2018-06-22T15:30",
		"title": "Ritka anomália gyakori tünetek mögött",
		"performer": "Csete Mónika",
		"authors": "Csete Mónika, Szukits Sándor",
		"location": "Komló, Pécs"
	},
	{
		"id": "8",
		"sectionId": "2.10",
		"from": "2018-06-22T15:30",
		"to": "2018-06-22T15:40",
		"title": "Aorto-duodenalis fistula sürgősségi képalkotása",
		"performer": "Bézi István",
		"authors": "Bézi István, Székely András, Pusztai Ferenc, Szücs István, Vinnai Gyula, Bágyi Péter",
		"location": "Debrecen"
	},
	{
		"id": "9",
		"sectionId": "2.10",
		"from": "2018-06-22T15:40",
		"to": "2018-06-22T15:50",
		"title": "Tompa hasi sérülés okozta hasfali sérvek jelentősége",
		"performer": "Gyömbér Edit",
		"authors": "Gyömbér Edit, Callum Stove, Paul Duffy",
		"location": "Glasgow"
	},
	{
		"id": "10",
		"sectionId": "2.10",
		"from": "2018-06-22T15:50",
		"to": "2018-06-22T16:00",
		"title": "Apophenia a hasban – a Pokélabda esete",
		"performer": "Bagi Alexandra",
		"authors": "Bagi Alexandra, Borbola György",
		"location": "Mosonmagyaróvár, Békéscsaba"
	},
	{
		"id": "11",
		"sectionId": "2.10",
		"from": "2018-06-22T16:00",
		"to": "2018-06-22T16:10",
		"title": "A rectum alsó harmadában fekvő malignus elváltozások MR diagnosztikai problémái",
		"performer": "Kalina Ildikó",
		"authors": "Kalina Ildikó, Turtóczki Kolos, Kaposi Pál, István Gábor, Zaránd Attila, Bérczi, Viktor",
		"location": "Budapest"
	},
	{
		"id": "12",
		"sectionId": "2.10",
		"from": "2018-06-22T16:10",
		"to": "2018-06-22T16:20",
		"title": "Új kezelési stratégia neoadjuváns terápia után komplett klinikai választ adó, rectum tumoros betegeknél",
		"performer": "Hoffer Krisztina",
		"authors": "Hoffer Krisztina, Rakos Gyula",
		"location": "Sopron"
	},
	{
		"id": "13",
		"sectionId": "2.10",
		"from": "2018-06-22T16:20",
		"to": "2018-06-22T16:30",
		"title": "Talált tárgyak radiológia osztálya - Avagy előkerült idegentestek 4 eset kapcsán",
		"performer": "Dankházi Levente",
		"authors": "Dankházi Levente, Szabó Albert, Bartek Péter",
		"location": "Győr"
	},
	{
		"id": "1",
		"sectionId": "3.1",
		"from": "2018-06-23T8:20",
		"to": "2018-06-23T8:35",
		"title": "Gyakorlati képalkotás akut ischaemiás stroke-ban: módszerek, lehetőségek és irányelvek",
		"performer": "Várallyay Péter",
		"authors": "Várallyay Péter",
		"location": "Budapest"
	},
	{
		"id": "2",
		"sectionId": "3.1",
		"from": "2018-06-23T8:35",
		"to": "2018-06-23T8:50",
		"title": "Fejlett képalkotási módszerek és alkalmazási területeik akut ishaemiás stroke-ban",
		"performer": "Rostás Tamás",
		"authors": "Rostás Tamás, Kövér Ferenc",
		"location": "Pécs"
	},
	{
		"id": "3",
		"sectionId": "3.1",
		"from": "2018-06-23T8:50",
		"to": "2018-06-23T9:05",
		"title": "Neurointervenciós rekanalizációs módszerek és eredményeik akut ischaemiás stroke-ban",
		"performer": "Szólics Alex",
		"authors": "Szólics Alex",
		"location": "Pécs"
	},
	{
		"id": "4",
		"sectionId": "3.1",
		"from": "2018-06-23T9:05",
		"to": "2018-06-23T9:20",
		"title": "Akut ischaemiás stroke intervenciós kezelésének hazai helyzete a nemzetközi tapasztalatok és irányelvek fényében",
		"performer": "Szikora István",
		"authors": "Szikora István, Kis Balázs, Vadász Ágnes, Gubucz István, Berentei Zsolt, Nardai Sándor",
		"location": "Budapest"
	},
	{
		"id": "5",
		"sectionId": "3.1",
		"from": "2018-06-23T9:20",
		"to": "2018-06-23T9:28",
		"title": "Akut ischaemiás stroke endovascularis kezelésének eredményei az Országos Klinikai Idegtudományi Intézetben",
		"performer": "Kis Balázs",
		"authors": "Kis Balázs, Vadász Ágnes, Nagy András, Nardai Sándor, Gubucz István, Berentei Zsolt, Szikora István",
		"location": "Budapest"
	},
	{
		"id": "6",
		"sectionId": "3.1",
		"from": "2018-06-23T9:28",
		"to": "2018-06-23T9:36",
		"title": "Mechanikus thrombectomiával szerzett kezdeti tapasztalataink",
		"performer": "Tamáska Péter",
		"authors": "Tamáska Péter, Ráski Gergely, Oláh Csaba, Pataki Ákos, Koncz Júlia, Lázár István",
		"location": "Miskolc"
	},
	{
		"id": "7",
		"sectionId": "3.1",
		"from": "2018-06-23T9:36",
		"to": "2018-06-23T9:42",
		"title": "Mechanikus thrombectomia helyzete Győrben",
		"performer": "Bartek Péter",
		"authors": "Bartek Péter, Szabó Albert, Garab Gergely",
		"location": "Győr"
	},
	{
		"id": "8",
		"sectionId": "3.1",
		"from": "2018-06-23T9:42",
		"to": "2018-06-23T9:50",
		"title": "Kezdeti tapasztalataink az endovascularis stroke ellátás során",
		"performer": "Nagy Csaba",
		"authors": "Nagy Csaba, Király István",
		"location": "Szombathely"
	},
	{
		"id": "9",
		"sectionId": "3.1",
		"from": "2018-06-23T9:50",
		"to": "2018-06-23T9:58",
		"title": "Akut ischaemiás stroke kezelés: thrombectomia eredmények Debrecen",
		"performer": "Tóth Judit",
		"authors": "Tóth Judit, Belán Ivett, Veisz Richárd, Fekete István",
		"location": "Debrecen"
	},
	{
		"id": "10",
		"sectionId": "3.1",
		"from": "2018-06-23T9:58",
		"to": "2018-06-23T10:06",
		"title": "Áttekintés a pécsi thrombectomia ellátás első három évéről",
		"performer": "Lenzsér Gábor",
		"authors": "Lenzsér Gábor",
		"location": "Pécs"
	},
	{
		"id": "11",
		"sectionId": "3.1",
		"from": "2018-06-23T10:06",
		"to": "2018-06-23T10:15",
		"title": "Az ischémiás stroke endovaszkuláris kezelésével szerzett tapasztalatok a Somogy Megyei Kaposi Mór Oktatókórházban.",
		"performer": "Vajda Zsolt",
		"authors": "Vajda Zsolt, Nagy Csaba, Lenzsér Gábor, Horváth Gyula, Bajzik Gábor, Radnai Péter, Repa Imre, Nagy Ferenc",
		"location": "Kaposvár"
	},
	{
		"id": "1",
		"sectionId": "3.2",
		"from": "2018-06-23T8:30",
		"to": "2018-06-23T8:40",
		"title": "A legújabb, TNM 8 osztályozás alapja, prognosztikát befolyásoló általános szempontjai",
		"performer": "Gődény Mária",
		"authors": "Gődény Mária",
		"location": "Budapest"
	},
	{
		"id": "2",
		"sectionId": "3.2",
		"from": "2018-06-23T8:40",
		"to": "2018-06-23T8:50",
		"title": "Változások a fej-nyaki daganatok stádium meghatározásában a TNM-8 alapján",
		"performer": "Léránt Gergely",
		"authors": "Léránt Gergely, Gődény Mária",
		"location": "Budapest)"
	},
	{
		"id": "3",
		"sectionId": "3.2",
		"from": "2018-06-23T8:50",
		"to": "2018-06-23T9:00",
		"title": "A gastrointestinalis rendszert érintő változások a UICC/AJCC 8. TNM rendszerében",
		"performer": "Jederán Éva",
		"authors": "Jederán Éva, Ujlaki Mátyás, Bahéry Mária",
		"location": "Budapest"
	},
	{
		"id": "4",
		"sectionId": "3.2",
		"from": "2018-06-23T9:00",
		"to": "2018-06-23T9:10",
		"title": "Változások a máj és epeúti daganatok stádium meghatározásában a TNM-8 alapján",
		"performer": "Csemez Imre",
		"authors": "Csemez Imre, Gődény Mária",
		"location": "Budapest"
	},
	{
		"id": "5",
		"sectionId": "3.2",
		"from": "2018-06-23T9:10",
		"to": "2018-06-23T9:20",
		"title": "Pancreas tumorok módosított TNM klasszifikációja",
		"performer": "Kárteszi Hedvig",
		"authors": "Kárteszi, Hedvig",
		"location": "Bristol"
	},
	{
		"id": "6",
		"sectionId": "3.2",
		"from": "2018-06-23T9:20",
		"to": "2018-06-23T9:30",
		"title": "Újdonságok a mellkasi tumorok TNM beosztásában",
		"performer": "Kerpel-Fronius Anna",
		"authors": "Kerpel-Fronius, Anna",
		"location": "Budapest"
	},
	{
		"id": "7",
		"sectionId": "3.2",
		"from": "2018-06-23T9:30",
		"to": "2018-06-23T9:40",
		"title": "Változások a csont- és lágyrészdaganatok stádium meghatározásában a TNM-8 alapján",
		"performer": "Manninger Sándor",
		"authors": "Manninger Sándor, Gődény Mária",
		"location": "Budapest"
	},
	{
		"id": "8",
		"sectionId": "3.2",
		"from": "2018-06-23T9:40",
		"to": "2018-06-23T9:50",
		"title": "Változások a melanoma malignum stádium meghatározásában a TNM-8 alapján.",
		"performer": "Bőcs Katalin",
		"authors": "Bőcs Katalin, Plótár Vanda, Liszkay Gabriella",
		"location": "Budapest"
	},
	{
		"id": "9",
		"sectionId": "3.2",
		"from": "2018-06-23T9:50",
		"to": "2018-06-23T10:00",
		"title": "Emlő tumorok TNM besorolása - UICC/AJCC 8. kiadása alapján",
		"performer": "Kovács Eszter",
		"authors": "Kovács Eszter, Bidlek Mária, Gődény Mária",
		"location": "Budapest"
	},
	{
		"id": "10",
		"sectionId": "3.2",
		"from": "2018-06-23T10:00",
		"to": "2018-06-23T10:10",
		"title": "Változások a nőgyógyászati daganatok stádium meghatározásában TNM-8 alapján",
		"performer": "Horváth Katalin",
		"authors": "Horváth Katalin",
		"location": "Budapest"
	},
	{
		"id": "11",
		"sectionId": "3.2",
		"from": "2018-06-23T10:10",
		"to": "2018-06-23T10:20",
		"title": "Változások a húgy-ivar szervek daganatainak stádium meghatározásában",
		"performer": "Oláhné Petri Klára",
		"authors": "Oláhné Petri Klára, Lóránd Ágnes, Bíró Krisztina",
		"location": "Budapest"
	},
	{
		"id": "1",
		"sectionId": "3.3",
		"from": "2018-06-23T8:30",
		"to": "2018-06-23T9:50",
		"title": "Magyar Radiológia workshop",
		"performer": "Harkányi Zoltán",
		"authors": "Harkányi Zoltán, Palkó András, Tárnoki Ádám, Béki János",
		"location": ""
	},
	{
		"id": "2",
		"sectionId": "3.3",
		"from": "2018-06-23T9:50",
		"to": "2018-06-23T10:30",
		"title": "A Zsebők című dokumentumfilm vetítése",
		"performer": "Harkányi Zoltán",
		"authors": "Bevezeti Harkányi Zoltán és Szekulesz Péter",
		"location": ""
	},
	{
		"id": "1",
		"sectionId": "3.4",
		"from": "2018-06-23T10:50",
		"to": "2018-06-23T11:00",
		"title": "Klinikai Audit, 2018 – az áttörés éve? (ESR Standard and Audits Committee)",
		"performer": "Vargha András",
		"authors": "Vargha András",
		"location": "Hidegség"
	},
	{
		"id": "2",
		"sectionId": "3.4",
		"from": "2018-06-23T11:00",
		"to": "2018-06-23T11:10",
		"title": "Hibaforrások és tévedési lehetőségek a leletezésben (ESR Standard and Audits Committee)",
		"performer": "Vargha András",
		"authors": "Vargha András",
		"location": "Hidegség"
	},
	{
		"id": "3",
		"sectionId": "3.4",
		"from": "2018-06-23T11:10",
		"to": "2018-06-23T11:33",
		"title": "Az emlődiagnosztika leggyakoribb szakmai műhibái, az emlőszűrés problémái, leletkiadási és beleegyezési hibák, leggyakoribb lelethibák.",
		"performer": "Forrai Gábor",
		"authors": "Forrai Gábor",
		"location": "Budapest"
	},
	{
		"id": "4",
		"sectionId": "3.4",
		"from": "2018-06-23T11:33",
		"to": "2018-06-23T11:50",
		"title": "Orvosi műhibák a jogász szemével",
		"performer": "Kovács György",
		"authors": "Kovács György",
		"location": "Budapest"
	},
	{
		"id": "5",
		"sectionId": "3.4",
		"from": "2018-06-23T11:50",
		"to": "2018-06-23T12:00",
		"title": "CT dózismonitorozás a mindennapi gyakorlatban",
		"performer": "Bágyi Péter",
		"authors": "Bágyi Péter, Balázs Ervin, Dankó Zsolt, Balkay László",
		"location": "Debrecen"
	},
	{
		"id": "6",
		"sectionId": "3.4",
		"from": "2018-06-23T12:00",
		"to": "2018-06-23T12:10",
		"title": "Mit keres a fizikus a röntgenben?",
		"performer": "Porubszky Tamás",
		"authors": "Porubszky Tamás",
		"location": "Budapest"
	},
	{
		"id": "7",
		"sectionId": "3.4",
		"from": "2018-06-23T12:10",
		"to": "2018-06-23T12:30",
		"title": "HCCM Injection Protocol for Radiation Dose Reduction in CTA and Chest",
		"performer": "Prof. Martin Krix",
		"authors": "Ewopharma - Bracco symposium: Prof. Martin Krix, Global Medical & Regulatory Affairs, Bracco Imaging Deutschland",
		"location": "Konstanz, Germany"
	},
	{
		"id": "8",
		"sectionId": "3.4",
		"from": "2018-06-23T12:30",
		"to": "2018-06-23T12:40",
		"title": "Egy év percről percre – az idő relatív",
		"performer": "Kostyál László",
		"authors": "Kostyál László",
		"location": "Miskolc"
	},
	{
		"id": "9",
		"sectionId": "3.4",
		"from": "2018-06-23T12:40",
		"to": "2018-06-23T12:50",
		"title": "Minőségbiztosítás a szakorvosképzésben",
		"performer": "Lánczi Levente István",
		"authors": "Lánczi Levente István, Pusztai Ferenc, Bágyi Péter",
		"location": "Debrecen"
	},
	{
		"id": "10",
		"sectionId": "3.4",
		"from": "2018-06-23T12:50",
		"to": "2018-06-23T13:00",
		"title": "Kettős leletezés, peer-review a mindennapi gyakorlatban",
		"performer": "Bágyi Péter",
		"authors": "Bágyi Péter, Pusztai Ferenc, Lánczi Levente István",
		"location": "Debrecen"
	},
	{
		"id": "1",
		"sectionId": "3.5",
		"from": "2018-06-23T10:50",
		"to": "2018-06-23T11:00",
		"title": "A szív CT és/vagy MR vizsgálata. Mikor melyik? Milyen sorrendben?",
		"performer": "Tóth Levente",
		"authors": "Tóth Levente",
		"location": "Pécs"
	},
	{
		"id": "2",
		"sectionId": "3.5",
		"from": "2018-06-23T11:00",
		"to": "2018-06-23T11:10",
		"title": "Szívritmus szabályozó készülékek a mellkas röntgenen - Amit radiológusként érdemes tudnunk",
		"performer": "Panajotu Alexisz",
		"authors": "Panajotu Alexisz",
		"location": "Budapest"
	},
	{
		"id": "3",
		"sectionId": "3.5",
		"from": "2018-06-23T11:10",
		"to": "2018-06-23T11:20",
		"title": "Myocarditis és/vagy ARVC: kontroll szív MR vizsgálat ICD beültetés után",
		"performer": "Tóth Attila",
		"authors": "Tóth Attila, Clemens Marcell, Suhai Ferenc Imre, Czimbalmos Csilla, Csécs Ibolya, Dohy Zsófia, Vágó Hajnalka, Simor Tamás, Édes István, Hüttl Kálmán, Merkely Béla",
		"location": "Budapest, Debrecen, Pécs"
	},
	{
		"id": "4",
		"sectionId": "3.5",
		"from": "2018-06-23T11:20",
		"to": "2018-06-23T11:30",
		"title": "A SZÍV 18F-FDG-PET/CT VIZSGÁLATA KÖTŐSZÖVETI BETEGSÉGBEN",
		"performer": "Besenyi Zsuzsanna",
		"authors": "Besenyi Zsuzsanna, Ágoston Gergely, Bakos Annamária, Hemelein Rita, Varga Albert, Kovács László, Pávics László",
		"location": "Szeged"
	},
	{
		"id": "5",
		"sectionId": "3.5",
		"from": "2018-06-23T11:30",
		"to": "2018-06-23T11:40",
		"title": "Pulmonális hipertenzió",
		"performer": "Várady Edit",
		"authors": "Várady Edit",
		"location": "Pécs"
	},
	{
		"id": "6",
		"sectionId": "3.5",
		"from": "2018-06-23T11:40",
		"to": "2018-06-23T11:50",
		"title": "Szív mágneses rezonanciás vizsgálat diagnosztikus szerepe malignus kamrai ritmuszavarok esetén",
		"performer": "Suhai Ferenc Imre",
		"authors": "Suhai Ferenc Imre, Szabó Liliána, Czimbalmos, Csilla, Csécs Ibolya, Tóth Attila, Becker Dávid2, Zima Endre, Gellér László, Hüttl Kálmán, Balázs György, Merkely Béla, Vágó Hajnalka",
		"location": "Budapest"
	},
	{
		"id": "7",
		"sectionId": "3.5",
		"from": "2018-06-23T11:50",
		"to": "2018-06-23T12:00",
		"title": "Mellékleletek TAVI CT angiográfiás vizsgálatok során",
		"performer": "Szukits Sándor",
		"authors": "Szukits Sándor, Tóth Levente, Várady Edit",
		"location": "Pécs"
	},
	{
		"id": "5",
		"sectionId": "3.5",
		"from": "2018-06-23T12:00",
		"to": "2018-06-23T12:10",
		"title": "Mycardiális bridge jelentőségének vizsgálata koszorúér áthidaló műtétek esetében coronaria CT angiográfián alapuló 3D tervezés tükrében",
		"performer": "Wlasitsch-Nagy Zsófia",
		"authors": "Wlasitsch-Nagy Zsófia, Szukits Sándor, Lénárd László, Bogner Péter, Varga Péter, Gasz Balázs, Várady Edit",
		"location": "Pécs"
	},
	{
		"id": "6",
		"sectionId": "3.5",
		"from": "2018-06-23T12:10",
		"to": "2018-06-23T12:20",
		"title": "Modified Coronary Calcium Scoring: Role in Coronary Artery Disease Risk Management of Patients Enrolled in a Lung Cancer Screening Program by Low-Dose CT",
		"performer": "Virágh Károly",
		"authors": "Virágh Károly, Lo Jocelyn, Sica Gregory, Gáspár Emese",
		"location": "New York, Los Angeles"
	},
	{
		"id": "7",
		"sectionId": "3.5",
		"from": "2018-06-23T12:20",
		"to": "2018-06-23T12:35",
		"title": "Új generációs szív CT, első tapasztalatok",
		"performer": "Maurovich-Horvat Pál",
		"authors": "Maurovich-Horvat Pál",
		"location": "Budapest"
	},
	{
		"id": "1",
		"sectionId": "3.6",
		"from": "2018-06-23T10:50",
		"to": "2018-06-23T11:10",
		"title": "A kortalan (határtalan) gyermekradiológia",
		"performer": "Lombay Béla",
		"authors": "Lombay Béla",
		"location": "Miskolc"
	},
	{
		"id": "2",
		"sectionId": "3.6",
		"from": "2018-06-23T11:10",
		"to": "2018-06-23T11:20",
		"title": "Gyermekkori csípőtáji csont-izületi gyulladás",
		"performer": "Polovitzer Mária",
		"authors": "Polovitzer Mária, Bartók Márta, Ráskai Csaba",
		"location": "Budapest"
	},
	{
		"id": "3",
		"sectionId": "3.6",
		"from": "2018-06-23T11:20",
		"to": "2018-06-23T11:30",
		"title": "A gyermekkori traumás hasi sérülések CEUS vizsgálata",
		"performer": "Koller Orsolya",
		"authors": "Koller Orsolya, Harkányi Zoltán, Hegyi Gábor",
		"location": "Budapest"
	},
	{
		"id": "4",
		"sectionId": "3.6",
		"from": "2018-06-23T11:30",
		"to": "2018-06-23T11:40",
		"title": "Az urachus záródási rendellenességeinek áttekintése",
		"performer": "Kiss Tünde",
		"authors": "Kiss Tünde, Bartók Márta, Weber Gabriella",
		"location": "Budapest"
	},
	{
		"id": "5",
		"sectionId": "3.6",
		"from": "2018-06-23T11:40",
		"to": "2018-06-23T11:50",
		"title": "Congenitalis hyperinzulinaemiás hypoglycaemia – gyermekkori focalis elváltozás a pancreasban",
		"performer": "Várkonyi Ildikó",
		"authors": "Várkonyi Ildikó, Balogh Lídia, Luczay Andrea, Harsányi László, Borka Katalin, Kálmán Attila",
		"location": "Budapest"
	},
	{
		"id": "6",
		"sectionId": "3.6",
		"from": "2018-06-23T11:50",
		"to": "2018-06-23T12:00",
		"title": "Vesesérülés ritka esete",
		"performer": "Héjj Ildikó Mária",
		"authors": "Héjj Ildikó Mária, Morvai Zsuzsanna, Harkányi Zoltán",
		"location": "Budapest"
	},
	{
		"id": "7",
		"sectionId": "3.6",
		"from": "2018-06-23T12:00",
		"to": "2018-06-23T12:10",
		"title": "Koponya UH vizsgálatok",
		"performer": "Mohay Gabriella",
		"authors": "Mohay Gabriella",
		"location": "Pécs"
	},
	{
		"id": "8",
		"sectionId": "3.6",
		"from": "2018-06-23T12:10",
		"to": "2018-06-23T12:20",
		"title": "A hypoxiás károsodás és az intracranialis vérzések hatása a fejlődésneurológiai kimenetelre: a korai MR prognosztikai szerepe neonatalis hypoxiás ischaemiás encephalopathiában",
		"performer": "Lakatos Andrea",
		"authors": "Lakatos Andrea, Kolossváry Márton, Szabó Miklós, Jermendy Ágnes, Barta Hajnalka, Gyebnár Gyula, Rudas Gábor, Kozák Lajos Rudolf",
		"location": "Budapest"
	},
	{
		"id": "9",
		"sectionId": "3.6",
		"from": "2018-06-23T12:20",
		"to": "2018-06-23T12:30",
		"title": "Többszervi elváltozást okozó ritka gastroenteritis",
		"performer": "Juhász Emese",
		"authors": "Juhász Emese, Falus Ágnes, Molnár Diana, Polovitzer Mária, Lőrincz Margit",
		"location": "Budapest"
	},
	{
		"id": "10",
		"sectionId": "3.6",
		"from": "2018-06-23T12:30",
		"to": "2018-06-23T12:40",
		"title": "Ritka mellkasi térfoglaló folyamat",
		"performer": "Szabó Ágota",
		"authors": "Szabó Ágota, Molnár Diana, Balázs György, Kálmán Attila",
		"location": "Budapest"
	},
	{
		"id": "11",
		"sectionId": "3.6",
		"from": "2018-06-23T12:40",
		"to": "2018-06-23T12:50",
		"title": "Choledochus cysta ritka szövődménye – esetbemutatás",
		"performer": "Nyitrai Anna",
		"authors": "Nyitrai Anna, Várkonyi Ildikó, Seszták Tímea, Kis Imre",
		"location": "Budapest"
	},
	{
		"id": "12",
		"sectionId": "3.6",
		"from": "2018-06-23T12:50",
		"to": "2018-06-23T13:00",
		"title": "Arteria femoralis profunda posttraumás pseuroaneurysmája",
		"performer": "Molnár Diána",
		"authors": "Molnár Diána, Balázs György, Csobay-Novák Csaba",
		"location": "Budapest"
	},
	{
		"id": "1",
		"sectionId": "3.7",
		"from": "2018-06-23T10:50",
		"to": "2018-06-23T12:00",
		"title": "Kvíz",
		"performer": "",
		"authors": "Olajos Eszter és az esetgazdák",
		"location": ""
	}
];

data.forEach(v=>{

	if(v.from.length != 16){
		v.from = [v.from.slice(0, 11), '0', v.from.slice(11)].join('')
	}
	if(v.to.length != 16){
		v.to = [v.to.slice(0, 11), '0', v.to.slice(11)].join('')
	}

	v.from = new Date(v.from);
	v.to = new Date(v.to);
	v.fullId = v.sectionId+'.'+v.id;
});

data.sort(
	(a,b)=>{
		if(a.from < b.from) return -1;
		if(a.from > b.from) return 1;
		return 0
	}
);


export default data;