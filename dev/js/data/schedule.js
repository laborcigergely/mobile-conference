let data = [
	{
		"type": "section",
		"from": "2018-06-21T10:30",
		"to": "2018-06-21T12:30",
		"title": "Hibrid és multimodális képalkotás",
		"room": "1",
		"id": "1.1"
	},
	{
		"type": "section",
		"from": "2018-06-21T11:00",
		"to": "2018-06-21T12:35",
		"title": "Intervenciós radiológia",
		"room": "2",
		"id": "1.2"
	},
	{
		"type": "section",
		"from": "2018-06-21T10:30",
		"to": "2018-06-21T12:35",
		"title": "Ifjúsági Bizottság Workshop",
		"room": "3",
		"id": "1.3"
	},
	{
		"type": "section",
		"from": "2018-06-21T14:00",
		"to": "2018-06-21T16:00",
		"title": "Boehringer-Ingelheim szimpózium és mellkasi radiológia",
		"room": "1",
		"id": "1.4"
	},
	{
		"type": "section",
		"from": "2018-06-21T14:00",
		"to": "2018-06-21T16:00 ",
		"title": "Radiológiai IT",
		"room": "2",
		"id": "1.5"
	},
	{
		"type": "section",
		"from": "2018-06-21T14:00",
		"to": "2018-06-21T15:50",
		"title": "Pelvis",
		"room": "3",
		"id": "1.6"
	},
	{
		"type": "section",
		"from": "2018-06-22T08:20",
		"to": "2018-06-22T16:30",
		"title": "Mesterséges intelligencia és radiológia",
		"room": "1",
		"id": "2.1"
	},
	{
		"type": "section",
		"from": "2018-06-22T08:30",
		"to": "2018-06-22T10:20",
		"title": "Gócos májelváltozások korszerű diagnosztikája és kezelése",
		"room": "2",
		"id": "2.2"
	},
	{
		"type": "section",
		"from": "2018-06-22T08:30",
		"to": "2018-06-22T10:20",
		"title": "Musculoskeletalis radiológia",
		"room": "3",
		"id": "2.3"
	},
	{
		"type": "section",
		"from": "2018-06-22T08:30",
		"to": "2018-06-22T10:20",
		"title": "Vasculáris ultrahang",
		"room": "4",
		"id": "2.4"
	},
	{
		"type": "section",
		"from": "2018-06-22T10:50",
		"to": "2018-06-22T12:30",
		"title": "Plenáris - Radiológia határok nélkül",
		"room": "1",
		"id": "2.5"
	},
	{
		"type": "section",
		"from": "2018-06-22T10:50",
		"to": "2018-06-22T12:30",
		"title": "Plenáris",
		"room": "2",
		"id": "2.6"
	},
	{
		"type": "section",
		"from": "2018-06-22T10:50",
		"to": "2018-06-22T12:30",
		"title": "Plenáris",
		"room": "3",
		"id": "2.7"
	},
	{
		"type": "section",
		"from": "2018-06-22T13:45",
		"to": "2018-06-22T16:25",
		"title": "Neuroradiológia",
		"room": "1",
		"id": "2.8"
	},
	{
		"type": "section",
		"from": "2018-06-22T14:00",
		"to": "2018-06-22T16:30",
		"title": "Mammográfia",
		"room": "2",
		"id": "2.9"
	},
	{
		"type": "section",
		"from": "2018-06-22T08:20",
		"to": "2018-06-22T16:30",
		"title": "Pancreas, gasztrointesztinális rendszer",
		"room": "3",
		"id": "2.10"
	},
	{
		"type": "section",
		"from": "2018-06-23T08:20",
		"to": "2018-06-23T10:06",
		"title": "Stroke",
		"room": "1",
		"id": "3.1"
	},
	{
		"type": "section",
		"from": "2018-06-23T08:30",
		"to": "2018-06-23T10:20",
		"title": "Onkológia",
		"room": "2",
		"id": "3.2"
	},
	{
		"type": "section",
		"from": "2018-06-23T08:30",
		"to": "2018-06-23T10:30",
		"title": "Workshop / Vetítés",
		"room": "3",
		"id": "3.3"
	},
	{
		"type": "section",
		"from": "2018-06-23T10:50",
		"to": "2018-06-23T13:00",
		"title": "Management",
		"room": "1",
		"id": "3.4"
	},
	{
		"type": "section",
		"from": "2018-06-23T10:50",
		"to": "2018-06-23T12:35",
		"title": "Kardiológiai képalkotás",
		"room": "2",
		"id": "3.5"
	},
	{
		"type": "section",
		"from": "2018-06-23T10:50",
		"to": "2018-06-23T13:00",
		"title": "Gyermekradiológia",
		"room": "3",
		"id": "3.6"
	},
	{
		"type": "section",
		"from": "2018-06-23T10:50",
		"to": "2018-06-23T12:00",
		"title": "Kvíz",
		"room": "4",
		"id": "3.7"
	},
	{
		"type": "event",
		"from": "2018-06-21T12:45",
		"to": "2018-06-21T14:00",
		"title": "Ebédszünet"
	},
	{
		"type": "event",
		"from": "2018-06-21T16:15",
		"to": "2018-06-21T18:00",
		"title": "Megnyitó"
	},
	{
		"type": "event",
		"from": "2018-06-21T20:00",
		"to": "2018-06-21T23:00",
		"title": "Koncert és állófogadás"
	},
	{
		"type": "event",
		"from": "2018-06-22T10:30",
		"to": "2018-06-22T10:50",
		"title": "Szünet"
	},
	{
		"type": "event",
		"from": "2018-06-22T12:30",
		"to": "2018-06-22T13:45",
		"title": "Ebédszünet"
	},
	{
		"type": "event",
		"from": "2018-06-22T17:00",
		"to": "2018-06-22T19:00",
		"title": "Közgyűlés"
	},
	{
		"type": "event",
		"from": "2018-06-22T20:00",
		"to": "2018-06-22T24:00",
		"title": "Gálavacsora"
	},
	{
		"type": "event",
		"from": "2018-06-23T10:30",
		"to": "2018-06-23T10:50",
		"title": "Szünet"
	}
];

data.forEach(v => {
	v.from = new Date(v.from);
	v.to = new Date(v.to);
});


data.sort(
	(a,b)=>{
		if(a.from < b.from) return -1;
		if(a.from > b.from) return 1;
		return 0
	}
);

export default data;

