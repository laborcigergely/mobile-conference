let data = [
	{
		"name": "Pávics László",
		"presentations": [
			{
				"id": "1",
				"sectionId": "1.1"
			}
		]
	},
	{
		"name": "Besenyi Zsuzsanna",
		"presentations": [
			{
				"id": "2",
				"sectionId": "1.1"
			},
			{
				"id": "4",
				"sectionId": "3.5"
			}
		]
	},
	{
		"name": "Séllei Ágnes",
		"presentations": [
			{
				"id": "3",
				"sectionId": "1.1"
			}
		]
	},
	{
		"name": "Győri Gabriella",
		"presentations": [
			{
				"id": "4",
				"sectionId": "1.1"
			}
		]
	},
	{
		"name": "Urbán Szabolcs",
		"presentations": [
			{
				"id": "5",
				"sectionId": "1.1"
			}
		]
	},
	{
		"name": "Bakos Annamária",
		"presentations": [
			{
				"id": "6",
				"sectionId": "1.1"
			}
		]
	},
	{
		"name": "Farkas István",
		"presentations": [
			{
				"id": "7",
				"sectionId": "1.1"
			},
			{
				"id": "6",
				"sectionId": "1.6"
			}
		]
	},
	{
		"name": "Sipka Gábor",
		"presentations": [
			{
				"id": "8",
				"sectionId": "1.1"
			}
		]
	},
	{
		"name": "Kári Béla",
		"presentations": [
			{
				"id": "9",
				"sectionId": "1.1"
			}
		]
	},
	{
		"name": "Sári Áron",
		"presentations": [
			{
				"id": "10",
				"sectionId": "1.1"
			}
		]
	},
	{
		"name": "Paul Duffy",
		"presentations": [
			{
				"id": "11",
				"sectionId": "1.1"
			}
		]
	},
	{
		"name": "Horváth Gábor",
		"presentations": [
			{
				"id": "1",
				"sectionId": "1.2"
			}
		]
	},
	{
		"name": "Vértes Miklós",
		"presentations": [
			{
				"id": "2",
				"sectionId": "1.2"
			}
		]
	},
	{
		"name": "Kis Balázs",
		"presentations": [
			{
				"id": "3",
				"sectionId": "1.2"
			},
			{
				"id": "5",
				"sectionId": "3.1"
			}
		]
	},
	{
		"name": "Bérczi Ákos",
		"presentations": [
			{
				"id": "4",
				"sectionId": "1.2"
			}
		]
	},
	{
		"name": "Oláh Csaba",
		"presentations": [
			{
				"id": "5",
				"sectionId": "1.2"
			},
			{
				"id": "6",
				"sectionId": "2.8"
			},
			{
				"id": "7",
				"sectionId": "2.8"
			}
		]
	},
	{
		"name": "Hernyes Anita",
		"presentations": [
			{
				"id": "6",
				"sectionId": "1.2"
			},
			{
				"id": "9",
				"sectionId": "2.4"
			}
		]
	},
	{
		"name": "Jermendy Ádám",
		"presentations": [
			{
				"id": "7",
				"sectionId": "1.2"
			}
		]
	},
	{
		"name": "Csizmadia Sándor",
		"presentations": [
			{
				"id": "8",
				"sectionId": "1.2"
			}
		]
	},
	{
		"name": "Csobay-Novák Csaba",
		"presentations": [
			{
				"id": "9",
				"sectionId": "1.2"
			}
		]
	},
	{
		"name": "",
		"presentations": [
			{
				"id": "1",
				"sectionId": "1.3"
			},
			{
				"id": "2",
				"sectionId": "1.3"
			},
			{
				"id": "7",
				"sectionId": "1.3"
			},
			{
				"id": "1",
				"sectionId": "3.7"
			}
		]
	},
	{
		"name": "Tárnoki Ádám",
		"presentations": [
			{
				"id": "3",
				"sectionId": "1.3"
			},
			{
				"id": "5",
				"sectionId": "2.4"
			}
		]
	},
	{
		"name": "Karlinger Kinga",
		"presentations": [
			{
				"id": "4",
				"sectionId": "1.3"
			}
		]
	},
	{
		"name": "Tárnoki Dávid",
		"presentations": [
			{
				"id": "5",
				"sectionId": "1.3"
			},
			{
				"id": "7",
				"sectionId": "2.3"
			}
		]
	},
	{
		"name": "Lánczi Levente",
		"presentations": [
			{
				"id": "6",
				"sectionId": "1.3"
			}
		]
	},
	{
		"name": "Földes Tamás",
		"presentations": [
			{
				"id": "8",
				"sectionId": "1.3"
			}
		]
	},
	{
		"name": "Tárnoki Dávid László",
		"presentations": [
			{
				"id": "1",
				"sectionId": "1.4"
			}
		]
	},
	{
		"name": "Monostori Zsuzsanna",
		"presentations": [
			{
				"id": "2",
				"sectionId": "1.4"
			}
		]
	},
	{
		"name": "Kerpel-Fronius Anna",
		"presentations": [
			{
				"id": "3",
				"sectionId": "1.4"
			},
			{
				"id": "6",
				"sectionId": "3.2"
			}
		]
	},
	{
		"name": "Balázs György",
		"presentations": [
			{
				"id": "4",
				"sectionId": "1.4"
			}
		]
	},
	{
		"name": "Székely András",
		"presentations": [
			{
				"id": "5",
				"sectionId": "1.4"
			}
		]
	},
	{
		"name": "Tárnoki Ádám Domonkos",
		"presentations": [
			{
				"id": "6",
				"sectionId": "1.4"
			}
		]
	},
	{
		"name": "Futácsi Balázs",
		"presentations": [
			{
				"id": "7",
				"sectionId": "1.4"
			}
		]
	},
	{
		"name": "Nagy Edit Boglárka",
		"presentations": [
			{
				"id": "8",
				"sectionId": "1.4"
			}
		]
	},
	{
		"name": "Kovács Anita",
		"presentations": [
			{
				"id": "9",
				"sectionId": "1.4"
			}
		]
	},
	{
		"name": "Orbán Vince",
		"presentations": [
			{
				"id": "10",
				"sectionId": "1.4"
			}
		]
	},
	{
		"name": "Zsiros László Róbert",
		"presentations": [
			{
				"id": "1",
				"sectionId": "1.5"
			},
			{
				"id": "4",
				"sectionId": "1.5"
			}
		]
	},
	{
		"name": "Damsa Andrei",
		"presentations": [
			{
				"id": "2",
				"sectionId": "1.5"
			}
		]
	},
	{
		"name": "Botz Bálint",
		"presentations": [
			{
				"id": "3",
				"sectionId": "1.5"
			}
		]
	},
	{
		"name": "Olajos Eszter Ajna",
		"presentations": [
			{
				"id": "5",
				"sectionId": "1.5"
			}
		]
	},
	{
		"name": "Varga Péter",
		"presentations": [
			{
				"id": "6",
				"sectionId": "1.5"
			}
		]
	},
	{
		"name": "Klimaj Zoltán",
		"presentations": [
			{
				"id": "7",
				"sectionId": "1.5"
			}
		]
	},
	{
		"name": "Derchi",
		"presentations": [
			{
				"id": "1",
				"sectionId": "1.6"
			},
			{
				"id": "1",
				"sectionId": "2.5"
			}
		]
	},
	{
		"name": "Koppán Miklós",
		"presentations": [
			{
				"id": "2",
				"sectionId": "1.6"
			}
		]
	},
	{
		"name": "Nagy Gyöngyi",
		"presentations": [
			{
				"id": "3",
				"sectionId": "1.6"
			}
		]
	},
	{
		"name": "Bérczi Viktor",
		"presentations": [
			{
				"id": "4",
				"sectionId": "1.6"
			}
		]
	},
	{
		"name": "Karczagi Lilla",
		"presentations": [
			{
				"id": "5",
				"sectionId": "1.6"
			}
		]
	},
	{
		"name": "Kaposi Novák Pál",
		"presentations": [
			{
				"id": "7",
				"sectionId": "1.6"
			},
			{
				"id": "5",
				"sectionId": "2.2"
			}
		]
	},
	{
		"name": "Kalina Ildikó",
		"presentations": [
			{
				"id": "8",
				"sectionId": "1.6"
			},
			{
				"id": "11",
				"sectionId": "2.10"
			}
		]
	},
	{
		"name": "Dombi Gergely",
		"presentations": [
			{
				"id": "1",
				"sectionId": "2.1"
			}
		]
	},
	{
		"name": "Joris Wakkie",
		"presentations": [
			{
				"id": "2",
				"sectionId": "2.1"
			}
		]
	},
	{
		"name": "Kecskeméthy Péter",
		"presentations": [
			{
				"id": "3",
				"sectionId": "2.1"
			}
		]
	},
	{
		"name": "Gál Andor Viktor",
		"presentations": [
			{
				"id": "4",
				"sectionId": "2.1"
			}
		]
	},
	{
		"name": "Samsung",
		"presentations": [
			{
				"id": "5",
				"sectionId": "2.1"
			}
		]
	},
	{
		"name": "Lombay Béla",
		"presentations": [
			{
				"id": "6",
				"sectionId": "2.1"
			},
			{
				"id": "1",
				"sectionId": "3.6"
			}
		]
	},
	{
		"name": "Palkó András",
		"presentations": [
			{
				"id": "1",
				"sectionId": "2.2"
			}
		]
	},
	{
		"name": "Doros Attila",
		"presentations": [
			{
				"id": "2",
				"sectionId": "2.2"
			}
		]
	},
	{
		"name": "Papp András",
		"presentations": [
			{
				"id": "3",
				"sectionId": "2.2"
			}
		]
	},
	{
		"name": "Demjén Boglárka",
		"presentations": [
			{
				"id": "4",
				"sectionId": "2.2"
			}
		]
	},
	{
		"name": "Bánsághi Zoltán",
		"presentations": [
			{
				"id": "6",
				"sectionId": "2.2"
			}
		]
	},
	{
		"name": "Kónya Júlia Anna",
		"presentations": [
			{
				"id": "7",
				"sectionId": "2.2"
			}
		]
	},
	{
		"name": "Nőt László Gergely",
		"presentations": [
			{
				"id": "1",
				"sectionId": "2.3"
			}
		]
	},
	{
		"name": "Farbaky Zsófia",
		"presentations": [
			{
				"id": "2",
				"sectionId": "2.3"
			}
		]
	},
	{
		"name": "Hetényi Szabolcs",
		"presentations": [
			{
				"id": "3",
				"sectionId": "2.3"
			}
		]
	},
	{
		"name": "Soltész Judit",
		"presentations": [
			{
				"id": "4",
				"sectionId": "2.3"
			}
		]
	},
	{
		"name": "Tasnádi Tünde",
		"presentations": [
			{
				"id": "5",
				"sectionId": "2.3"
			},
			{
				"id": "4",
				"sectionId": "2.9"
			},
			{
				"id": "5",
				"sectionId": "2.9"
			}
		]
	},
	{
		"name": "Brzózka Ádám",
		"presentations": [
			{
				"id": "6",
				"sectionId": "2.3"
			}
		]
	},
	{
		"name": "Bánáti Laura",
		"presentations": [
			{
				"id": "8",
				"sectionId": "2.3"
			}
		]
	},
	{
		"name": "Brkljacic, Boris",
		"presentations": [
			{
				"id": "1",
				"sectionId": "2.4"
			}
		]
	},
	{
		"name": "Szalontai László",
		"presentations": [
			{
				"id": "2",
				"sectionId": "2.4"
			}
		]
	},
	{
		"name": "Janositz Gréta",
		"presentations": [
			{
				"id": "3",
				"sectionId": "2.4"
			}
		]
	},
	{
		"name": "Sayed-Ahmad Mustafa",
		"presentations": [
			{
				"id": "4",
				"sectionId": "2.4"
			}
		]
	},
	{
		"name": "Szabó Helga",
		"presentations": [
			{
				"id": "6",
				"sectionId": "2.4"
			}
		]
	},
	{
		"name": "Gyánó Marcell",
		"presentations": [
			{
				"id": "7",
				"sectionId": "2.4"
			}
		]
	},
	{
		"name": "Fekete Márton",
		"presentations": [
			{
				"id": "8",
				"sectionId": "2.4"
			}
		]
	},
	{
		"name": "Hitachi prezentáció",
		"presentations": [
			{
				"id": "10",
				"sectionId": "2.4"
			}
		]
	},
	{
		"name": "Carlos Schorlemmer",
		"presentations": [
			{
				"id": "2",
				"sectionId": "2.5"
			}
		]
	},
	{
		"name": "Robledo",
		"presentations": [
			{
				"id": "3",
				"sectionId": "2.5"
			}
		]
	},
	{
		"name": "Milos A. Lucic",
		"presentations": [
			{
				"id": "1",
				"sectionId": "2.8"
			}
		]
	},
	{
		"name": "Patay Zoltán",
		"presentations": [
			{
				"id": "2",
				"sectionId": "2.8"
			}
		]
	},
	{
		"name": "Persely Aliz",
		"presentations": [
			{
				"id": "3",
				"sectionId": "2.8"
			}
		]
	},
	{
		"name": "Csomor Angéla",
		"presentations": [
			{
				"id": "4",
				"sectionId": "2.8"
			}
		]
	},
	{
		"name": "Bitai Zsuzsanna",
		"presentations": [
			{
				"id": "5",
				"sectionId": "2.8"
			}
		]
	},
	{
		"name": "Földes Zsuzsa",
		"presentations": [
			{
				"id": "8",
				"sectionId": "2.8"
			}
		]
	},
	{
		"name": "Kozák Lajos Rudolf",
		"presentations": [
			{
				"id": "9",
				"sectionId": "2.8"
			}
		]
	},
	{
		"name": "Környei Bálint Soma",
		"presentations": [
			{
				"id": "10",
				"sectionId": "2.8"
			}
		]
	},
	{
		"name": "Szily Marcell",
		"presentations": [
			{
				"id": "11",
				"sectionId": "2.8"
			}
		]
	},
	{
		"name": "Dienes András",
		"presentations": [
			{
				"id": "12",
				"sectionId": "2.8"
			}
		]
	},
	{
		"name": "Tamáska Péter",
		"presentations": [
			{
				"id": "13",
				"sectionId": "2.8"
			},
			{
				"id": "6",
				"sectionId": "3.1"
			}
		]
	},
	{
		"name": "Music, Maja",
		"presentations": [
			{
				"id": "1",
				"sectionId": "2.9"
			}
		]
	},
	{
		"name": "Lehotska V.",
		"presentations": [
			{
				"id": "2",
				"sectionId": "2.9"
			}
		]
	},
	{
		"name": "Forrai Gábor",
		"presentations": [
			{
				"id": "3",
				"sectionId": "2.9"
			},
			{
				"id": "3",
				"sectionId": "3.4"
			}
		]
	},
	{
		"name": "Fülöp Rita",
		"presentations": [
			{
				"id": "6",
				"sectionId": "2.9"
			}
		]
	},
	{
		"name": "Besenyei Róbert",
		"presentations": [
			{
				"id": "7",
				"sectionId": "2.9"
			}
		]
	},
	{
		"name": "Vámos Izabella",
		"presentations": [
			{
				"id": "8",
				"sectionId": "2.9"
			}
		]
	},
	{
		"name": "Szalai Gábor",
		"presentations": [
			{
				"id": "9",
				"sectionId": "2.9"
			}
		]
	},
	{
		"name": "Milics Margit",
		"presentations": [
			{
				"id": "10",
				"sectionId": "2.9"
			}
		]
	},
	{
		"name": "Porubszky Tamás",
		"presentations": [
			{
				"id": "11",
				"sectionId": "2.9"
			},
			{
				"id": "6",
				"sectionId": "3.4"
			}
		]
	},
	{
		"name": "Nahm Krisztina",
		"presentations": [
			{
				"id": "12",
				"sectionId": "2.9"
			}
		]
	},
	{
		"name": "Hegyi Péter",
		"presentations": [
			{
				"id": "1",
				"sectionId": "2.10"
			}
		]
	},
	{
		"name": "Weninger Csaba",
		"presentations": [
			{
				"id": "2",
				"sectionId": "2.10"
			}
		]
	},
	{
		"name": "Tóth Judit",
		"presentations": [
			{
				"id": "3",
				"sectionId": "2.10"
			},
			{
				"id": "9",
				"sectionId": "3.1"
			}
		]
	},
	{
		"name": "Kárteszi Hedvig",
		"presentations": [
			{
				"id": "4",
				"sectionId": "2.10"
			},
			{
				"id": "5",
				"sectionId": "3.2"
			}
		]
	},
	{
		"name": "Faluhelyi Nándor",
		"presentations": [
			{
				"id": "5",
				"sectionId": "2.10"
			}
		]
	},
	{
		"name": "Kiss Ildikó",
		"presentations": [
			{
				"id": "6",
				"sectionId": "2.10"
			}
		]
	},
	{
		"name": "Csete Mónika",
		"presentations": [
			{
				"id": "7",
				"sectionId": "2.10"
			}
		]
	},
	{
		"name": "Bézi István",
		"presentations": [
			{
				"id": "8",
				"sectionId": "2.10"
			}
		]
	},
	{
		"name": "Gyömbér Edit",
		"presentations": [
			{
				"id": "9",
				"sectionId": "2.10"
			}
		]
	},
	{
		"name": "Bagi Alexandra",
		"presentations": [
			{
				"id": "10",
				"sectionId": "2.10"
			}
		]
	},
	{
		"name": "Hoffer Krisztina",
		"presentations": [
			{
				"id": "12",
				"sectionId": "2.10"
			}
		]
	},
	{
		"name": "Dankházi Levente",
		"presentations": [
			{
				"id": "13",
				"sectionId": "2.10"
			}
		]
	},
	{
		"name": "Várallyay Péter",
		"presentations": [
			{
				"id": "1",
				"sectionId": "3.1"
			}
		]
	},
	{
		"name": "Rostás Tamás",
		"presentations": [
			{
				"id": "2",
				"sectionId": "3.1"
			}
		]
	},
	{
		"name": "Szólics Alex",
		"presentations": [
			{
				"id": "3",
				"sectionId": "3.1"
			}
		]
	},
	{
		"name": "Szikora István",
		"presentations": [
			{
				"id": "4",
				"sectionId": "3.1"
			}
		]
	},
	{
		"name": "Bartek Péter",
		"presentations": [
			{
				"id": "7",
				"sectionId": "3.1"
			}
		]
	},
	{
		"name": "Nagy Csaba",
		"presentations": [
			{
				"id": "8",
				"sectionId": "3.1"
			}
		]
	},
	{
		"name": "Lenzsér Gábor",
		"presentations": [
			{
				"id": "10",
				"sectionId": "3.1"
			}
		]
	},
	{
		"name": "Vajda Zsolt",
		"presentations": [
			{
				"id": "11",
				"sectionId": "3.1"
			}
		]
	},
	{
		"name": "Gődény Mária",
		"presentations": [
			{
				"id": "1",
				"sectionId": "3.2"
			}
		]
	},
	{
		"name": "Léránt Gergely",
		"presentations": [
			{
				"id": "2",
				"sectionId": "3.2"
			}
		]
	},
	{
		"name": "Jederán Éva",
		"presentations": [
			{
				"id": "3",
				"sectionId": "3.2"
			}
		]
	},
	{
		"name": "Csemez Imre",
		"presentations": [
			{
				"id": "4",
				"sectionId": "3.2"
			}
		]
	},
	{
		"name": "Manninger Sándor",
		"presentations": [
			{
				"id": "7",
				"sectionId": "3.2"
			}
		]
	},
	{
		"name": "Bőcs Katalin",
		"presentations": [
			{
				"id": "8",
				"sectionId": "3.2"
			}
		]
	},
	{
		"name": "Kovács Eszter",
		"presentations": [
			{
				"id": "9",
				"sectionId": "3.2"
			}
		]
	},
	{
		"name": "Horváth Katalin",
		"presentations": [
			{
				"id": "10",
				"sectionId": "3.2"
			}
		]
	},
	{
		"name": "Oláhné Petri Klára",
		"presentations": [
			{
				"id": "11",
				"sectionId": "3.2"
			}
		]
	},
	{
		"name": "Harkányi Zoltán",
		"presentations": [
			{
				"id": "1",
				"sectionId": "3.3"
			},
			{
				"id": "2",
				"sectionId": "3.3"
			}
		]
	},
	{
		"name": "Vargha András",
		"presentations": [
			{
				"id": "1",
				"sectionId": "3.4"
			},
			{
				"id": "2",
				"sectionId": "3.4"
			}
		]
	},
	{
		"name": "Kovács György",
		"presentations": [
			{
				"id": "4",
				"sectionId": "3.4"
			}
		]
	},
	{
		"name": "Bágyi Péter",
		"presentations": [
			{
				"id": "5",
				"sectionId": "3.4"
			},
			{
				"id": "10",
				"sectionId": "3.4"
			}
		]
	},
	{
		"name": "Ewopharma-Bracco Symposium",
		"presentations": [
			{
				"id": "7",
				"sectionId": "3.4"
			}
		]
	},
	{
		"name": "Kostyál László",
		"presentations": [
			{
				"id": "8",
				"sectionId": "3.4"
			}
		]
	},
	{
		"name": "Lánczi Levente István",
		"presentations": [
			{
				"id": "9",
				"sectionId": "3.4"
			}
		]
	},
	{
		"name": "Tóth Levente",
		"presentations": [
			{
				"id": "1",
				"sectionId": "3.5"
			}
		]
	},
	{
		"name": "Panajotu Alexisz",
		"presentations": [
			{
				"id": "2",
				"sectionId": "3.5"
			}
		]
	},
	{
		"name": "Tóth Attila",
		"presentations": [
			{
				"id": "3",
				"sectionId": "3.5"
			}
		]
	},
	{
		"name": "Várady Edit",
		"presentations": [
			{
				"id": "5",
				"sectionId": "3.5"
			}
		]
	},
	{
		"name": "Suhai Ferenc Imre",
		"presentations": [
			{
				"id": "6",
				"sectionId": "3.5"
			}
		]
	},
	{
		"name": "Szukits Sándor",
		"presentations": [
			{
				"id": "7",
				"sectionId": "3.5"
			}
		]
	},
	{
		"name": "Wlasitsch-Nagy Zsófia",
		"presentations": [
			{
				"id": "5",
				"sectionId": "3.5"
			}
		]
	},
	{
		"name": "Virágh Károly",
		"presentations": [
			{
				"id": "6",
				"sectionId": "3.5"
			}
		]
	},
	{
		"name": "Maurovich-Horvat Pál",
		"presentations": [
			{
				"id": "7",
				"sectionId": "3.5"
			}
		]
	},
	{
		"name": "Polovitzer Mária",
		"presentations": [
			{
				"id": "2",
				"sectionId": "3.6"
			}
		]
	},
	{
		"name": "Koller Orsolya",
		"presentations": [
			{
				"id": "3",
				"sectionId": "3.6"
			}
		]
	},
	{
		"name": "Kiss Tünde",
		"presentations": [
			{
				"id": "4",
				"sectionId": "3.6"
			}
		]
	},
	{
		"name": "Várkonyi Ildikó",
		"presentations": [
			{
				"id": "5",
				"sectionId": "3.6"
			}
		]
	},
	{
		"name": "Héjj Ildikó Mária",
		"presentations": [
			{
				"id": "6",
				"sectionId": "3.6"
			}
		]
	},
	{
		"name": "Mohay Gabriella",
		"presentations": [
			{
				"id": "7",
				"sectionId": "3.6"
			}
		]
	},
	{
		"name": "Lakatos Andrea",
		"presentations": [
			{
				"id": "8",
				"sectionId": "3.6"
			}
		]
	},
	{
		"name": "Juhász Emese",
		"presentations": [
			{
				"id": "9",
				"sectionId": "3.6"
			}
		]
	},
	{
		"name": "Szabó Ágota",
		"presentations": [
			{
				"id": "10",
				"sectionId": "3.6"
			}
		]
	},
	{
		"name": "Nyitrai Anna",
		"presentations": [
			{
				"id": "11",
				"sectionId": "3.6"
			}
		]
	},
	{
		"name": "Molnár Diána",
		"presentations": [
			{
				"id": "12",
				"sectionId": "3.6"
			}
		]
	}
];

data.sort(
	(a,b)=>{
		if(a.name < b.name) return -1;
		if(a.name > b.name) return 1;
		return 0
	}
);

export default data;

